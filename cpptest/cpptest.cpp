extern "C" 
{
#include "asss.h"
#include "objects.h"
}

#include <stdio.h>
#include <iostream>

static Imodman     *mm;
static Iconfig     *cfg;
static Icmdman     *cmd;
static Iplayerdata *pd;
static Ilogman     *lm;

static int playerKey = -1;

struct playerData
{
        int bla;
};

static void Ccpp(const char *tc, const char *params, Player *p, const Target *target)
{
        lm->Log(L_ERROR, "?cmd: Hello, C++ world!\n");
}


static void ReleaseInterfaces()
{
        mm->ReleaseInterface(cfg   );
        mm->ReleaseInterface(cmd   );
        mm->ReleaseInterface(pd    );
        mm->ReleaseInterface(lm    );
}

extern "C" EXPORT int MM_cpptest(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;

                cfg    = (Iconfig*)     mm->GetInterface(I_CONFIG    , ALLARENAS);
                cmd    = (Icmdman*)     mm->GetInterface(I_CMDMAN    , ALLARENAS);
                pd     = (Iplayerdata*) mm->GetInterface(I_PLAYERDATA, ALLARENAS);
                lm     = (Ilogman*)     mm->GetInterface(I_LOGMAN    , ALLARENAS);

                if (!cfg || !cmd || !pd || !lm)
                {
                        printf("Missing Interface\n");
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));

                if (playerKey == -1)
                {
                        ReleaseInterfaces();

                        return MM_FAIL;
                }

		cmd->AddCommand("cpp", Ccpp, ALLARENAS, NULL);

                std::cout << "Hello, C++ world!\n";
                lm->Log(L_ERROR, "Hello, C++ world!\n");

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                cmd->RemoveCommand("cpp", Ccpp, ALLARENAS);

                pd->FreePlayerData(playerKey);

                ReleaseInterfaces();
                return MM_OK;
        }

        return MM_FAIL;
}
