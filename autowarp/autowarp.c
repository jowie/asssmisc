/*
 * autowarp3
 *
 * based on autowarp. Reads .conf entries instead. The warps are relative
 * instead of absolute. this is an attach module.
 *
 * arena.conf format:

[AutoWarp]
Area#=x,y,x2,y2
WarpXY#=x,y

 * 07/03/04 - Mod started by Smong.
 *  Don't make the destination overlap a source area!
 *
 * 31/03/04 - Parts updated from cvs by hand.
 *
 * 04/06/04 - Changed dest area to warp dist xy.
 *
 * 01/07/04 - Changed SPEC to SHIP_SPEC.
 *
 * 06/02/04 - removed dependency on regions.h
 *
 * 07/02/04 - corrected .conf example. fixed width/height bug.
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include "asss.h"

typedef struct rect_t
{
	int x, y, w, h;
} rect_t;

struct wdata
{
	rect_t src, dst;
};

struct adata
{
	int on;
	int count;
	struct wdata *data;
};

local inline void str_to_rect(const char *str, rect_t *r);
local int conf_count_entries(ConfigHandle ch);
local void conf_extract_entries(ConfigHandle ch, struct adata *ad);
local rect_t *check_areas(Arena *arena, struct C2SPosition *pos);

/* packet funcs */
local void Pppk(Player *, byte *, int);

/* global data */
local int adkey;
local Imodman *mm;
local Inet *net;
local Iarenaman *aman;
local Iconfig *cfg;

local inline void str_to_rect(const char *str, rect_t *r)
{
	sscanf(str, "%d,%d,%d,%d", &r->x, &r->y, &r->w, &r->h);
	
	/* convert coords to width/height */
	r->w = 1 + r->w - r->x;
	r->h = 1 + r->h - r->y;
}

local int conf_count_entries(ConfigHandle ch)
{
	char key[8];
	const char *str;
	int i;

	for(i = 0; i < 1000; i++)
	{
		sprintf(key, "Area%d", i);
		str = cfg->GetStr(ch, "AutoWarp", key);
		if (!str) break;

		sprintf(key, "WarpXY%d", i);
		str = cfg->GetStr(ch, "AutoWarp", key);
		if (!str) break;
	}

	return i;
}

local void conf_extract_entries(ConfigHandle ch, struct adata *ad)
{
	char key[8];
	const char *str;
	int i;

	for(i = 0; i < ad->count; i++)
	{
		sprintf(key, "Area%d", i);
		str = cfg->GetStr(ch, "AutoWarp", key);
		if (!str) return;
		str_to_rect(str, &ad->data[i].src);

		sprintf(key, "WarpXY%d", i);
		str = cfg->GetStr(ch, "AutoWarp", key);
		if (!str) return;
		sscanf(str, "%d , %d", &ad->data[i].dst.x, &ad->data[i].dst.y);
	}
}

local rect_t *check_areas(Arena *arena, struct C2SPosition *pos)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	int i, x, y;
	rect_t *dst = NULL;

	x = pos->x >> 4;
	y = pos->y >> 4;

	for(i = 0; i < ad->count; i++)
	{
		rect_t *r = &ad->data[i].src;

		if (x < r->x) continue;
		if (y < r->y) continue;
		if (x > r->x + r->w) continue;
		if (y > r->y + r->h) continue;

		dst = &ad->data[i].dst;
		break;
	}

	return dst;
}

EXPORT int MM_autowarp(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;
		net = mm->GetInterface(I_NET, ALLARENAS);
		aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		if (!net || !aman || !cfg) return MM_FAIL;

		adkey = aman->AllocateArenaData(sizeof(struct adata));
		if (adkey == -1) return MM_FAIL;

		net->AddPacket(C2S_POSITION, Pppk);

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		net->RemovePacket(C2S_POSITION, Pppk);
		
		aman->FreeArenaData(adkey);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(cfg);
		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		struct adata *ad = P_ARENA_DATA(arena, adkey);
		ad->on = 1;
		
		ad->count = conf_count_entries(arena->cfg);
		if (ad->count > 0)
		{
			ad->data = amalloc( ad->count * sizeof(struct wdata) );
			conf_extract_entries(arena->cfg, ad);
		}
		
		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		struct adata *ad = P_ARENA_DATA(arena, adkey);
		ad->on = 0;
		
		if (ad->count > 0)
		{
			free( ad->data );
			ad->count = 0;
		}
		
		return MM_OK;
	}
	return MM_FAIL;
}


local void DoChecksum(struct S2CWeapons *pkt)
{
	int i;
	u8 ck = 0;
	pkt->checksum = 0;
	for (i = 0; i < sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData); i++)
		ck ^= ((unsigned char*)pkt)[i];
	pkt->checksum = ck;
}


void Pppk(Player *p, byte *p2, int len)
{
	struct C2SPosition *pos = (struct C2SPosition *)p2;
	Arena *arena = p->arena;
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	rect_t *r;

	if (len < 22)
		return;

	/* handle common errors */
	if (!arena || !ad->on) return;

	/* speccers don't get their position sent to anyone */
	if (p->p_ship == SHIP_SPEC)
		return;

	r = check_areas(p->arena, pos);

	if (r)
	{
		int x = pos->x + (r->x << 4);
		int y = pos->y + (r->y << 4);
	
		struct S2CWeapons wpn = {
			S2C_WEAPON, pos->rotation, (pos->time + 10000) & 0xFFFF,
			x, pos->yspeed, p->pid, pos->xspeed, 0, pos->status, 0,
			y, pos->bounty
		};

		DoChecksum(&wpn);
		net->SendToOne(p, (byte*)&wpn, sizeof(struct S2CWeapons) -
				sizeof(struct ExtraPosData), NET_PRI_P4);
	}
}

