/*
 * Gun Turrets
 * D1st0rt for EverSpace
 * License: MIT/X11
 * Created: 2010-09-29
 *
 * Customizable fake players that attach and fire with anchor ship.
 */


///////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////
#include <string.h>
#include <math.h>
#include "asss.h"
#include "fake.h"
#include "gunturret.h"
#include "advdamage.h"

///////////////////////////////////////////////////////////////////////
// Interfaces
///////////////////////////////////////////////////////////////////////
local Imodman *mm;
local Ilogman *lm;
local Ichat *chat;
local Igame *game;
local Iplayerdata *pd;
local Ifake *fake;
local Inet *net;
local Imainloop *ml;
local Icmdman *cmd;

///////////////////////////////////////////////////////////////////////
// Defines/Types
///////////////////////////////////////////////////////////////////////
#define COAST_TIME 500
#define PPK_INTERVAL 50

typedef struct gtppd
{
    LinkedList turrets;
} gtppd;

typedef struct gt
{
    GunTurretInfo *type;
    Player *p;
    int coast;
    ticks_t lastpkt;
    ticks_t startcoast;
} gt;

///////////////////////////////////////////////////////////////////////
// Globals
///////////////////////////////////////////////////////////////////////
local pthread_mutex_t globalmutex;
local int pdkey;
#define PI 3.14159265f
local double sintabf[40];

EXPORT const char info_gunturret[] = "release 1 by D1st0rt <d1st0rter@gmail.com>";

///////////////////////////////////////////////////////////////////////
// Interface Prototypes
///////////////////////////////////////////////////////////////////////
local int CheckTurret(Player *p, GunTurretInfo *info);
local int SetTurret(Player *p, GunTurretInfo *info);
local int AddTurret(Player *p, GunTurretInfo *info);
local int RemoveTurret(Player *p, GunTurretInfo *info);
local int RemoveAllTurrets(Player *p);
local GunTurretInfo *CreateSimpleTurret(const char *name, int ship, int type, int level, int x, int y, int rot);
local int GetGunTurretInfo(Player *p, LinkedList *list);

///////////////////////////////////////////////////////////////////////
// Interface Definitions
///////////////////////////////////////////////////////////////////////
local Igunturret myint =
{
    INTERFACE_HEAD_INIT(I_GUNTURRET, "gunturret")

    CheckTurret,
    SetTurret,
    AddTurret,
    RemoveTurret,
    RemoveAllTurrets,
    CreateSimpleTurret,
    GetGunTurretInfo,
};

///////////////////////////////////////////////////////////////////////
// Non-Interface Prototypes
///////////////////////////////////////////////////////////////////////
local gt *CreateTurret(Player *p, GunTurretInfo *info);
local void DestroyTurret(Player *p, gt *turret);
local void ActivateTurret(Player *p, gt *turret);
local void DisableTurret(Player *p, gt *turret);
local void DisableAllTurrets(Player *p);
local void ActivateAllTurrets(Player *p);
local void SendTurretPosition(Player *p, gt *turret);

///////////////////////////////////////////////////////////////////////
// Callbacks
///////////////////////////////////////////////////////////////////////
local void PAction(Player *p, int action, Arena *arena);
local void ShipFreqChange(Player *p, int ns, int os, int nf, int of);
local void PPK(Player *p, const struct C2SPosition *pos);
local void Kill(Arena *a, Player *k, Player *d, int bty, int flags, int pts, int green);
local void AttemptKill(Player *p, Player *shooter);
local void NewPlayer(Player *p, int isnew);


///////////////////////////////////////////////////////////////////////
// Commands
///////////////////////////////////////////////////////////////////////
local void Cresetturrets(const char *tc, const char *params, Player *p_cmd, const Target *target);

///////////////////////////////////////////////////////////////////////
// Advisers
///////////////////////////////////////////////////////////////////////

local void EditDeathAdv(Arena *arena, Player **killer, Player **killed, int *bounty);
local Akill killadv = 
{
    ADVISER_HEAD_INIT(A_KILL)

    NULL,
    EditDeathAdv
};

///////////////////////////////////////////////////////////////////////
// Timers
///////////////////////////////////////////////////////////////////////
local int SendPositions(void *clos);

///////////////////////////////////////////////////////////////////////
// Entry Point
///////////////////////////////////////////////////////////////////////
EXPORT int MM_gunturret(int action, Imodman *mm_, Arena *arena)
{
    int i;
    if (action == MM_LOAD)
    {
        // Global interfaces
        mm = mm_;
        chat    = mm->GetInterface(I_CHAT,       ALLARENAS);
        game    = mm->GetInterface(I_GAME,       ALLARENAS);
        lm      = mm->GetInterface(I_LOGMAN,     ALLARENAS);
        pd      = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
        fake    = mm->GetInterface(I_FAKE,       ALLARENAS);
        net     = mm->GetInterface(I_NET,        ALLARENAS);
        ml      = mm->GetInterface(I_MAINLOOP,   ALLARENAS);
        cmd     = mm->GetInterface(I_CMDMAN,     ALLARENAS);

        if (!chat || !game || !lm || !pd || !fake || !net || !ml || !cmd)
            return MM_FAIL;

        // Locking
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&globalmutex, &attr);
        pthread_mutexattr_destroy(&attr);

        // Data
        pdkey = pd->AllocatePlayerData(sizeof(gtppd));

        // Callbacks
        mm->RegCallback(CB_PLAYERACTION, PAction, ALLARENAS);
        mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChange, ALLARENAS);
        mm->RegCallback(CB_PPK, PPK, ALLARENAS);
        mm->RegCallback(CB_KILL, Kill, ALLARENAS);
        mm->RegCallback(CB_ADVDAMAGE_ATTEMPTKILL, AttemptKill, ALLARENAS);
        mm->RegCallback(CB_NEWPLAYER, NewPlayer, ALLARENAS);


        // Commands
        cmd->AddCommand("resetturrets", Cresetturrets, ALLARENAS, NULL);
        
	// Timers
        ml->SetTimer(SendPositions, 1, 1, NULL, NULL);

        // Interface registration
        mm->RegInterface(&myint, ALLARENAS);
        mm->RegAdviser(&killadv, ALLARENAS);

        for (i = 0; i < 40; i++)
	    sintabf[i] = sin(i * PI / 20);

        return MM_OK;
    }
    else if (action == MM_UNLOAD)
    {
        // Interface registration
        mm->UnregInterface(&myint, ALLARENAS);
        mm->UnregAdviser(&killadv, ALLARENAS);
        
        // Timers
        ml->ClearTimer(SendPositions, NULL);

        // Commands
        cmd->RemoveCommand("resetturrets", Cresetturrets, ALLARENAS);

        // Callbacks
        mm->UnregCallback(CB_PPK, PPK, ALLARENAS);
        mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChange, ALLARENAS);
        mm->UnregCallback(CB_PLAYERACTION, PAction, ALLARENAS);
        mm->UnregCallback(CB_KILL, Kill, ALLARENAS);
        mm->UnregCallback(CB_ADVDAMAGE_ATTEMPTKILL, AttemptKill, ALLARENAS);
        mm->UnregCallback(CB_NEWPLAYER, NewPlayer, ALLARENAS);

        // Data
        pd->FreePlayerData(pdkey);

        // Locking
        pthread_mutex_destroy(&globalmutex);

        // Global interfaces
        mm->ReleaseInterface(ml);
        mm->ReleaseInterface(net);
        mm->ReleaseInterface(fake);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(lm);
        mm->ReleaseInterface(game);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(cmd);

        return MM_OK;
    }
    else if (action == MM_PREUNLOAD)
    {
        Link *link;
        Player *p;

        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
            RemoveAllTurrets(p);
        }
        pd->Unlock();
        return MM_OK;
    }

    return MM_FAIL;
}

///////////////////////////////////////////////////////////////////////
// Interface Functions
///////////////////////////////////////////////////////////////////////
local int CheckTurret(Player *p, GunTurretInfo *info)
{
    int result = 0;
    gtppd *data;
    Link *link;
    gt *turret;

    if(p)
    {
        if(info)
        {
            pthread_mutex_lock(&globalmutex);

            data = (gtppd *)PPDATA(p, pdkey);
            FOR_EACH(&data->turrets, turret, link)
            {
                if(turret->type == info)
                {
                    result = 1;
                    break;
                }
            }

            pthread_mutex_unlock(&globalmutex);
        }
        else
        {
            lm->LogP(L_ERROR, "gunturret", p, "Tried to pass null info to CheckTurret");
        }
    }
    else
    {
        lm->Log(L_ERROR, "<gunturret> Tried to call CheckTurret on null player");
    }

    return result;
}

local int SetTurret(Player *p, GunTurretInfo *info)
{
    int result = 0;

    if(p)
    {
        if(info)
        {
            pthread_mutex_lock(&globalmutex);
            RemoveAllTurrets(p);
            result = AddTurret(p, info);
            pthread_mutex_unlock(&globalmutex);
        }
        else
        {
            lm->LogP(L_ERROR, "gunturret", p, "Tried to pass null info to SetTurret");
        }
    }
    else
    {
        lm->Log(L_ERROR, "<gunturret> Tried to call SetTurret on null player");
    }

    return result;
}

local int AddTurret(Player *p, GunTurretInfo *info)
{
    int result = 0;
    gtppd *data;
    gt *turret;

    if(p)
    {
        if(info)
        {
            pthread_mutex_lock(&globalmutex);
            if(CheckTurret(p, info))
            {
                lm->LogP(L_WARN, "gunturret", p, "Tried to add existing turret again, ignoring");
            }
            else
            {
                data = (gtppd *)PPDATA(p, pdkey);
                turret = CreateTurret(p, info);
                LLAdd(&data->turrets, turret);
            }
            pthread_mutex_unlock(&globalmutex);

            result = 1;
        }
        else
        {
            lm->LogP(L_ERROR, "gunturret", p, "Tried to pass null info to AddTurret");
        }
    }
    else
    {
        lm->Log(L_ERROR, "<gunturret> Tried to call AddTurret on null player");
    }

    return result;
}

local int RemoveTurret(Player *p, GunTurretInfo *info)
{
    int result = 0;
    Link *link;
    gtppd *data;
    gt *turret;

    if(p)
    {
        if(info)
        {
            pthread_mutex_lock(&globalmutex);

            if(!CheckTurret(p, info))
            {
                lm->LogP(L_WARN, "gunturret", p, "Tried to remove nonexistant turret, ignoring");
            }
            else
            {
                data = (gtppd *)PPDATA(p, pdkey);
                FOR_EACH(&data->turrets, turret, link)
                {
                    if(turret->type == info)
                    {
                        result = 1;
                        break;
                    }
                }

                if(result)
                {
                    DestroyTurret(p, turret);
                    LLRemove(&data->turrets, turret);
                }
            }
            result = 1;

            pthread_mutex_unlock(&globalmutex);
        }
        else
        {
            lm->LogP(L_ERROR, "gunturret", p, "Tried to pass null info to RemoveTurret");
        }
    }
    else
    {
        lm->Log(L_ERROR, "<gunturret> Tried to call RemoveTurret on null player");
    }

    return result;
}

local int RemoveAllTurrets(Player *p)
{
    int result = 0;
    gtppd *data;
    gt *turret;
    Link *link;

    if(p)
    {
        pthread_mutex_lock(&globalmutex);

        data = (gtppd *)PPDATA(p, pdkey);
        FOR_EACH(&data->turrets, turret, link)
        {
            DestroyTurret(p, turret);
        }
        LLEmpty(&data->turrets);

        result = 1;
        lm->LogP(L_DRIVEL, "gunturret", p, "Removed all turrets");

        pthread_mutex_unlock(&globalmutex);
    }
    else
    {
        lm->Log(L_ERROR, "<gunturret> Tried to call RemoveTurret on null player");
    }

    return result;
}

local GunTurretInfo *CreateSimpleTurret(const char *name, int ship, int type, int level, int x, int y, int rot)
{
    GunTurretInfo *info = NULL;

    if(name)
    {
        if(ship >= SHIP_WARBIRD && ship <= SHIP_SHARK)
        {
            if(type >= W_BULLET && type <= W_THOR)
            {
                if(level >= 0 && level <= 3)
                {
                    info = amalloc(sizeof(GunTurretInfo));
                    astrncpy(info->name, name, sizeof(info->name));
                    info->ship = ship;
                    info->x = x;
                    info->y = y;
                    info->rot = rot;
                    info->weapon.type = type;
                    info->weapon.level = level;
                }
                else
                {
                    lm->Log(L_ERROR, "<gunturret> Tried to call CreateSimpleTurret with invalid level (%d)", level);
                }
            }
            else
            {
                lm->Log(L_ERROR, "<gunturret> Tried to call CreateSimpleTurret with invalid type (%d)", type);
            }
        }
        else
        {
            lm->Log(L_ERROR, "<gunturret> Tried to call CreateSimpleTurret with invalid ship (%d)", ship);
        }
    }
    else
    {
        lm->Log(L_ERROR, "<gunturret> Tried to call CreateSimpleTurret with null name");
    }

    return info;
}

local int GetGunTurretInfo(Player *p, LinkedList *list)
{
    int result = 0;
    gtppd *data;
    Link *link;
    gt *turret;

    if(p)
    {
        if(list)
        {
            pthread_mutex_lock(&globalmutex);

            data = (gtppd *)PPDATA(p, pdkey);
            FOR_EACH(&data->turrets, turret, link)
            {
                LLAdd(list, turret->type);
            }

            result = 1;

            pthread_mutex_unlock(&globalmutex);
        }
        else
        {
            lm->LogP(L_ERROR, "gunturret", p, "Tried to pass null list to GetGunTurretInfo");
        }
    }
    else
    {
        lm->Log(L_ERROR, "<gunturret> Tried to call GetGunTurretInfo on null player");
    }

    return result;
}

///////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////
local gt *CreateTurret(Player *p, GunTurretInfo *info)
{
    gt *turret;

    turret = amalloc(sizeof(gt));
    turret->type = info;

    if(p->p_ship != SHIP_SPEC)
    {
        ActivateTurret(p, turret);
    }

    return turret;
}

local void DestroyTurret(Player *p, gt *turret)
{
    if(turret->p)
    {
        DisableTurret(p, turret);
    }
    afree(turret);
}

local void ActivateTurret(Player *p, gt *turret)
{
    turret->p = fake->CreateFakePlayer(turret->type->name, p->arena, turret->type->ship, p->p_freq);
    turret->p->p_attached = p->pid;
    struct SimplePacket pkt = { S2C_TURRET,turret->p->pid, p->pid };
    net->SendToArena(p->arena, NULL, (byte *)&pkt, 5, NET_RELIABLE);
    SendTurretPosition(p, turret);
    DO_CBS(CB_GUNTURRET_UPDATE, p->arena, GunTurretUpdateFunc, (p, turret->type, 1));
}

local void DisableTurret(Player *p, gt *turret)
{
    if (turret->p)
        fake->EndFaked(turret->p);
    turret->p = NULL;
    DO_CBS(CB_GUNTURRET_UPDATE, p->arena, GunTurretUpdateFunc, (p, turret->type, 0));
}

local void DisableAllTurrets(Player *p)
{
    gtppd *data;
    gt *turret;
    Link *link;

    pthread_mutex_lock(&globalmutex);

    data = (gtppd *)PPDATA(p, pdkey);
    FOR_EACH(&data->turrets, turret, link)
    {
        if(!turret->p)
        {
            lm->LogP(L_WARN, "gunturret", p, "Tried to disable turrets that are already disabled, ignoring");
            break;
        }

        DisableTurret(p, turret);
    }

    pthread_mutex_unlock(&globalmutex);
}

local void ActivateAllTurrets(Player *p)
{
    gtppd *data;
    gt *turret;
    Link *link;

    pthread_mutex_lock(&globalmutex);

    data = (gtppd *)PPDATA(p, pdkey);
    FOR_EACH(&data->turrets, turret, link)
    {
        if(turret->p)
        {
            lm->LogP(L_WARN, "gunturret", p, "Tried to activate turrets that are already activated, ignoring");
            break;
        }

        ActivateTurret(p, turret);
    }

    pthread_mutex_unlock(&globalmutex);
}

local void SendTurretPosition(Player *p, gt *turret)
{
    struct C2SPosition ppk;
    memset(&ppk, 0, sizeof(ppk));

    ppk.type = C2S_POSITION;
    ppk.rotation = p->position.rotation;

    if(turret->coast)
    {
        ppk.y = 1;
        ppk.x = 1;
        ppk.time = current_ticks();
    }
    else
    {
        ppk.y = p->position.y;
        ppk.x = p->position.x;
        ppk.time = p->position.time;
    }
    ppk.xspeed = p->position.xspeed;
    ppk.yspeed = p->position.yspeed;
    ppk.energy = p->position.energy;
    ppk.status = STATUS_UFO | STATUS_CLOAK | STATUS_STEALTH;
    ppk.weapon.type = W_NULL;

    game->FakePosition(turret->p, &ppk, sizeof(ppk));
    turret->lastpkt = current_ticks();
}

///////////////////////////////////////////////////////////////////////
// Callbacks
///////////////////////////////////////////////////////////////////////
local void PAction(Player *p, int action, Arena *arena)
{
    if(action == PA_DISCONNECT)
    {
        RemoveAllTurrets(p);
    }
    else if(action == PA_LEAVEARENA)
    {
        DisableAllTurrets(p);
    }
    else if(action == PA_ENTERGAME && p->p_ship != SHIP_SPEC)
    {
        ActivateAllTurrets(p);
    }
}

local void ShipFreqChange(Player *p, int ns, int os, int nf, int of)
{
    gtppd *data;
    Link *link;
    gt *turret;

    if(ns == SHIP_SPEC && os != SHIP_SPEC)
    {
        DisableAllTurrets(p);
    }
    else if(os == SHIP_SPEC && ns != SHIP_SPEC)
    {
        ActivateAllTurrets(p);
    }
    else if(ns != SHIP_SPEC && nf != of)
    {
        pthread_mutex_lock(&globalmutex);

        data = (gtppd *)PPDATA(p, pdkey);
        FOR_EACH(&data->turrets, turret, link)
        {
            game->SetFreq(turret->p, nf);
            SendTurretPosition(p, turret);
        }

        pthread_mutex_unlock(&globalmutex);
    }
}

local void PPK(Player *p, const struct C2SPosition *pos)
{
    gtppd *data;
    Link *link;
    gt *turret;
    struct C2SPosition ppk;
    int rot;

    switch(pos->weapon.type)
    {
        // player is firing
        case W_BULLET:
        case W_BOUNCEBULLET:
        case W_BOMB:
        case W_PROXBOMB:
            pthread_mutex_lock(&globalmutex);

            memset(&ppk, 0, sizeof(ppk));
            ppk.type = C2S_POSITION;
            ppk.rotation = pos->rotation;
            ppk.time = pos->time;
            ppk.y = pos->y;
            ppk.x = pos->x;
            ppk.xspeed = pos->xspeed;
            ppk.yspeed = pos->yspeed;
            ppk.energy = pos->energy;
            ppk.status = STATUS_UFO | STATUS_CLOAK | STATUS_STEALTH;

            data = (gtppd *)PPDATA(p, pdkey);
            FOR_EACH(&data->turrets, turret, link)
            {
                if(turret->p && !turret->coast)
                {
                    ppk.y = pos->y;
                    ppk.x = pos->x;
                    rot = pos->rotation;
                    
                    rot += turret->type->rot;
                    rot %= 40;
                    if (rot < 0)
                    	rot += 40;
                    
                    ppk.x += (int)round(  turret->type->x * sintabf[(rot + 10) % 40] - turret->type->y * sintabf[rot % 40]  );
                    ppk.y += (int)round(  turret->type->x * sintabf[rot % 40]        + turret->type->y * sintabf[(rot + 10) % 40]  );
                                        
                    ppk.rotation = rot;
                    memcpy(&ppk.weapon, &turret->type->weapon, sizeof(struct Weapons));
                    game->FakePosition(turret->p, &ppk, sizeof(ppk));
                    turret->lastpkt = current_ticks();
                }
            }

            pthread_mutex_unlock(&globalmutex);
        break;
    }
}

local void Kill(Arena *a, Player *k, Player *d, int bty, int flags, int pts, int green)
{
    gtppd *data;
    Link *link;
    gt *turret;

    pthread_mutex_lock(&globalmutex);

    data = (gtppd *)PPDATA(d, pdkey);
    FOR_EACH(&data->turrets, turret, link)
    {
        turret->coast = 1;
        turret->p->p_attached = -1;
    }
    pthread_mutex_unlock(&globalmutex);
}

local void AttemptKill(Player *d, Player *k)
{
    gtppd *data;
    Link *link;
    gt *turret;

    pthread_mutex_lock(&globalmutex);

    data = (gtppd *)PPDATA(d, pdkey);
    FOR_EACH(&data->turrets, turret, link)
    {
        turret->coast = 1;
        turret->p->p_attached = -1;
    }
    pthread_mutex_unlock(&globalmutex);
}

local void NewPlayer(Player *newp, int isnew)
{
    Player *p;
    gtppd *data;
    gt *turret;
    Link *link;
    Link *l2, *l2next;
    
    if (isnew)
        return;
    
    // remove turrets of which the fake player has been destroyed (?killfake for example)
    
    pthread_mutex_lock(&globalmutex);
    pd->Lock();
    
    FOR_EACH_PLAYER_P(p, data, pdkey)
    {
        l2 = LLGetHead(&data->turrets);
        while (l2)
        {
            turret = l2->data;
            l2next = l2->next;
        
            if(turret->p == newp)
            {
                 turret->p = NULL; // so that the fake player is not ended 2x
                 DestroyTurret(p, turret);
                 LLRemoveAll(&data->turrets, turret);
            }

            l2 = l2next;
        }
    }

    pd->Unlock();
    pthread_mutex_unlock(&globalmutex);
}

///////////////////////////////////////////////////////////////////////
// Commands
///////////////////////////////////////////////////////////////////////
local void Cresetturrets(const char *tc, const char *params, Player *p_cmd, const Target *target)
{
    Link *link;
    Player *p;

    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if (p->arena == p_cmd->arena)
            RemoveAllTurrets(p);
    }
    pd->Unlock();
}

///////////////////////////////////////////////////////////////////////
// Advisers
///////////////////////////////////////////////////////////////////////
local void EditDeathAdv(Arena *arena, Player **killer, Player **killed, int *bounty)
{
    Player *p;
    gtppd *data;
    gt *turret;
    Link *link;
    Link *l2;
    

    pthread_mutex_lock(&globalmutex);
    pd->Lock();
    
    FOR_EACH_PLAYER_P(p, data, pdkey)
    {
        FOR_EACH(&data->turrets, turret, l2)
        {
            if(turret->p == *killer)
            {
                *killer = p;
                pd->Unlock();
                pthread_mutex_unlock(&globalmutex);
                return;      
            }

            
        }
    }

    pd->Unlock();
    pthread_mutex_unlock(&globalmutex);
}

///////////////////////////////////////////////////////////////////////
// Timers
///////////////////////////////////////////////////////////////////////
local int SendPositions(void *clos)
{
    Player *p;
    gtppd *data;
    gt *turret;
    Link *link;
    Link *l2;
    ticks_t now;

    pthread_mutex_lock(&globalmutex);
    pd->Lock();
    now = current_ticks();

    FOR_EACH_PLAYER_P(p, data, pdkey)
    {
        FOR_EACH(&data->turrets, turret, l2)
        {
            if(turret->coast)
            {
                if(now - turret->startcoast > COAST_TIME)
                {
                    turret->coast = 0;
                    turret->p->p_attached = p->pid;
                    struct SimplePacket pkt = { S2C_TURRET, turret->p->pid, p->pid};
                    net->SendToArena(p->arena, NULL, (byte *)&pkt, 5, NET_RELIABLE);
                }
            }

            if(!turret->coast && now - turret->lastpkt > PPK_INTERVAL && turret->p)
            {
                SendTurretPosition(p, turret);
            }
        }
    }

    pd->Unlock();
    pthread_mutex_unlock(&globalmutex);

    return TRUE;
}

///////////////////////////////////////////////////////////////////////
// Commands
///////////////////////////////////////////////////////////////////////
