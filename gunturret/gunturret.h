/*
    gunturret.h
    Copyright (c) 2010 D1st0rt

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use, copy,
    modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
    BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
    ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/


#ifndef __GUNTURRET_H__
#define __GUNTURRET_H__

/* pyinclude: d1st0rt/gunturret.h */

/**
 * Type definition for a gun turret. Specify the name and ship for the
 * fake player this gun turret will use and fill out the weapon details
 */
typedef struct GunTurretInfo
{
    char name[20];
    byte ship;
    int x;
    int y;
    int rot;
    struct Weapons weapon;
} GunTurretInfo;
/* pytype: opaque, GunTurretInfo *, gtinfo */

#define CB_GUNTURRET_UPDATE "gunturret_update"
/**
 * Callback function for gun turret updates.
 * @param p the updated player
 * @param info the gun turret in question
 * @param active 1 if activated, 0 if deactivated
 */
typedef void (*GunTurretUpdateFunc)(Player *p, GunTurretInfo *info, int active);
/* pycb: player, gtinfo, int */

#define I_GUNTURRET "gunturret"
/**
 * Interface for manipulating "gun turrets" on players. A gun turret is
 * a fake player that attaches to another player and fires a
 * customizable weapon whenever the anchor ship fires a gun or bomb.
 * Multiple gun turrets can be attached to a player, but only one of
 * each type can be attached at any given time. Gun turrets will stay
 * with a player from arena to arena until they are explicitly removed
 * or the player disconnects from the server.
 */
typedef struct Igunturret
{
    INTERFACE_HEAD_DECL
    /* pyint: use */

    /**
     * Checks to see if a player has a specific gun turret attached.
     * @param p player to check
     * @param info turret to check for
     * @return returns 1 if found, 0 otherwise or failure
     */
    int (*CheckTurret)(Player *p, GunTurretInfo *info);
    /* pyint: player, gtinfo -> int */

    /**
     * Sets a player's gun turret. Will remove any previously attached
     * gun turrets.
     * @param p the player to set
     * @param info the turret to set
     * @return 1 if success, 0 if failure
     */
    int (*SetTurret)(Player *p, GunTurretInfo *info);
    /* pyint: player, gtinfo -> int */

    /**
     * Attaches a gun turret to a player. Will ignore requests to add
     * gun turrets already attached to the player.
     * @param p the player to attach to
     * @param info the turret to attach
     * @return 1 if success, 0 if failure
     */
    int (*AddTurret)(Player *p, GunTurretInfo *info);
    /* pyint: player, gtinfo -> int */

    /**
     * Removes a gun turret from a player. Will ignore requests to
     * remove gun turrets not attached to the player.
     * @param p the player to remove from
     * @param info the turret to remove
     * @return 1 if success, 0 if failure.
     */
    int (*RemoveTurret)(Player *p, GunTurretInfo *info);
    /* pyint: player, gtinfo -> int */

    /**
     * Removes all gun turrets from a player.
     * @param p the player to remove from
     * @return 1 if success, 0 if failure.
     */
    int (*RemoveAllTurrets)(Player *p);
    /* pyint: player -> int */

    /**
     * Primarily intended for python modules, creates a basic
     * gun turret type.
     * @param name the name of the gun turret
     * @param ship the ship to use
     * @param type the weapon code
     * @param level the weapon level
     * @param x the x position offset
     * @param y the y position offset
     * @param rot the rotation offset
     * @return a GunTurretInfo with those specs or null if failure
     */
    GunTurretInfo *(*CreateSimpleTurret)(const char *name, int ship,
        int type, int level, int x, int y, int rot);
    /* pyint: string, int, int, int, int, int, int -> gtinfo */

     /**
     * Gets info on all turrets attached to a specific player
     * @param p the player to check
     * @param list the list to append the info to
     * @return 1 if success, 0 if failure
     */
    int (*GetGunTurretInfo)(Player *p, LinkedList *list);

} Igunturret;

#endif
