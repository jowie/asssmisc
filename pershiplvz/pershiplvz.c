/* $Id: pershiplvz.c 241 2010-02-04 13:57:40Z joris $ */
/*
	PerShipLVZ


	Warbird:ShowLVZ = 8500
	Javelin:ShowLVZ = 8501
	Spider:ShowLVZ = 8502
	Leviathan:ShowLVZ = 8503
	Terrier:ShowLVZ = 8504
	WeaselShow:LVZ = 8505
	Lancaster:ShowLVZ = 8506
	Shark:ShowLVZ = 8507
	Spectator:ShowLVZ = -1

	6 juli 2007 - Created by JoWie
	16 juli 2007 - Solved bug where a player which just entered did not get a objon
*/

#include "asss.h"
#include "objects.h"

// Interfaces
local Imodman *mm;
local Iobjects *objs;
local Iarenaman *aman;
local Iconfig *cfg;
local Iplayerdata *pd;

struct arenaData
{
        int lvz[9]; //object ids of every ship type
};

struct playerData
{
        int oldShip;
};

local void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq);
local void PlayerAction (Player *p, int action, Arena *arena);
local void setLVZ (Player *p, int newship);

local int arenaKey = -1;
local int playerKey = -1;

local const char *ship_names[] =
{
        "Warbird",
        "Javelin",
        "Spider",
        "Leviathan",
        "Terrier",
        "Weasel",
        "Lancaster",
        "Shark",
        "Spectator"
};


EXPORT const char info_pershiplvz[] = "PerShipLVZ by JoWie ("BUILDDATE")\n"
                                      "This is an attach module, requires the object module.\n"
                                      "Shipname:ShowLVZ\n"
                                      "Spectator:ShowLVZ\n";

// The entry point:
EXPORT int MM_pershiplvz(int action, Imodman *mm_, Arena *arena)
{
        int rv = MM_FAIL; // return value
        int i;

        if (action == MM_LOAD)
        {
                mm = mm_;

                objs = mm->GetInterface(I_OBJECTS, ALLARENAS);
                aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
                cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
                pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);

                if (!objs || !aman || !cfg || !pd) // check interfaces
                {
                        // release interfaces if loading failed
                        mm->ReleaseInterface(objs);
                        mm->ReleaseInterface(aman);
                        mm->ReleaseInterface(cfg);
                        mm->ReleaseInterface(pd);
                        rv = MM_FAIL;
                }
                else
                {
                        // allocate data
                        arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));

                        playerKey = pd->AllocatePlayerData(sizeof(struct playerData));

                        if (arenaKey == -1 || playerKey == -1) // check if we ran out of memory
                        {
                                if (arenaKey != -1) // free data if it was allocated
                                        aman->FreeArenaData(arenaKey);

                                if (playerKey!= -1) // free data if it was allocated
                                        pd->FreePlayerData(playerKey);

                                mm->ReleaseInterface(objs);
                                mm->ReleaseInterface(aman);
                                mm->ReleaseInterface(cfg);
                                mm->ReleaseInterface(pd);

                                rv = MM_FAIL;
                        }
                        else
                        {

                                rv = MM_OK;
                        }
                }
        }
        else if (action == MM_UNLOAD)
        {
                mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChange, ALLARENAS);
                mm->UnregCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);

                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);

                mm->ReleaseInterface(objs);
                mm->ReleaseInterface(aman);
                mm->ReleaseInterface(cfg);
                mm->ReleaseInterface(pd);

                rv = MM_OK;
        }
        else if (action == MM_ATTACH)
        {
                mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChange, arena);
                mm->RegCallback(CB_PLAYERACTION, PlayerAction, arena);

                //read settings
                struct arenaData *adata = P_ARENA_DATA(arena, arenaKey);
                ConfigHandle ch = arena->cfg;

                for (i = 0; i < 9; i++)
                {
                        adata->lvz[i] = cfg->GetInt(ch, ship_names[i], "ShowLVZ", -1);
                }
                rv = MM_OK;
        }
        else if (action == MM_DETACH)
        {
                mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChange, arena);
                mm->UnregCallback(CB_PLAYERACTION, PlayerAction, arena);
                rv = MM_OK;
        }

        return rv;
}

local void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
        if (!p || !p->arena) return;
        if (newship != oldship)
        	setLVZ(p, newship);
}

local void PlayerAction (Player *p, int action, Arena *arena)
{
        if (!p || !arena || p->arena != arena || playerKey == -1) return;

        if (action == PA_ENTERGAME)
        {
                struct playerData *pdata = PPDATA(p, playerKey);
                if (!pdata) return;

                pdata->oldShip = -1;

                setLVZ(p, p->p_ship);
        }
}

local void setLVZ (Player *p, int newship)
{
        if (!p->arena) return;
        pd->Lock();

        struct arenaData *adata = P_ARENA_DATA(p->arena, arenaKey);
        struct playerData* pdata = PPDATA(p, playerKey);

        int oldLVZ;
        int newLVZ;

        Target tgt;
        tgt.type = T_PLAYER;
        tgt.u.p = p;

        if (pdata->oldShip >= 0 && pdata->oldShip <= 8)
        {
                oldLVZ = adata->lvz[pdata->oldShip];
        }
        else
        {
                oldLVZ = -1;
        }

        if (newship >= 0 && newship <= 8)
        {
                newLVZ = adata->lvz[newship];
        }
        else
        {
                newLVZ = -1;
        }

        if (oldLVZ != newLVZ)
        {
                if (oldLVZ >= 0)
                        objs->Toggle(&tgt, oldLVZ, 0);
                if (newLVZ >= 0)
                        objs->Toggle(&tgt, newLVZ, 1);
        }
        pdata->oldShip = newship;

        pd->Unlock();
}
