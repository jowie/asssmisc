/**
    Superweapon v1.0 by JoWie

    Launches various SuperWeapons
	TODO: only rechage player charge when the player is in ship on the correct freq!

[superweapon]
NukeSound = 55
NukeScreenObj = 1951
NukeObj = 1950
NukeObjXOffset = -38
NukeObjYOffset = -130
NukeTurretDamage = 700
NukeTurretRadius = 200
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "asss.h"
#include "dodamage.h"
#include "fake.h"
#include "cncutil.h"
#include "staticturret.h"
#include "superweapon.h"
#include "structures.h"
#include "gamecredits.h"

#define FAKELEAVEAFTER (400)

static int arenaKey = -1;
static int playerKey = -1;
struct arenaData
{
	LinkedList freqs; // struct freqData*
};

struct freqData
{
	union
	{
		Arena *arena;
		Player *p;
	} t;
	int freq;

	struct
	{
		ticks_t chargeStartAt; // when did we begin charging?
		int nukeCharge; // how much has been charged in the past?
	} superweapons[SUPERWEAPONS];
};

struct playerData
{
	Arena *arena;
	LinkedList freqs; // struct freqData*

	bool inFireSequence;
	bool fireTimer;
	Superweapon aimingWeapon;

	ticks_t lastFireObjectUpdate;
	int lastFireX;
	int lastFireY;
	int oldShip;
	int refundCredits;
	int freqChargeRequired;
};

static struct arenaData* GetArenaData (Arena *arena);
static struct playerData* GetPlayerData(Player *p);
static struct freqData *GetFreqData(Arena *arena, int freq, bool allocFreqData);
static struct freqData *GetPlayerFreqData(Player *p, int freq, bool allocFreqData);
static void ConstructArenaData(struct arenaData *adata);
static void DestructArenaData(struct arenaData *adata);
static void ConstructFreqData(struct freqData *fdata);
static void DestructFreqData(struct freqData *fdata);
static struct freqData *GetFreqData(Arena *arena, int freq, bool allocFreqData);
static struct freqData *GetPlayerFreqData(Player *p, int freq, bool allocFreqData);
static void ConstructPlayerData(struct playerData *pdata);
static void DestructPlayerData(struct playerData *pdata);
static void NewPlayerCB(Player *p, int isNew);
static void CancelFireSequence(Player *p, bool refund);
static void Cnuke(const char *tc, const char *params, Player *p, const Target *target);
static void Nuke(Arena *arena, int x, int y, int friendlyfreq);
static void NukeFireSequence(Player *p, int refundCredits, int freqChargeRequired);
static  int GetChargeFreq(Arena *arena, int freq, Superweapon superweapon);
static  int GetChargePlayer(Player *p, Superweapon superweapon);
static void StartGame(Arena *arena);
static void StopGame(Arena *arena);

static Imodman     *mm;
static Ichat       *chat;
static Iconfig     *cfg;
static Ilogman     *lm;
static Icmdman     *cmd;
static Ifake       *fake;
static Idodamage   *dodamage;
static Iarenaman   *aman;
static Iplayerdata *pd;
static Igame       *game;
static Iutil       *util;
static Iobjects    *objs;
static Imainloop   *ml;
static Isuperweapon superweapon =
{
	INTERFACE_HEAD_INIT(I_SUPERWEAPON, "SUPERWEAPON")

	NukeFireSequence,
	GetChargeFreq,
	GetChargePlayer,
	StartGame,
	StopGame
};

static const char * const superweapon_names[] = {
	"Nuclear Missile"
};

static void ConstructArenaData(struct arenaData *adata)
{
	LLInit(&adata->freqs);

};


static void DestructArenaData(struct arenaData *adata)
{
	struct freqData *fdata;
	Link *l;
	for (l = LLGetHead(&adata->freqs); l; l = l->next)
        {
                fdata = l->data;
                DestructFreqData(fdata);
                afree(fdata);
        }
        LLEmpty(&adata->freqs);
};

static struct arenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey != -1);
        struct arenaData* adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);

        return adata;
}

static void ConstructFreqData(struct freqData *fdata)
{
        assertlm(fdata);

}

static void DestructFreqData(struct freqData *fdata)
{
        assertlm(fdata);

}

static struct freqData *GetFreqData(Arena *arena, int freq, bool allocFreqData)
{
        struct arenaData *adata;
        struct freqData *fdata;
        Link *l;

        assertlm(arena);
        assertlm(freq < 10000);
        adata = GetArenaData(arena);

        for (l = LLGetHead(&adata->freqs); l; l = l->next)
        {
                fdata = l->data;

                if (fdata->freq == freq)
                        return fdata;
        }

        // freq data not found after this point
        if (allocFreqData)
        {
                fdata = amalloc(sizeof(struct freqData));
                ConstructFreqData(fdata);
                fdata->t.arena = arena;
                fdata->freq = freq;

                LLAdd(&adata->freqs, fdata);
                return fdata;
        }

        return NULL;
}

static struct freqData *GetPlayerFreqData(Player *p, int freq, bool allocFreqData)
{
        struct playerData *pdata;
        struct freqData *fdata;
        Link *l;

        assertlm(p);
        assertlm(freq < 10000);
        pdata = GetPlayerData(p);

        for (l = LLGetHead(&pdata->freqs); l; l = l->next)
        {
                fdata = l->data;

                if (fdata->freq == freq)
                        return fdata;
        }

        // freq data not found after this point
        if (allocFreqData)
        {
                fdata = amalloc(sizeof(struct freqData));
                ConstructFreqData(fdata);
                fdata->t.p = p;
                fdata->freq = freq;

                LLAdd(&pdata->freqs, fdata);
                return fdata;
        }

        return NULL;
}

static void ConstructPlayerData(struct playerData *pdata)
{
        assertlm(pdata);
	LLInit(&pdata->freqs);
}

static void DestructPlayerData(struct playerData *pdata)
{
	struct freqData *fdata;
	Link *l;
        assertlm(pdata);

	pdata->arena = NULL;

	for (l = LLGetHead(&pdata->freqs); l; l = l->next)
        {
                fdata = l->data;
                DestructFreqData(fdata);
                afree(fdata);
        }
        LLEmpty(&pdata->freqs);
}

static struct playerData* GetPlayerData(Player *p)
{
        assertlm(p);
        assertlm(playerKey != -1);
        struct playerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);

        if (p->arena && p->arena != pdata->arena)
        {
        	if (pdata->arena)
			DestructPlayerData(pdata);
                pdata->arena = p->arena;
                ConstructPlayerData(pdata);

        }
        return pdata;
}


static helptext_t Cnuke_help =
        "Targets: arena\n"
        "Args: <x coord> <y coord>\n"
        "Fires a nuclear missile.\n"
        "If the coordinates are not given, your current location is used\n";

static void Cnuke(const char *tc, const char *params, Player *p, const Target *target)
{
        char *next;
        int x, y;

        if (!p->arena) return;

        x = strtol(params, &next, 0);
        while (*next == ',' || *next == ' ') next++;
        y = strtol(next, NULL, 0);

        if (x)
                x = x << 4;
        else
                x = p->position.x;

        if (y)
                y = y << 4;
        else
                y = p->position.y;


	Nuke(p->arena, x, y, p->p_freq);
}

static void Nuke(Arena *arena, int x, int y, int friendlyfreq)
{
	Target tgt;
	int screenObj;
	int nukeObj;
	int xoffset, yoffset;
	int sound;
	int turretdamage, turretradius;
	int structuredamage, structureradius;
	struct arenaData* adata;
	ConfigHandle ch;

        tgt.type = T_ARENA;
        tgt.u.arena = arena;
        ch = arena->cfg;
	adata = GetArenaData(arena);




	sound = cfg->GetInt(ch, "superweapon", "NukeSound", 0);
	screenObj = cfg->GetInt(ch, "superweapon", "NukeScreenObj", -1);
	nukeObj = cfg->GetInt(ch, "superweapon", "NukeObj", -1);
	xoffset = cfg->GetInt(ch, "superweapon", "NukeObjXOffset", 0);
	yoffset = cfg->GetInt(ch, "superweapon", "NukeObjYOffset", 0);
	turretdamage = cfg->GetInt(ch, "superweapon", "NukeTurretDamage", 0);
	turretradius = cfg->GetInt(ch, "superweapon", "NukeTurretRadius", 0);
	structuredamage = cfg->GetInt(ch, "superweapon", "NukeStructureDamage", 0);
	structureradius = cfg->GetInt(ch, "superweapon", "NukeStructureRadius", 0);

	if (sound)
		chat->SendArenaSoundMessage(arena, sound, "WARNING: Nuclear Missile launched");
	else
		chat->SendArenaMessage(arena, "WARNING: Nuclear Missile launched");
	//util->SFXToNearbyPlayers(arena, 55, x, y, 1, 1, 1600);

	Player *killer = dodamage->MapDamageN(arena, &tgt, "<Nuclear Missile>", x, y, W_THOR, 3, 31, 3, false);

	Istaticturret *turret = mm->GetInterface(I_STATICTURRET, arena);
	if (turret)
		turret->DoDamage(arena, killer, x, y, turretdamage, turretradius, friendlyfreq);
	mm->ReleaseInterface(turret);

	Istructures *structures = mm->GetInterface(I_STRUCTURES, arena);
	if (structures)
		structures->DoDamage(arena, x, y, structuredamage, structureradius, friendlyfreq);
	mm->ReleaseInterface(structures);

	if (nukeObj >= 0)
	{
		util->ObjMove(arena, nukeObj, x+xoffset, y+yoffset, 0, 0, false);
		util->ObjToggle(arena, nukeObj, 1, false);
	}

	if (screenObj >= 0)
		util->ObjToggle(arena, screenObj, 1, false);
}

static void NukeFireSequence(Player *p, int refundCredits, int freqChargeRequired)
{
	Target tgtPlayer;
	struct playerData* pdata;

	tgtPlayer.type = T_PLAYER;
	tgtPlayer.u.p = p;
	pdata = GetPlayerData(p);

	pdata->inFireSequence = true;
	pdata->oldShip = p->p_ship;
	pdata->aimingWeapon = SUPERWEAPON_NUKE;
	pdata->refundCredits = refundCredits;
	pdata->freqChargeRequired = freqChargeRequired;
	game->SetShip(p, SHIP_SPEC); //spec him

	// does not work:
	game->WarpTo(&tgtPlayer, p->position.x, p->position.x); // have the player spec its old location instead of the first player in the arena list

	//game->Lock(&tgtPlayer, 0, 0, 0);

	objs->Toggle(&tgtPlayer, 1952, 1); // map
	objs->Toggle(&tgtPlayer, 1953, 1); // screen
	pdata->lastFireX = -1;
	pdata->lastFireY = -1;
	pdata->lastFireObjectUpdate = current_ticks();

}

static int GetChargeFreq(Arena *arena, int freq, Superweapon superweapon)
{
	struct freqData *fdata;
	fdata = GetFreqData(arena, freq, false);
	if (!fdata)
	{
		return 0;
	}
	if (fdata->superweapons[superweapon].chargeStartAt)
	{
		return fdata->superweapons[superweapon].nukeCharge + TICK_DIFF(current_ticks(), fdata->superweapons[superweapon].chargeStartAt);
	}

	return fdata->superweapons[superweapon].nukeCharge;
}

static int GetChargePlayer(Player *p, Superweapon superweapon)
{
	struct freqData *fdata;
	fdata = GetPlayerFreqData(p, p->p_freq, false);
	if (!fdata)
		return 0;

	if (fdata->superweapons[superweapon].chargeStartAt)
		return fdata->superweapons[superweapon].nukeCharge + TICK_DIFF(current_ticks(), fdata->superweapons[superweapon].chargeStartAt);

	return fdata->superweapons[superweapon].nukeCharge;
}

static void StructuresTypeDataStructureCountChangedCB(struct structureTypeData *stdata, int oldStructureCount)
{
	struct freqData *fdata;
	ticks_t now;
	now = current_ticks();
	Link *link;
	Player *p;
	Superweapon superweapon;

	if (stdata->type == NUKE)
	{
		superweapon = SUPERWEAPON_NUKE;

		if (stdata->structureCount > 0 && oldStructureCount <= 0) // a nuke silo was built
		{

			fdata = GetFreqData(stdata->arena, stdata->freq, true);
			fdata->superweapons[superweapon].chargeStartAt = now;


			pd->Lock();
			FOR_EACH_PLAYER(p)
			{
				if (p->arena == stdata->arena && p->p_freq == stdata->freq)
				{
					fdata = GetPlayerFreqData(p, stdata->freq, true);
					fdata->superweapons[superweapon].chargeStartAt = now;
				}
			}
			pd->Unlock();
		}
		else if (stdata->structureCount <= 0 && oldStructureCount > 0) // all nuke silo's destroyed
		{
			fdata = GetFreqData(stdata->arena, stdata->freq, false);
			if (fdata)
			{
				if (fdata->superweapons[superweapon].chargeStartAt)
					fdata->superweapons[superweapon].nukeCharge += TICK_DIFF(now, fdata->superweapons[superweapon].chargeStartAt);
				fdata->superweapons[superweapon].chargeStartAt = 0;
			}

			pd->Lock();
			FOR_EACH_PLAYER(p)
			{
				if (p->arena == stdata->arena)
				{
					if ( (fdata = GetPlayerFreqData(p, stdata->freq, false)) )
					{
						if (fdata->superweapons[superweapon].chargeStartAt)
							fdata->superweapons[superweapon].nukeCharge += TICK_DIFF(now, fdata->superweapons[superweapon].chargeStartAt);
						fdata->superweapons[superweapon].chargeStartAt = 0;
					}

					if (p->p_freq == stdata->freq)
					{
						CancelFireSequence(p, true);
					}
				}
			}
			pd->Unlock();
		}
	}

}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	struct freqData *fdata, *afdata;
	Superweapon superweapon;
	ticks_t now;
	now = current_ticks();
	if (newship != SHIP_SPEC || newfreq != oldfreq)
		CancelFireSequence(p, true);


	if (newfreq != oldfreq) // do not recharge superweapons if you are not on the freq
	{
		if ( (fdata = GetPlayerFreqData(p, oldfreq, false)) )
		{
			for (superweapon = 0; superweapon < SUPERWEAPONS; superweapon++)
			{
				if (fdata->superweapons[superweapon].chargeStartAt)
					fdata->superweapons[superweapon].nukeCharge += TICK_DIFF(now, fdata->superweapons[superweapon].chargeStartAt);
				fdata->superweapons[superweapon].chargeStartAt = 0;
			}
		}

		if ( (afdata = GetFreqData(p->arena, newfreq, false)) )
		{
			fdata = NULL;
			for (superweapon = 0; superweapon < SUPERWEAPONS; superweapon++)
			{
				if (afdata->superweapons[superweapon].chargeStartAt) // nuke is currently recharging, restart charging for this player
				{
					if (!fdata)
						fdata = GetPlayerFreqData(p, newfreq, true);

					if (!fdata->superweapons[superweapon].chargeStartAt)
						fdata->superweapons[superweapon].chargeStartAt = now;
				}
			}
		}
	}
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	if (action == PA_LEAVEARENA)
	{
		CancelFireSequence(p, true);
		DestructPlayerData(GetPlayerData(p));
	}
}

static void NewPlayerCB(Player *p, int isnew)
{
	struct playerData *pdata;
	if (!isnew)
	{
		pdata = PPDATA(p, playerKey);
		if (pdata->arena)
			DestructPlayerData(pdata);
	}
}

static void StartGame(Arena *arena)
{
	StopGame(arena);
	//...
}

static void StopGame(Arena *arena)
{
	Link *link;
	struct playerData *pdata;
	struct arenaData *adata;
	Player *p;

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->arena == arena)
		{
			CancelFireSequence(p, false);
			pdata = GetPlayerData(p);
			DestructPlayerData(pdata);
		}
	}
	pd->Unlock();

	adata = GetArenaData(arena);
	DestructArenaData(adata);
	ConstructArenaData(adata);
}

static int fire_timer(Player *p)
{
	struct playerData* pdata;
	Target tgtPlayer;

	tgtPlayer.type = T_PLAYER;
	tgtPlayer.u.p = p;
	pdata = GetPlayerData(p);
	pdata->fireTimer = false;
	if (p->p_ship == SHIP_SPEC) // player already went into a ship himself
		game->SetShip(p, pdata->oldShip);

	return 0;
}


static void CancelFireSequence(Player *p, bool refund)
{
	struct playerData *pdata;
	Igamecredits *credits;
	Target tgtPlayer;

	pdata = GetPlayerData(p);
	tgtPlayer.type = T_PLAYER;
	tgtPlayer.u.p = p;

	if (pdata->inFireSequence)
	{
		if (refund && pdata->refundCredits)
		{
			credits = mm->GetInterface(I_GAMECREDITS, p->arena);
			if (credits)
				credits->AddCreditsPlayer(p, p->p_freq, pdata->refundCredits, false, false);
			mm->ReleaseInterface(credits);
		}

		pdata->refundCredits = 0;
		//game->Unlock(&tgtPlayer, 0);
		objs->Toggle(&tgtPlayer, 1952, 0);
		objs->Toggle(&tgtPlayer, 1953, 0);
	}

	pdata->inFireSequence = false;
	if (pdata->fireTimer);
		ml->ClearTimer((TimerFunc) fire_timer, p);
	pdata->fireTimer = false;
}


static void Cfire(const char *tc, const char *params, Player *p, const Target *target)
{
	struct playerData* pdata;
	struct freqData *fdata;
	Target tgtPlayer;
	int charge;
	Superweapon superweapon;

	tgtPlayer.type = T_PLAYER;
	tgtPlayer.u.p = p;
	pdata = GetPlayerData(p);
	superweapon = pdata->aimingWeapon;

	charge = GetChargeFreq(p->arena, p->p_freq, superweapon);
	if (pdata->freqChargeRequired > charge)
	{
		chat->SendCmdMessage(p, "The %s has not finnished charging yet, please wait %d seconds", superweapon_names[superweapon], (pdata->freqChargeRequired - charge)/100);
		return;
	}

	pdata->refundCredits = 0;
	if ( (fdata = GetFreqData(p->arena, p->p_freq, false)) )
	{
		fdata->superweapons[superweapon].chargeStartAt = current_ticks();
		fdata->superweapons[superweapon].nukeCharge = 0;
	}

	if ( (fdata = GetPlayerFreqData(p, p->p_freq, false)) )
	{
		fdata->superweapons[superweapon].chargeStartAt = current_ticks();
		fdata->superweapons[superweapon].nukeCharge = 0;
	}

	if (superweapon == SUPERWEAPON_NUKE)
	{
		Nuke(p->arena, p->position.x, p->position.y, p->p_freq);
	}

	CancelFireSequence(p, false);
	ml->SetTimer((TimerFunc) fire_timer, 300, 0, p, p);
	pdata->fireTimer = true;
}


static void Cnukefiresequence(const char *tc, const char *params, Player *p, const Target *target)
{
	NukeFireSequence(p, 0, 0);
}


static void MainLoopCB()
{
	Player *p;
	Link *link;
	struct playerData *pdata;
	ticks_t now;
	Target tgtPlayer;
	tgtPlayer.type = T_PLAYER;

	now = current_ticks();


	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		tgtPlayer.u.p = p;
		pdata = GetPlayerData(p);
		if (pdata->inFireSequence)
		{
			pdata->lastFireObjectUpdate = current_ticks();

			if (pdata->lastFireX != p->position.x || pdata->lastFireY != p->position.y)
				objs->Move(&tgtPlayer, 1952, (pdata->lastFireX = p->position.x) - 41, (pdata->lastFireY = p->position.y) - 33, 0, 0);
		}
	}
	pd->Unlock();
}


EXPORT const char info_superweapon[] = "superweapon by JoWie ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(cmd     );
        mm->ReleaseInterface(chat    );
        mm->ReleaseInterface(fake    );
        mm->ReleaseInterface(cmd     );
        mm->ReleaseInterface(cfg     );
        mm->ReleaseInterface(lm      );
        mm->ReleaseInterface(aman    );
        mm->ReleaseInterface(dodamage);
        mm->ReleaseInterface(pd      );
        mm->ReleaseInterface(game    );
        mm->ReleaseInterface(util    );
        mm->ReleaseInterface(objs    );
        mm->ReleaseInterface(ml      );
}

EXPORT int MM_superweapon(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;

                chat     = mm->GetInterface(I_CHAT      , ALLARENAS);
                cmd      = mm->GetInterface(I_CMDMAN    , ALLARENAS);
                fake     = mm->GetInterface(I_FAKE      , ALLARENAS);
                lm       = mm->GetInterface(I_LOGMAN    , ALLARENAS);
                cfg      = mm->GetInterface(I_CONFIG    , ALLARENAS);
                aman     = mm->GetInterface(I_ARENAMAN  , ALLARENAS);
                dodamage = mm->GetInterface(I_DODAMAGE  , ALLARENAS);
                pd       = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
                game     = mm->GetInterface(I_GAME      , ALLARENAS);
                util     = mm->GetInterface(I_UTIL      , ALLARENAS);
                objs     = mm->GetInterface(I_OBJECTS   , ALLARENAS);
                ml       = mm->GetInterface(I_MAINLOOP  , ALLARENAS);

                if (!chat || !cmd || !fake || !lm || !cfg || !aman || !dodamage || !pd || !game || !util || !objs || !ml)
                {
                        printf("<superweapon> Missing Interface:\n");

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));

                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));
		if (playerKey == -1)
		{
			aman->FreeArenaData(arenaKey);
			ReleaseInterfaces();
			return MM_FAIL;
		}
                mm->RegCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);
                mm->RegCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);


                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);
                mm->UnregCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);
                mm->UnregCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);

                ReleaseInterfaces();
                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
		ConstructArenaData(GetArenaData(arena));

		mm->RegInterface(&superweapon, arena);
		mm->RegCallback(CB_STRUCTURES_TYPEDATASTRUCTURECOUNTCHANGED, StructuresTypeDataStructureCountChangedCB, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);

                cmd->AddCommand("nuke", Cnuke, arena, Cnuke_help);
                cmd->AddCommand("nukef", Cnukefiresequence, arena, NULL);
                cmd->AddCommand("fire", Cfire, arena, NULL);
        }
        else if (action == MM_DETACH)
        {
        	if (mm->UnregInterface(&superweapon, arena))
                        return MM_FAIL;

		mm->UnregCallback(CB_STRUCTURES_TYPEDATASTRUCTURECOUNTCHANGED, StructuresTypeDataStructureCountChangedCB, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);

		cmd->RemoveCommand("nuke", Cnuke, arena);
                cmd->RemoveCommand("nukef", Cnukefiresequence, arena);
                cmd->RemoveCommand("fire", Cfire, arena);


		DestructArenaData(GetArenaData(arena));
        }

        return MM_FAIL;
}
