#ifndef __HARVEST_H
#define __HARVEST_H

#include <stdbool.h>

#define I_HARVEST "harvest-1"

typedef struct Iharvest
{
	INTERFACE_HEAD_DECL

	void (*StartGame)(Arena *arena);
	void (*StopGame)(Arena *arena);
	

} Iharvest;

#endif

