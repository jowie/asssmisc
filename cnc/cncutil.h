#ifndef __CNCUTIL_H
#define __CNCUTIL_H

#include "asss.h"
#include <stdbool.h>

#define I_UTIL "cncutil-2"

/** Make sure an integral variable is within a specific range */
#define MIN(variable, min) if (variable < (min)) variable = (min)
#define MAX(variable, max) if (variable > (max)) variable = (max)


/* Example:
    int a = -400
    MIN(a, -1);
    a == -1
*/

typedef struct Iutil
{
        INTERFACE_HEAD_DECL

        /** quick integer square root */
        long (*LHypot)(register long dx, register long dy);

        /** get ship names as they are used in settings; 0 = warbird; 8 = spectator; Example: Warbird:FireDelay */
        const char *(*GetShipName)(unsigned int ship);

        /** get ship names as set in settings; Uses Warbird:Name;*/
        const char *(*GetCustomShipName)(Arena *arena, unsigned int ship);

        void (*AssertLM)(const char* e, const char* file, const char *function, int line);
        void (*FunctionLogLM)(const char *module, int line, const char *func, const char *info);

        void (*SFXToNearbyPlayers)(Arena *arena, int sound, int x, int y, int width, int height, int range);
        void (*PrizeNearbyPlayers)(Arena *arena, int prize, int x, int y, int width, int height, int range);

        void (*GetArenaSet)(LinkedList *set, Arena *arena);
        void (*GetFreqSet)(LinkedList *set, Arena *arena, int freq);
        void (*GetNotonFreqSet)(LinkedList *set, Arena *arena, int freq);

        //make sure to call afree() on the return value at some point unless it is NULL
        char* (*cfgGetStrAlloc)(ConfigHandle ch, const char *section, const char *key);
        char* (*strcpyalloc)(const char *s);


        void (*ObjToggle)(Arena *arena, int id, int on, bool saveState);
        void (*ObjToggleSet)(Arena *arena, short *id, char *ons, int size, bool saveState);
        void (*ObjMove)(Arena *arena, int id, int x, int y, int rx, int ry, bool saveState);
        void (*ObjImage)(Arena *arena, int id, int image, bool saveState);
        void (*ObjLayer)(Arena *arena, int id, int layer, bool saveState);
        void (*ObjTimer)(Arena *arena, int id, int time, bool saveState);
        void (*ObjMode)(Arena *arena, int id, int mode, bool saveState);
} Iutil;


#ifdef NDEBUG
//Debugging disabled
#define assertlm(x)	    ((void)0)
#define FNSTART         ((void)0)
#define FNEND           ((void)0)
#define DEBUGSTEP       ((void)0)

#else

//#define assertlm(e)       ((e) ? (void)0 : util->assertlmfunc(#e, __FILE__, __LINE__))

//taken from assert.h
//requires the global setting "log_whatever:all = E"
//only use this when in the same thread!
#define assertlm(e) \
        if (!(e)) \
        { \
                if (util) { \
                        util->AssertLM(#e, __FILE__, __func__, __LINE__); \
                } else { \
                        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", #e, __FILE__, __LINE__, __func__); \
                } \
        } \

//requires the global setting "log_whatever:mymodule = D"
#define FNSTART util->FunctionLogLM(MODULE, __LINE__, __func__, NULL)
#define FNEND util->FunctionLogLM(MODULE, __LINE__, "\\"__func__, NULL)
#define DEBUGSTEP util->FunctionLogLM(MODULE, __LINE__, __func__, "DEBUGSTEP")

#endif

#endif // __CNCUTIL_H
