/* sgm_cnc2 (attach module)
 * hacked up sgm_cnc, so you have the structure game and stats but no voting
 * required modules: structures
 *
 * commands: ?newgame (stops current game and starts a vote)
 *
 * settings:

[sgm_cnc]
RoundPauseSeconds = 40
RoundStartSound = 0

VictorySound = 0
VictoryObjectID = 0
DefeatSound = 0
DefeatObjectID = 0

 *
 * jun 18 2007 Smong: branched from sgm_cnc
 * jul 1  2007 JoWie: fixed a copy-paste typos in cb_find_best() which caused most deaths and most tks to always be the same person as the one that has most kills
 * sep 14 2007 JoWie: fixed for asss 1.4.4: (arena->status != ARENA_DO_DEINIT) becomes (arena->status < ARENA_CLOSING)
 * nov 22 2007 JoWie: fixed a crashing bug which occurs in combination with fake players
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "asss.h"
#include "swappablegame.h"
#include "objects.h"

#include "structures.h"


#define GAME_COUNT 1

struct adata
{
	int votecount[GAME_COUNT];
	int votesexpected;
	int votesreceived;

	Iswappablegame *sgame;
	int currentgameid;

	/* settings */
	int cfg_roundpause;
	int cfg_roundstartsound;

	int cfg_victorysound, cfg_victoryobjectid;
	int cfg_defeatsound, cfg_defeatobjectid;

	int cfg_noticesound;

	struct
	{
		int x;
		int y;
		int radius;
	} spawn[4];

	/* stats */
	HashTable *statset;
};

struct beststatdata
{
	struct statdata *mostkills;
	struct statdata *mostdeaths;
	struct statdata *mosttks;
	struct statdata *highestrating;
	int bestrating;
};

/* track stats for everyone that played, not just current players */
struct statdata
{
	int kills;
	int deaths;
	int tks;
	int killpoints;
	/* rating is derived, use HashEnum with cb_find_best to update it */
	/* Rating = (points from kills x 10 + (wins-losses) x 100) / (wins + 100)  */
	int rating;
	char playername[32];
};

local void Cnewgame(const char *command, const char *params, Player *p,	const Target *target);

local void ArenaAction(Arena *arena, int action);
local void cleanup_settings(Arena *arena);
local void read_settings(Arena *arena);

local void real_start_game(Arena *arena);
local int round_pause_timer1(void *arena_);
local int round_pause_timer2(void *arena_);

local void Gameover(Arena *arena, const Target *tgt);

local struct statdata * get_player_stats(Arena *arena, const char *playername);
local void print_stats(Arena *arena, int winningfreq);
local int cb_find_best(const char *key, void *val, void *clos);
local void Kill(Arena *arena, Player *killer, Player *killed,
		int bounty, int flags, int pts, int green);

local int adkey;

local Imodman *mm;
local Ilogman *lm;
local Iarenaman *aman;
local Iconfig *cfg;
local Iplayerdata *pd;
local Ichat *chat;
local Imainloop *ml;
local Iprng *prng;
local Iobjects *objs;
local Igame *game;
local Icmdman *cmd;
local Iballs *balls;
local Inet *net;

local const struct
{
	const char *iid;
	const char *votename;
	const char *helptext;
} game_info[] =
{
	/* don't edit this, if you do you probably want sgm_cnc not sgm_cnc2 */
	{ I_SWAPPABLEGAME_STRUCTURES, "Base war", "Destroy the enemy base" },
};


/* settings stuff */

local void ArenaAction(Arena *arena, int action)
{
	if ((action == AA_CREATE || AA_CONFCHANGED) &&
		arena->status < ARENA_CLOSING)
	{
		read_settings(arena);
	}
	else if (action == AA_DESTROY)
	{
		cleanup_settings(arena);
	}
}

local void cleanup_settings(Arena *arena)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);

	if (ad->sgame)
	{
		lm->LogA(L_DRIVEL, "sgm_cnc2", arena, "cleanup_settings: releasing interface");
		ad->sgame->StopGame(arena);
		mm->ReleaseInterface(ad->sgame);
		ad->sgame = NULL;
		ad->currentgameid = -1;
	}

	if (ad->statset)
	{
		HashEnum(ad->statset, hash_enum_afree, NULL);
	    HashFree(ad->statset);
	    ad->statset = NULL;
	}
}

local void read_settings(Arena *arena)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	int a;
	ConfigHandle ch = arena->cfg;

	ad->cfg_roundpause = cfg->GetInt(ch, "sgm_cnc", "RoundPauseSeconds", 40) * 100;
	ad->cfg_roundstartsound = cfg->GetInt(ch, "sgm_cnc", "RoundStartSound", 0);

	ad->cfg_victorysound = cfg->GetInt(ch, "sgm_cnc", "VictorySound", 0);
	ad->cfg_victoryobjectid = cfg->GetInt(ch, "sgm_cnc", "VictoryObjectID", 0);
	ad->cfg_defeatsound = cfg->GetInt(ch, "sgm_cnc", "DefeatSound", 0);
	ad->cfg_defeatobjectid = cfg->GetInt(ch, "sgm_cnc", "DefeatObjectID", 0);

	ad->cfg_noticesound = cfg->GetInt(ch, "sgm_cnc", "NoticeSound", 0);


	char xname[] = "Spawn#-X";
	char yname[] = "Spawn#-Y";
	char rname[] = "Spawn#-Radius";

	for (a = 0; a < 4; a++)
	{
		xname[5] = yname[5] = rname[5] = '0' + a;

		ad->spawn[a].x = cfg->GetInt(ch, "sgm_cnc", xname, -1);
		ad->spawn[a].y = cfg->GetInt(ch, "sgm_cnc", yname, -1);
		ad->spawn[a].radius = cfg->GetInt(ch, "sgm_cnc", rname, 0);
		if (ad->spawn[a].radius < 0) ad->spawn[a].radius = 0;
	}

	/* checks */
	if (ad->cfg_roundpause < 0)
		ad->cfg_roundpause = 0;
}

/* vote stuff */

local void real_start_game(Arena *arena)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	Target tgt, tgtP;
	Player *p;
	Link *link;
	int a, cnt;

	/* reset stats */
	if (ad->statset)
	{
		HashEnum(ad->statset, hash_enum_afree, NULL);
		HashFree(ad->statset);
		ad->statset = NULL;
	}
	ad->statset = HashAlloc();

	// warp everyone
	tgt.type = T_ARENA;
	tgt.u.arena = arena;
	game->ShipReset(&tgt);

	cnt = 0;
	while(cnt < 4)
	{
		if (ad->spawn[cnt].x > 0 && ad->spawn[cnt].y > 0)
			cnt++;
		else
			break;
	}

	if (cnt)
	{
		cnt++;

		tgtP.type = T_PLAYER;

		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena && IS_HUMAN(p) && p->p_ship != SHIP_SPEC && p->p_freq >= 0)
			{
				tgtP.u.p = p;
				a = p->p_freq % cnt;
				game->WarpTo(&tgtP,
				             prng->Number(ad->spawn[a].x - ad->spawn[a].radius, ad->spawn[a].x + ad->spawn[a].radius),
				             prng->Number(ad->spawn[a].y - ad->spawn[a].radius, ad->spawn[a].y + ad->spawn[a].radius)
				             );
			}
		}
		pd->Unlock();
	}
	else
	{
		game->GivePrize(&tgt, PRIZE_WARP, 1);
	}


	if (balls) balls->EndGame(arena);

	/* try and load the game interface and start the game */
	ad->sgame = mm->GetInterface(game_info[ad->currentgameid].iid, arena);
	if (ad->sgame)
	{
		lm->LogA(L_DRIVEL, "sgm_cnc2", arena, "got game interface (%s,%s)",	game_info[ad->currentgameid].iid, game_info[ad->currentgameid].votename);

		if (ad->cfg_roundstartsound)
			chat->SendArenaSoundMessage(arena, ad->cfg_roundstartsound, "%s", "");

		chat->SendArenaMessage(arena, "Game started. Destroy the enemy base to win!");
		ad->sgame->StartGame(arena, Gameover);
	}
	else
	{
		lm->LogA(L_ERROR, "sgm_cnc2", arena, "game interface missing (%s,%s)",
			game_info[ad->currentgameid].iid, game_info[ad->currentgameid].votename);
	}
}

local int round_pause_timer1 (void *arena_)
{
	//give 10 seconds notice
	Arena *arena = arena_;
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	chat->SendArenaSoundMessage(arena, ad->cfg_noticesound, "%s", "");

	chat->SendArenaMessage(arena, "NOTICE: Game starting in 10 seconds.");
	ml->SetTimer(round_pause_timer2, 1000, 0, arena, arena);

	return 0;
}

local int round_pause_timer2 (void *arena_)
{
	Arena *arena = arena_;
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	ad->currentgameid = 0;
	real_start_game(arena);
	return 0;
}

local void Gameover(Arena *arena, const Target *targ)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	Target tgt;

	/* release old game interface */
	if (ad->sgame)
	{
		lm->LogA(L_DRIVEL, "sgm_cnc2", arena, "Gameover: releasing interface");
		ad->sgame->StopGame(arena); /* stop it for good measure */
		mm->ReleaseInterface(ad->sgame);
		ad->sgame = NULL;
		ad->currentgameid = -1;
	}

	/* do the winners/losers sound/lvz thing */
	if (targ && targ->type == T_FREQ)
	{
		Player *p;
		Link *link;
		int winfreq = targ->u.freq.freq;
		int losefreq = 1 - winfreq;

		pd->Lock();
		FOR_EACH_PLAYER(p)
			if (p->status == S_PLAYING &&
				p->arena == arena &&
				p->p_ship != SHIP_SPEC &&
				IS_HUMAN(p))
			{
				if (p->p_freq == winfreq)
				{
					if (ad->cfg_victorysound)
						chat->SendSoundMessage(p, ad->cfg_victorysound, "%s", "");
				}
				else if (p->p_freq == losefreq)
				{
					if (ad->cfg_defeatsound)
						chat->SendSoundMessage(p, ad->cfg_defeatsound, "%s", "");
				}
			}
		pd->Unlock();

		if (objs)
		{
			/* can just re-use the targ here */
			if (ad->cfg_victoryobjectid)
				objs->Toggle(targ, ad->cfg_victoryobjectid, 1);

			if (ad->cfg_defeatobjectid)
			{
				tgt.type = T_FREQ;
				tgt.u.freq.arena = arena;
				tgt.u.freq.freq = losefreq;
				objs->Toggle(&tgt, ad->cfg_defeatobjectid, 1);
			}
		}

		/* print round end stats */
		print_stats(arena, targ->u.freq.freq);
	}



	/* start vote for next game */
	chat->SendArenaSoundMessage(arena, ad->cfg_noticesound, "%s", "");
	chat->SendArenaMessage(arena, "NOTICE: Next game in %d seconds.", (int) (ad->cfg_roundpause / 100) + 10); //add 10 for the 10 seconds notice
	ml->SetTimer(round_pause_timer1, ad->cfg_roundpause, 0, arena, arena);
}


local helptext_t newgame_help =
	"Module: sgm_cnc2\n"
	"Basically restarts the game.";

local void Cnewgame(const char *command, const char *params, Player *p,	const Target *target)
{
	struct adata *ad = P_ARENA_DATA(p->arena, adkey);
	chat->SendMessage(p, "Forcing the start of a new game...");

	/* release old game interface */
	if (ad->sgame)
	{
		lm->LogA(L_DRIVEL, "sgm_cnc2", p->arena, "Cnewgame: releasing interface");
		ad->sgame->StopGame(p->arena); /* stop it for good measure */
		mm->ReleaseInterface(ad->sgame);
		ad->sgame = NULL;
		ad->currentgameid = -1;
	}

	/* clear any previous timers */
	ml->ClearTimer(round_pause_timer1, p->arena);
	ml->ClearTimer(round_pause_timer2, p->arena);

	/* start the game */
	ml->SetTimer(round_pause_timer1, 0, 0, p->arena, p->arena); //give 10 second notice
}


/* stats tracking */

local struct statdata * get_player_stats(Arena *arena, const char *playername)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	struct statdata *sdata = NULL;

	if (!ad->statset) //JoWie: This was causing a segfault in combination with fake players
	{
        ad->statset = HashAlloc();
	}

	sdata = HashGetOne(ad->statset, playername); //BACKTRACE SEGFAULT #1 -> ad->statset is 0x0

	if (sdata == NULL)
	{
		sdata = amalloc(sizeof(*sdata));
		astrncpy(sdata->playername, playername, sizeof(sdata->playername));
		HashAdd(ad->statset, playername, sdata);
	}

	return sdata;
}

local int cb_find_best(const char *key, void *val, void *clos)
{
	struct beststatdata *bsd = clos;
	struct statdata *sdata = val;
	int rating;

	rating = sdata->killpoints * 10;
	rating += (sdata->kills - sdata->deaths) * 100;
	rating *= 10; /* store rating as x10 for 1 dp precision */
	rating /= sdata->kills + 100;

	if (rating < 0)
		rating = 0;

	sdata->rating = rating;

	if (!bsd->mostkills || bsd->mostkills->kills < sdata->kills)
		bsd->mostkills = sdata;
	if (!bsd->mostdeaths || bsd->mostdeaths->kills < sdata->deaths)
		bsd->mostdeaths = sdata;
	if (!bsd->mosttks || bsd->mosttks->kills < sdata->tks)
		bsd->mosttks = sdata;

	if (!bsd->highestrating || bsd->bestrating < rating)
	{
        bsd->highestrating = sdata;
        bsd->bestrating = rating;
	}

	return FALSE;
}

local void print_stats(Arena *arena, int winningfreq)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	struct statdata *sdata;
	struct beststatdata bsd;
	Player *p;
	Link *link;

    if (!ad->statset) return; //JoWie: Just to be safe

	memset(&bsd, 0, sizeof(bsd));
    HashEnum(ad->statset, cb_find_best, &bsd);

	/* print winning team stats. for some reason the first line needs an extra
	 * trailing space. */
	chat->SendArenaSoundMessage(arena, ad->cfg_noticesound, "%s", "");
	chat->SendArenaMessage(arena, "Player                 W   L   TK   R      ");
	chat->SendArenaMessage(arena, "---------------------------------------");
	//chat->SendArenaMessage(arena, "winningfreq %d", winningfreq);
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->status == S_PLAYING &&
			p->arena == arena &&
			p->p_ship != SHIP_SPEC &&
			IS_HUMAN(p) &&
			p->p_freq == winningfreq)
		{
            //chat->SendArenaMessage(arena, "if ok (p=%s,f=%d)", p->name, p->p_freq);
            sdata = get_player_stats(arena, p->name);
			chat->SendArenaMessage(arena, "%-20s %3d %3d %3d %4d.%1d",
				sdata->playername, sdata->kills, sdata->deaths, sdata->tks,
				sdata->rating / 10, sdata->rating % 10);
		}
		//else
        //    chat->SendArenaMessage(arena, "if not ok (p=%s,f=%d)", p->name, p->p_freq);
	}
	pd->Unlock();
	chat->SendArenaMessage(arena, "---------------------------------------");

	/* print top stats */
	if (bsd.mostkills && bsd.mostkills->kills > 0)
	{
		chat->SendArenaMessage(arena, "[W] Most kills: %s (%d)",
			bsd.mostkills->playername, bsd.mostkills->kills);
	}
	if (bsd.mostdeaths && bsd.mostdeaths->deaths > 0)
	{
		chat->SendArenaMessage(arena, "[L] Most deaths: %s (%d)",
			bsd.mostdeaths->playername, bsd.mostdeaths->deaths);
	}
	if (bsd.mosttks && bsd.mosttks->tks > 0)
	{
		chat->SendArenaMessage(arena, "[TK] Most TKs: %s (%d)",
			bsd.mosttks->playername, bsd.mosttks->tks);
	}
	if (bsd.highestrating && bsd.bestrating > 0)
	{
		chat->SendArenaMessage(arena, "[R] Highest rating: %s (%d.%d)",
			bsd.highestrating->playername, bsd.bestrating / 10, bsd.bestrating % 10);
	}
}

local void Kill(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
	struct statdata *sdata_killer;
	struct statdata *sdata_killed;

	if (IS_HUMAN(killer))
	{
		sdata_killer = get_player_stats(arena, killer->name);
		sdata_killer->kills++;
		sdata_killer->killpoints += pts;

		if (killer->p_freq == killed->p_freq)
			sdata_killer->tks++;
	}

	if (IS_HUMAN(killed))
	{
		sdata_killed = get_player_stats(arena, killed->name); //BACKTRACE SEGFAULT #2
		sdata_killed->deaths++;
	}
}


EXPORT const char info_sgm_cnc2[] =
	"sgm_cnc2 v1.1 smong <pfft.whatever@gmail.com>\n"
	"based on sgm_cnc v1.3 smong <pfft.whatever@gmail.com>";

EXPORT int MM_sgm_cnc2(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		/* objects module optionally loaded */
		mm = mm_;
		lm = mm->GetInterface(I_LOGMAN, ALLARENAS);
		aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		chat = mm->GetInterface(I_CHAT, ALLARENAS);
		ml = mm->GetInterface(I_MAINLOOP, ALLARENAS);
		prng = mm->GetInterface(I_PRNG, ALLARENAS);
		objs = mm->GetInterface(I_OBJECTS, ALLARENAS);
		game = mm->GetInterface(I_GAME, ALLARENAS);
		cmd = mm->GetInterface(I_CMDMAN, ALLARENAS);
		balls = mm->GetInterface(I_BALLS, ALLARENAS); //optional
		net = mm->GetInterface(I_NET, ALLARENAS);

		if (!lm || !aman || !cfg || !pd || !chat || !ml || !prng ||
			!game || !cmd || !net)
			return MM_FAIL;
		if (!objs)
			lm->Log(L_WARN, "<sgm_cnc2> objects module not loaded");
		adkey = aman->AllocateArenaData(sizeof(struct adata));
		if (adkey == -1) return MM_FAIL;
		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		ml->ClearTimer(round_pause_timer1, NULL);
		ml->ClearTimer(round_pause_timer2, NULL);
		aman->FreeArenaData(adkey);
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(prng);
		mm->ReleaseInterface(objs);
		mm->ReleaseInterface(game);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(balls);
		mm->ReleaseInterface(net);
		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		struct adata *ad = P_ARENA_DATA(arena, adkey);
		ad->currentgameid = -1;
		cmd->AddCommand("newgame", Cnewgame, arena, newgame_help);
		mm->RegCallback(CB_ARENAACTION, ArenaAction, arena);
		/* using post notify because we want to track the kill points */
		mm->RegCallback(CB_KILL, Kill, arena);
		/* start vote for next game */
		ml->SetTimer(round_pause_timer1, 1000, 0, arena, arena);
		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		cmd->RemoveCommand("newgame", Cnewgame, arena);
		mm->UnregCallback(CB_ARENAACTION, ArenaAction, arena);
		mm->UnregCallback(CB_KILL, Kill, arena);
		ml->ClearTimer(round_pause_timer1, arena);
		ml->ClearTimer(round_pause_timer2, arena);
		return MM_OK;
	}
	return MM_FAIL;
}
