/**
    DoDamage v1.0 by JoWie

    Allows other modules to hurt players in various ways.

*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "asss.h"
#include "dodamage.h"
#include "fake.h"

#define FAKELEAVEAFTER (400)

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __func__, __LINE__)
#endif

static void AssertLM                    (const char* e, const char* file, const char *function, int line);
static void Lock                        ();
static void Unlock                      ();
static void WeaponsPacketChecksum       (struct S2CWeapons *pkt);
static void MapDamage                   (const Target *victims, Player *killer, Player *blocker, int x, int y, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);
static Player* MapDamageN                  (Arena *arena, const Target *tgtVictims, const char *fakeName, int x, int y, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);
static void PlayerDamage                (const Target *victims, Player *killer, bool warpOutSafe, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);
static Player* PlayerDamageN               (Arena *arena, const Target *victims, const char *fakeName, bool warpOutSafe, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);

static pthread_mutex_t mymutex = PTHREAD_MUTEX_INITIALIZER;
static int arenaKey = -1;
static int playerKey = -1;

static Imodman     *mm;
static Iplayerdata *pd;
static Inet        *net;
static Igame       *game;
static Ilogman     *lm;
static Iarenaman   *aman;
static Iplayerdata *pd;
static Ifake       *fake;
static Icmdman     *cmd;
static Ichat       *chat;
static Idodamage    dodamage =
{
        INTERFACE_HEAD_INIT(I_DODAMAGE, "dodamage")

        MapDamage,
        MapDamageN,
        PlayerDamage,
        PlayerDamageN
};

struct arenaData
{
	HashTable *fakes;
};

struct playerData
{
	ticks_t lastUse;
};

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<dodamage> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct arenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey != -1);
        struct arenaData* adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);

        if (!adata->fakes)
		adata->fakes = HashAlloc();

        return adata;
}

static struct playerData* GetPlayerData(Player *p)
{
        assertlm(p);
        assertlm(playerKey != -1);
        struct playerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);

        return pdata;
}

static void Lock()
{
        pthread_mutex_lock(&mymutex);
}
static void Unlock()
{
        pthread_mutex_unlock(&mymutex);
}

static void ArenaActionCB(Arena *arena, int action)
{
	struct arenaData* adata;

	if (action == AA_DESTROY)
	{
		adata = GetArenaData(arena);
		if (adata->fakes)
			HashFree(adata->fakes);

		adata->fakes = NULL;
	}
}

static int fakes_enum_endFakes(const char *fakeName, Player *p, bool *endAll)
{
	struct playerData *pdata;
	if (*endAll)
	{
		fake->EndFaked(p);
		return 1; // remove it
	}
	else // only end if it expired
	{
		pdata = GetPlayerData(p);
		if (!pdata->lastUse || TICK_DIFF(current_ticks(), pdata->lastUse) > FAKELEAVEAFTER)
		{
			fake->EndFaked(p);
			return 1; // remove it
		}
	}

	return 0;
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
        if (action == PA_LEAVEARENA)
        {
                struct arenaData* adata = GetArenaData(p->arena);
                bool endAll = true;

                Link *link;
                Player *q;

                int players = 0;

                pd->Lock();
                FOR_EACH_PLAYER(q)
                {
                        if (q != p && q->arena == arena && IS_HUMAN(q))
                        {
                                players++;
                        }
                }
                pd->Unlock();


                if (!players)
                {
			HashEnum(adata->fakes, fakes_enum_endFakes, &endAll);
                }
        }
}

static void MainLoopCB()
{
	Arena *arena;
	struct arenaData *adata;
	bool endAll = false;
	Link *link;

	aman->Lock();
	FOR_EACH_ARENA(arena)
	{
		adata = GetArenaData(arena);
		if (adata->fakes)
			HashEnum(adata->fakes, fakes_enum_endFakes, &endAll);
	}
	aman->Unlock();
}

static void WeaponsPacketChecksum(struct S2CWeapons *pkt)
{
        unsigned i;
        u8 ck = 0;
        pkt->checksum = 0;
        for (i = 0; i < sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData); i++)
                ck ^= ((unsigned char*)pkt)[i];
        pkt->checksum = ck;
}

static void MapDamage(const Target *tgtVictims, Player *killer, Player *blocker, int x, int y, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing)
{
        Lock();
        assertlm(killer);
        assertlm(tgtVictims);
        assertlm(killer->arena);
        assertlm(killer->arena == blocker->arena);

        Arena *arena = killer->arena;

        LinkedList victims = LL_INITIALIZER;

        pd->Lock();
        pd->TargetToSet(tgtVictims, &victims);

        Link *l;
        Player *p;

#ifndef NDEBUG
        for (l = LLGetHead(&victims); l; l = l->next)
        {
                p = l->data;
                assertlm(p->arena == arena);
        }
#endif

        u8 status = STATUS_STEALTH | STATUS_CLOAK | STATUS_UFO;

        struct S2CWeapons wpnPacket =
        {
                S2C_WEAPON,                 //u8 type
                0,                          //i8 rotation
                (current_ticks() & 0xFFFF) - 1,//u16 time
                x,                          //i16 x
                0,                          //i16 yspeed
                killer->pid,                //u16 playerid
                0,                          //i16 xspeed
                0,                          //u8 checksum
                status,                     //u8 status
                0,                          //u8 c2slatency
                y,                          //i16 y
                0,                          //u16 bounty

                {
                        //struct Weapons weapon
                        weaponType,	        //u16 type
                        weaponLevel,            //u16 level : 2
                        shrapelBouncing,        //u16 shrapbouncing : 1
                        shrapnelLevel,	        //u16 shraplevel : 2
                        shrapnel,		//u16 shrap : 5
                        0  		        //u16 alternate : 1
                },

                {
                        //struct ExtraPosData extra
                        //I dont like warnings
                        0,0,0,0,0,0,0,0,0,0,0,0,0
                }
        };


        WeaponsPacketChecksum(&wpnPacket);

        for (l = LLGetHead(&victims); l; l = l->next)
        {
                p = l->data;

                if (IS_STANDARD(p) && p != killer)
                        net->SendToOne(p, (byte*)&wpnPacket, sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData), NET_RELIABLE);
        }

        struct S2CWeapons warpPacket =
        {
                S2C_WEAPON,
                10,
                (current_ticks() & 0xFFFF) + 1,
                x, //x
                0,
                blocker->pid,
                0,
                0,
                status, 0,
                y, //y
                p->position.bounty,
                { 0,0,0,0,0,0 },
                { 0,0,0,0,0,0,0,0,0,0,0,0,0 }
        };

        WeaponsPacketChecksum(&warpPacket);

        for (l = LLGetHead(&victims); l; l = l->next)
        {
                p = l->data;

                if (IS_STANDARD(p) && p != killer)
                        net->SendToOne(p, (byte*)&warpPacket, sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData), NET_RELIABLE);
        }

        LLEmpty(&victims);
        pd->Unlock();
        Unlock();
}

static Player* MapDamageN(Arena *arena, const Target *tgtVictims, const char *fakeName, int x, int y, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing)
{
	LinkedList fakes = LL_INITIALIZER;
	struct arenaData* adata;
	struct playerData* pdata;
	Player *killer, *blocker;
	Link *l;
	ticks_t now = current_ticks();

	adata = GetArenaData(arena);

	HashGetAppend(adata->fakes, fakeName, &fakes);
	killer = NULL;
	blocker = NULL;

	l = LLGetHead(&fakes);
	if (l)
	{
		killer = l->data;
		l = l->next;
		if (l)
			blocker = l->data;
	}
	LLEmpty(&fakes);

	if (!killer)
        {
                killer = fake->CreateFakePlayer(fakeName, arena, SHIP_WARBIRD, 9999);
                if (killer)
			HashAdd(adata->fakes, fakeName, killer);
        }

        if (!blocker)
        {
                blocker = fake->CreateFakePlayer(fakeName, arena, SHIP_WARBIRD, 9998);
                if (blocker)
			HashAdd(adata->fakes, fakeName, blocker);
        }

	if (killer && blocker)
	{
		pdata = GetPlayerData(killer);
		pdata->lastUse = now;
		pdata = GetPlayerData(blocker);
		pdata->lastUse = now;
		MapDamage(tgtVictims, killer, blocker, x, y, weaponType, weaponLevel, shrapnel, shrapnelLevel, shrapelBouncing);
	}
	else
	{
		lm->LogA(L_ERROR, "dodamage", arena, "Unable to create fake players");
	}
	return killer;
}

static void PlayerDamage(const Target *tgtVictims, Player *killer, bool warpOutSafe, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing)
{
        Lock();
        assertlm(killer);
        assertlm(tgtVictims);
        Arena *arena = killer->arena;
        assertlm(arena);

        ticks_t now = current_ticks();

        LinkedList victims = LL_INITIALIZER;

        pd->TargetToSet(tgtVictims, &victims);

        Link *l;
        Player *p;

#ifndef NDEBUG
        for (l = LLGetHead(&victims); l; l = l->next)
        {
                p = l->data;
                assertlm(p->arena == arena);
        }
#endif


        u8 status = STATUS_STEALTH | STATUS_CLOAK | STATUS_UFO;

        struct S2CWeapons wpnPacket =
        {
                S2C_WEAPON,                 //u8 type
                0,                          //i8 rotation
                now & 0xFFFF,               //u16 time
                0,                          //i16 x
                0,                          //i16 yspeed
                killer->pid,                //u16 playerid
                0,                          //i16 xspeed
                0,                          //u8 checksum
                status,                     //u8 status
                0,                          //u8 c2slatency
                0,                          //i16 y
                0,                          //u16 bounty

                {
                        //struct Weapons weapon
                        weaponType,	            //u16 type
                        weaponLevel,            //u16 level : 2
                        shrapelBouncing,        //u16 shrapbouncing : 1
                        shrapnelLevel,	        //u16 shraplevel : 2
                        shrapnel,		        //u16 shrap : 5
                        0  		                //u16 alternate : 1
                },

                {
                        //struct ExtraPosData extra
                        0,0,0,0,0,0,0,0,0,0,0,0,0
                }
        };

        WeaponsPacketChecksum(&wpnPacket);

        for (l = LLGetHead(&victims); l; l = l->next)
        {
                p = l->data;

                if (IS_STANDARD(p) && p->p_ship != SHIP_SPEC && p != killer)
                {
                        if (p->position.status & STATUS_SAFEZONE && warpOutSafe)
                        {
                                //warp the player out of his safe zone
                                struct S2CWeapons warpPacket =
                                {
                                        S2C_WEAPON,
                                        10,
                                        now & 0xFFFF,
                                        150, //x
                                        0,
                                        p->pid,
                                        0,
                                        0,
                                        p->position.status,
                                        0,
                                        150, //y
                                        p->position.bounty,
                                        { 0,0,0,0,0,0 },
                                        { 0,0,0,0,0,0,0,0,0,0,0,0,0 }
                                };

                                wpnPacket.x = 150;
                                wpnPacket.y = 150;
                                wpnPacket.xspeed = 0;
                                wpnPacket.yspeed = 0;
                                wpnPacket.rotation = 0;

                                WeaponsPacketChecksum(&warpPacket);

                                net->SendToOne(p, (byte*)&warpPacket, sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData), NET_RELIABLE);
                        }
                        else
                        {
                                wpnPacket.x = p->position.x;
                                wpnPacket.y = p->position.y;
                                wpnPacket.xspeed = p->position.xspeed;
                                wpnPacket.yspeed = p->position.yspeed;
                                wpnPacket.rotation = p->position.rotation;
                        }

                        WeaponsPacketChecksum(&wpnPacket);

                        net->SendToOne(p, (byte*)&wpnPacket, sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData), NET_RELIABLE);
                }
        }

        LLEmpty(&victims);
        Unlock();
}

static Player* PlayerDamageN (Arena *arena, const Target *tgtVictims, const char *fakeName, bool warpOutSafe, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing)
{
	struct arenaData* adata;
	struct playerData* pdata;
	Player *killer;
	ticks_t now = current_ticks();

	adata = GetArenaData(arena);

	killer = HashGetOne(adata->fakes, fakeName);

	if (!killer)
        {
                killer = fake->CreateFakePlayer(fakeName, arena, SHIP_WARBIRD, 9999);
                if (killer)
			HashAdd(adata->fakes, fakeName, killer);
        }

	if (killer)
	{
		pdata = GetPlayerData(killer);
		pdata->lastUse = now;
		PlayerDamage(tgtVictims, killer, warpOutSafe, weaponType, weaponLevel, shrapnel, shrapnelLevel, shrapelBouncing);
	}
	else
	{
		lm->LogA(L_ERROR, "dodamage", arena, "Unable to create fake player");
	}
	return killer;
}

static helptext_t Cdie_help =
        "Targets: any\n"
        "Args: {-f} {-me}\n"
        "Kills the targeted players.\n"
        "When not targeting a player, you will have to supply -f to prevent mistakes\n"
        "Use -me to not use a bot, but yourself as the killer. However, -me does not work on team mates or if you are in spectator mode\n";

static void Cdie(const char *tc, const char *params, Player *p, const Target *target)
{
        if (!p->arena) return;

        bool FFlag = false;
        bool MeFlag = false;

        if (strstr(params, "-f"))
                FFlag = true;

        if (strstr(params, "-me"))
                MeFlag = true;

        if (target->type != T_PLAYER && !FFlag)
        {
                chat->SendCmdMessage(p, "You must use the -f parameter when not targeting a player.");
                return;
        }

        if (MeFlag)
        {
                PlayerDamage(target, p, true, W_THOR, 3, 31, 3, true);
        }
        else
        {
                PlayerDamageN(p->arena, target, "<KillBot>", true, W_THOR, 3, 31, 3, true);
        }
}

EXPORT const char info_dodamage[] = "dodamage by JoWie ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(pd    );
        mm->ReleaseInterface(net   );
        mm->ReleaseInterface(game  );
        mm->ReleaseInterface(lm    );
        mm->ReleaseInterface(aman  );
        mm->ReleaseInterface(fake  );
        mm->ReleaseInterface(cmd   );
        mm->ReleaseInterface(chat  );
}

EXPORT int MM_dodamage(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;

                pd     = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
                net    = mm->GetInterface(I_NET       , ALLARENAS);
                game   = mm->GetInterface(I_GAME      , ALLARENAS);
                lm     = mm->GetInterface(I_LOGMAN    , ALLARENAS);
                aman   = mm->GetInterface(I_ARENAMAN  , ALLARENAS);
                fake   = mm->GetInterface(I_FAKE      , ALLARENAS);
                cmd    = mm->GetInterface(I_CMDMAN    , ALLARENAS);
                chat   = mm->GetInterface(I_CMDMAN    , ALLARENAS);

                if (!pd || !net || !game || !lm || !aman || !fake || !cmd || !chat)
                {
                        printf("<dodamage> Missing Interface\n");

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));

                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));
		if (playerKey == -1)
		{
			aman->FreeArenaData(arenaKey);
			ReleaseInterfaces();
			return MM_FAIL;
		}

                mm->RegInterface(&dodamage, ALLARENAS);

		mm->RegCallback(CB_ARENAACTION, ArenaActionCB, ALLARENAS);
                mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, ALLARENAS);
                mm->RegCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);
                cmd->AddCommand("die", Cdie, arena, Cdie_help);

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                if (mm->UnregInterface(&dodamage, ALLARENAS))
                        return MM_FAIL;

		aman->FreeArenaData(arenaKey);
		pd->FreePlayerData(playerKey);

		cmd->RemoveCommand("die", Cdie, arena);

		mm->UnregCallback(CB_ARENAACTION, ArenaActionCB, ALLARENAS);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, ALLARENAS);
		mm->UnregCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);

                ReleaseInterfaces();

                return MM_OK;
        }

        return MM_FAIL;
}
