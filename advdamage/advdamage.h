/* $Id: advdamage.h 51 2010-10-15 12:16:37Z jowie $ */
#ifndef ADVDAMAGE_H_INCLUDED
#define ADVDAMAGE_H_INCLUDED

#include <stdbool.h>
#include "packets/ppk.h"

#define ADV_GUN(lvl) (lvl)
#define ADV_BOMB(lvl) (lvl+4)
#define ADV_EBOMB(lvl) (lvl+8)
#define ADV_BBOMB(lvl) (lvl+12)
#define ADV_BEBOMB(lvl) (lvl+16)
#define ADV_SHRAPNEL(lvl) (lvl+20)
#define ADV_THOR (24)

#define I_ADVDAMAGE "advdamage-1"

// Just warped the player to the slaughter zone
#define CB_ADVDAMAGE_ATTEMPTKILL "advdamage-attemptkill-1"
typedef void (*AdvDamageAttemptKillFunc)(Player *p, Player *shooter);


// Fake players are not handled in this module unless they implement the watchdamage module
// However, you can use CalculateDamage to manage it yourself
typedef struct Iadvdamage
{
	INTERFACE_HEAD_DECL
	
	
	long (*GetHealth)(Player *p);
	long (*GetMaxHealth)(Player *p);
	
	void (*DealDamage)(Player *p, Player *shooter, long damage);
	
	// has no side effects; Turns normal damage (energy damage) into health damage
	long (*CalculateDamage)(Player *p, Player *shooter, struct Weapons *wpn, int nrgdamage, bool consult, bool consultBonus);

} Iadvdamage;

#define A_ADVDAMAGE "advdamage-1"
typedef struct Aadvdamage
{
	ADVISER_HEAD_DECL
	// This advisor is not called when using Iadvdamage->DealDamage 

	// for advWeapon see ADV_ constants
	void (*GetWeaponBonus)(Player *shooter, long *bonus, int advWeapon);

	void (*EditDamage)(Player *p, Player *shooter, int advWeapon, long *damage);
} Aadvdamage;

#endif
