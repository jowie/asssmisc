/* $Id: advdamage.c 75 2011-02-13 23:22:30Z jowie $ */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "pthread.h"

#define USE_HSCORE
#define HPBAR_UPDATE_INTERVAL 20 
#define HPBAR_MAXPARTS 100

// do not set these lower then 1
#define SLAUGHTER_MINDELAY 50
#define SLAUGHTER_IDLEDELAY 1000

#include "asss.h"
#include "advdamage.h"
#include "objects.h"
#include "fake.h"
#include "watchdamage.h"


#ifdef USE_HSCORE
#include "hscore/hscore.h"
#include "hscore/hscore_database.h"
#endif

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif



/*
	AdvDamage by JoWie

	A lot of hacks combined so that we have total control over health
	Should be loaded as soon as possible (aka before any other modules use A_KILL)
	
	TODO: test hscore stuff (armor and bonus)
	
	[AdvDamage]
	
	; Health is shared between arena's with the same identifier
	ArenaIdentifier = main
	SlaughterX = 8
	SlaughterY = 8
	SlaughterShip = 5
	
	HealthBarStart = 1600
	HealthBarCount = 20
	
	Gun1 = 1000
	Gun2 = 1500
	Gun3 = 2000
	Gun4 = 2500
	
	Bomb1 = 2000
	Bomb2 = 2500
	Bomb3 = 3000
	Bomb4 = 3500
	
	EBomb1 = 2000
	EBomb2 = 2500
	EBomb3 = 3000
	EBomb4 = 3500
	
	BBomb1 = 2000
	BBomb2 = 2500
	BBomb3 = 3000
	BBomb4 = 3500
	
	BEBomb1 = 2000
	BEBomb2 = 2500
	BEBomb3 = 3000
	BEBomb4 = 3500
	
	Shrapnel1 = 10
	Shrapnel2 = 15
	Shrapnel3 = 20
	Shrapnel4 = 25
	
	[Warbird]
	HealthRecharge = 100
	Health = 1000
	
	
	
	; The arena config should be set to the following:
	
	[Bullet]
	ExactDamage = 1
	BulletDamageLevel = 1
	BulletDamageUpgrade = 0
	
	[Bomb]
	BombDamageLevel = 100
	BBombDamagePercent = 1000
	EBombDamagePercent = 1000
	
	[Shrapnel]
	InactiveShrapDamage = 0
	ShrapnelDamagePercent = 1000
	
	; The ship you use as the SlaugherShip
	[Terrier]
	BurstShrapnel = 80
	BurstSpeed = 10000
	
	;Every ship:
	[All]
	BurstMax = 0
	
	[Burst]
	BurstDamageLevel = 30000
	
	
*/

/* cfghelp: AdvDamage:Gun1, arena, int */
/* cfghelp: AdvDamage:Gun2, arena, int */
/* cfghelp: AdvDamage:Gun3, arena, int */
/* cfghelp: AdvDamage:Gun4, arena, int */

/* cfghelp: AdvDamage:Bomb1, arena, int */
/* cfghelp: AdvDamage:Bomb2, arena, int */
/* cfghelp: AdvDamage:Bomb3, arena, int */
/* cfghelp: AdvDamage:Bomb4, arena, int */

/* cfghelp: AdvDamage:EBomb1, arena, int */
/* cfghelp: AdvDamage:EBomb2, arena, int */
/* cfghelp: AdvDamage:EBomb3, arena, int */
/* cfghelp: AdvDamage:EBomb4, arena, int */

/* cfghelp: AdvDamage:BBomb1, arena, int */
/* cfghelp: AdvDamage:BBomb2, arena, int */
/* cfghelp: AdvDamage:BBomb3, arena, int */
/* cfghelp: AdvDamage:BBomb4, arena, int */

/* cfghelp: AdvDamage:BEBomb1, arena, int */
/* cfghelp: AdvDamage:BEBomb2, arena, int */
/* cfghelp: AdvDamage:BEBomb3, arena, int */
/* cfghelp: AdvDamage:BEBomb4, arena, int */

/* cfghelp: AdvDamage:Shrapnel1, arena, int */
/* cfghelp: AdvDamage:Shrapnel2, arena, int */
/* cfghelp: AdvDamage:Shrapnel3, arena, int */
/* cfghelp: AdvDamage:Shrapnel4, arena, int */

#define DAMAGE_SETTINGS 25
static const char* const ARENA_SETTING_NAMES[] = { // also the name used in hscore
	// 32 characters or less
	"Gun1"     , "Gun2"     , "Gun3"     , "Gun4"     ,
	"Bomb1"    , "Bomb2"    , "Bomb3"    , "Bomb4"    ,
	"EBomb1"   , "EBomb2"   , "EBomb3"   , "EBomb4"   ,
	"BBomb1"   , "BBomb2"   , "BBomb3"   , "BBomb4"   ,
	"BEBomb1"  , "BEBomb2"  , "BEBomb3"  , "BEBomb4"  ,
	"Shrapnel1", "Shrapnel2", "Shrapnel3", "Shrapnel4",
	"Thor"
};

// below only for hscore:
static const char* const BONUS_NAMES[] = { // amount of damage i deal extra
	"Gun1Bonus", "Gun2Bonus", "Gun3Bonus", "Gun4Bonus",
	"Bomb1Bonus", "Bomb2Bonus", "Bomb3Bonus", "Bomb4Bonus",
	"EBomb1Bonus", "EBomb2Bonus", "EBomb3Bonus", "EBomb4Bonus",
	"BBomb1Bonus", "BBomb2Bonus", "BBomb3Bonus", "BBomb4Bonus",
	"BEBomb1Bonus", "BEBomb2Bonus", "BEBomb3Bonus", "BEBomb4Bonus",
	"Shrapnel1Bonus", "Shrapnel2Bonus", "Shrapnel3Bonus", "Shrapnel4Bonus",
	"ThorBonus"
};

static const char* const ARMOR_NAMES[] = { // armor value in permille
	"Gun1Armor", "Gun2Armor", "Gun3Armor", "Gun4Armor",
	"Bomb1Armor", "Bomb2Armor", "Bomb3Armor", "Bomb4Armor",
	"EBomb1Armor", "EBomb2Armor", "EBomb3Armor", "EBomb4Armor",
	"BBomb1Armor", "BBomb2Armor", "BBomb3Armor", "BBomb4Armor",
	"BEBomb1Armor", "BEBomb2Armor", "BEBomb3Armor", "BEBomb4Armor",
	"Shrapnel1Armor", "Shrapnel2Armor", "Shrapnel3Armor", "Shrapnel4Armor",
	"ThorArmor"
};

static Imodman		*mm;
static Imainloop	*ml;
static Iarenaman 	*aman;
static Iplayerdata	*pd;
static Inet		*net;
static Igame		*game;
static Icmdman		*cmd;
static Ilogman		*lm;
static Iconfig		*cfg;
static Imapdata		*mapdata;
static Ifake		*fake;
static Iobjects		*objs;
static Iwatchdamage     *watchdamage;
static Ichat		*chat;
#ifdef USE_HSCORE
static Ihscoreitems	*hcitems;
static Ihscoredatabase  *hcdatabase;
#endif
static int arenaKey  = -1;
static int playerKey = -1;

static pthread_mutex_t data_mtx; // recursive

struct ArenaGroupPlayerData;
struct ArenaData
{
	bool enabled;
	int humancount;
	Player *fakePlayer;
	struct ArenaGroupData *arenaGroup;
	
	ticks_t lastSlaughterWeaponFire;
	bool needSlaughterWeaponFire;
	
	struct
	{
		int slaughterX;
		int slaughterY;
		int healthBarStart;
		int healthBarCount;
		
		int cset_BombDamageLevel;
		int cset_BBombDamagePercent;
		int cset_EBombDamagePercent;
		int cset_BulletDamageLevel;
		int cset_BulletDamageUpgrade;
		int cset_ShrapnelDamagePercent;
		int shrapneldamage[4];
	} config;
	
	struct
	{
		bool emp;
		bool bounce;
	} shipconfig[8];
};

#define PlayerData PlayerData_
struct PlayerData
{
	struct ArenaGroupPlayerData *currentGroupData; // The data for the current arena
	ticks_t lastRecharge;
	Player *justKilledBy;
	
	ticks_t lastHealthBarUpdate;
	int healthBarObjectsVisible;
	bool config_dirty; // the config must be updated (UpdatePlayerConfig). But the event that triggered it comes from hscore and we need a database lock
	
	struct
	{
		int ship; // the ship this config applies to
		int rechargeRate; // health recharged per 10 seconds
		long maxHealth;
		int damageSettings[DAMAGE_SETTINGS];
		int bonusSettings[DAMAGE_SETTINGS];
		int armorSettings[DAMAGE_SETTINGS];
	} config;
};

////////////////////////////////////////////////////////////////////
struct ArenaGroupData
{
	const char *identifier;
	HashTable playerData; // struct ArenaGroupPlayerData*
};
////////////////////////////////////////////////////////////////////

static struct ArenaGroupData* NewArenaGroupData(const char *identifier)
{
	struct ArenaGroupData* data;
	
	data = amalloc(sizeof(struct ArenaGroupData));
	data->identifier = identifier;
	HashInit(&data->playerData);
	return data;
}

static int DestroyArenaGroupPlayerData_enum(const char *key, void *val, void *clos);
static void DestroyArenaGroupData(struct ArenaGroupData *data)
{
	HashEnum(&data->playerData, DestroyArenaGroupPlayerData_enum, NULL);
	HashDeinit(&data->playerData);
	afree(data);
}

static int DestroyArenaGroupData_enum(const char *key, void *val, void *clos)
{
	DestroyArenaGroupData(val);
	return 1;
}

////////////////////////////////////////////////////////////////////
struct ArenaGroupPlayerData
{
	Player *p;
	struct
	{
		long health;
	} ship[8];
};
////////////////////////////////////////////////////////////////////

static struct ArenaGroupPlayerData* NewArenaGroupPlayerData(Player *p)
{
	struct ArenaGroupPlayerData* data;
	
	data = amalloc(sizeof(struct ArenaGroupPlayerData));
	data->p = p;
	return data;
}

static void DestroyArenaGroupPlayerData(struct ArenaGroupPlayerData *data)
{
	afree(data);
}

static int DestroyArenaGroupPlayerData_enum(const char *key, void *val, void *clos)
{
	DestroyArenaGroupPlayerData(val);
	return 1;
}



static HashTable arenaGroups; // struct ArenaGroupData*


static void ReadConfig(Arena *arena);
static void UpdatePlayerConfig(Player *p);

static void AssertLM(const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<advdamage> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct ArenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey >= 0);
        struct ArenaData *adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);
        return adata;
}

static struct PlayerData* GetPlayerData(Player *p)
{
        assertlm(p);
        assertlm(playerKey >= 0);
        struct PlayerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);
        return pdata;
}

static inline void LockData() // Lock me BEFORE any other locks (such as pd and aman)
{
        pthread_mutex_lock(&data_mtx);
}
static inline void UnlockData()
{
        pthread_mutex_unlock(&data_mtx);
}

static void ArenaActionCB(Arena *arena, int action)
{
	Link *link;
	Player *p;
	if (action == AA_CONFCHANGED)
	{
		ReadConfig(arena);
		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena)
				UpdatePlayerConfig(p);
		}
		pd->Unlock();
	}
}

static void ReadConfig(Arena *arena)
{
	ConfigHandle ch;
	struct ArenaData *adata;
	int a;
	int fakeShip;
	bool warned;

	ch = arena->cfg;
	adata = GetArenaData(arena);


	/* cfghelp: AdvDamage:SlaughterX, arena, int, def: 8, range:0-1023
         * The X coordinate of the position where players go to die with a burst */
	adata->config.slaughterX = cfg->GetInt(ch, "AdvDamage", "SlaughterX", 8);
	/* cfghelp: AdvDamage:SlaughterY, arena, int, def: 8, range:0-1023
         * The Y coordinate of the position where players go to die with a burst */
	adata->config.slaughterY = cfg->GetInt(ch, "AdvDamage", "SlaughterY", 8);
	
	/* cfghelp: AdvDamage:SlaughterShip, arena, int, def: 1, range:0-7
         * The ship that will fire the burst when a player needs to die */
	fakeShip = cfg->GetInt(ch, "AdvDamage", "SlaughterShip", 1) - 1;
	CLIP(fakeShip, SHIP_WARBIRD, SHIP_SHARK);
	
	adata->config.healthBarStart = cfg->GetInt(ch, "AdvDamage", "HealthBarStart", 0);
	adata->config.healthBarCount = cfg->GetInt(ch, "AdvDamage", "HealthBarCount", 0);
	if (adata->config.healthBarStart <= 0 || adata->config.healthBarCount <= 0)
	{
		adata->config.healthBarStart = 0;
		adata->config.healthBarCount = 0;
	}
	
	if (adata->config.healthBarCount > HPBAR_MAXPARTS)
		adata->config.healthBarCount = HPBAR_MAXPARTS;

	if (adata->enabled && adata->humancount)
	{
		if (!adata->fakePlayer)
		{
			adata->fakePlayer = fake->CreateFakePlayer("", arena, fakeShip, 9999);
			lm->LogA(L_DRIVEL, "advdamage", arena, "Recreating fake player");
		}
		else
		{
			if (fakeShip != adata->fakePlayer->p_ship)
				game->SetShip(adata->fakePlayer, fakeShip);
		}
	}
	
	for (a = 0; a < 8; a++)
	{
		adata->shipconfig[a].emp = !!cfg->GetInt(ch, cfg->SHIP_NAMES[a], "EmpBomb", 0);
		adata->shipconfig[a].bounce =  !!cfg->GetInt(ch, cfg->SHIP_NAMES[a], "BombBounceCount", 0);
	}
	
	adata->config.cset_BombDamageLevel = cfg->GetInt(ch, "Bomb", "BombDamageLevel", 0);
	adata->config.cset_BBombDamagePercent = cfg->GetInt(ch, "Bomb", "BBombDamagePercent", 0);
	adata->config.cset_EBombDamagePercent = cfg->GetInt(ch, "Bomb", "EBombDamagePercent", 0);
	adata->config.cset_BulletDamageLevel = cfg->GetInt(ch, "Bullet", "BulletDamageLevel", 0);
	adata->config.cset_BulletDamageUpgrade = cfg->GetInt(ch, "Bullet", "BulletDamageUpgrade", 0);
	adata->config.cset_ShrapnelDamagePercent = cfg->GetInt(ch, "Shrapnel", "ShrapnelDamagePercent", 0);
	
	// Warnings about unsupported arena settings
	
	if (!cfg->GetInt(ch, "Bullet", "ExactDamage", 0))
		lm->LogA(L_ERROR, "advdamage", arena, "Bullet:ExactDamage must be enabled");
	
	
	if (adata->config.cset_BulletDamageLevel < 1)
		lm->LogA(L_ERROR, "advdamage", arena, "Bullet::BulletDamageLevel is set to low");
	
	if (adata->config.cset_BulletDamageUpgrade < 1)
		lm->LogA(L_ERROR, "advdamage", arena, "Bullet::BulletDamageUpgrade is set to low"); 
	
	if (adata->config.cset_BombDamageLevel < 100)
		lm->LogA(L_ERROR, "advdamage", arena, "Bomb:BombDamageLevel is set to low");
	
	
	if (!adata->config.cset_BBombDamagePercent || 
	     adata->config.cset_BombDamageLevel * adata->config.cset_BBombDamagePercent / 1000 < 100)
	{
		lm->LogA(L_ERROR, "advdamage", arena, "Bomb:BBombDamagePercent is set to low");
	}
	
	
	if (!adata->config.cset_EBombDamagePercent || 
	     adata->config.cset_BombDamageLevel * adata->config.cset_EBombDamagePercent / 1000 < 100)
	{
		lm->LogA(L_ERROR, "advdamage", arena, "Bomb:EBombDamagePercent is set to low");
	}
	
	if (adata->config.cset_BBombDamagePercent && adata->config.cset_BombDamageLevel )
	{
		if (adata->config.cset_BombDamageLevel * adata->config.cset_EBombDamagePercent / 1000
		    * adata->config.cset_BBombDamagePercent / 1000 < 100)
		{
			lm->LogA(L_ERROR, "advdamage", arena, "Bomb:BBombDamagePercent and/or Bomb:EBombDamagePercent is set to low");
		}		    
	}
	
	if (cfg->GetInt(ch, "Shrapnel", "InactiveShrapDamage", 0) > 0)
		lm->LogA(L_ERROR, "advdamage", arena, "Shrapnel:InactiveShrapDamage must be set to 0");
	
	warned = false;
	for (a = 0; a < 4; a++)
	{
		adata->config.shrapneldamage[a] = adata->config.cset_BulletDamageLevel + adata->config.cset_BulletDamageUpgrade * a;
		adata->config.shrapneldamage[a] = adata->config.shrapneldamage[a] * adata->config.cset_ShrapnelDamagePercent / 1000;
		
		if (adata->config.shrapneldamage[a] < 1 || (a > 0 && adata->config.shrapneldamage[a-1] >= adata->config.shrapneldamage[a]))
		{
			if (!warned)
				lm->LogA(L_ERROR, "advdamage", arena, "Shrapnel:ShrapnelDamagePercent is set to low or see previous error about bullet damage");
			warned = true;
		}
	}
}

static void KillPlayer(Player *p, Player *shooter)
{
	struct ArenaData *adata;
	struct PlayerData *pdata;
	Arena *arena;
	Target tgtPlayer;
	
	assertlm(p->arena == shooter->arena);
	arena = p->arena;
	tgtPlayer.type = T_PLAYER;
	tgtPlayer.u.p = p;
	adata = GetArenaData(arena);
	pdata = GetPlayerData(p);
	
	adata->needSlaughterWeaponFire = true;
	pdata->justKilledBy = shooter;
	game->WarpTo(&tgtPlayer, adata->config.slaughterX, adata->config.slaughterY);
	
	DO_CBS(CB_ADVDAMAGE_ATTEMPTKILL, arena, AdvDamageAttemptKillFunc, (p, shooter));
	lm->LogP(L_DRIVEL, "advdamage", p, "Killed by %s", shooter->name);
	
}

static void EditDeathAdv(Arena *arena, Player **killer, Player **killed, int *bounty)
{
	struct ArenaData *adata;
	struct PlayerData *pdata;

	LockData();
	adata = GetArenaData(arena);
	pdata = GetPlayerData(*killed);
	
	if (adata->enabled && *killer == adata->fakePlayer && pdata->justKilledBy)
	{
		*killer = pdata->justKilledBy;
		pdata->justKilledBy = NULL; 		
	}
	UnlockData();
}

static void EditPPKAdv(Player *p, struct C2SPosition *pos)
{
	struct PlayerData *pdata;
	long health;

	LockData();
	
	pdata = GetPlayerData(p);
	
	if (IS_STANDARD(p)) // Modules using fake players should set the energy viewing themselves
	{
		health = 0;
		if (p->p_ship <= 7 && pdata->currentGroupData)
			health = pdata->currentGroupData->ship[p->p_ship].health;
		
		// i16
		CLIP(health, 0, ((1 << 16) - 1));
		pos->energy = health;
		pos->extra.energy = health;
		p->position.energy = health;
	}
	
	// drop the position packet so that we get a nice death animation at his old position
	if (pdata->justKilledBy)
	{
		pos->x = -1;
		pos->y = -1;
	}
	
	UnlockData();
}


static void NewPlayerCB(Player *p, int isnew)
{
	Arena *arena;
	Link *link;
	struct ArenaData *adata;
	struct PlayerData *pdata;

	LockData();
	pdata = GetPlayerData(p);
	pdata->config.ship = -1;

	if (p->type != T_FAKE || isnew)
	{
		UnlockData();
		return;
	}
	aman->Lock();
	FOR_EACH_ARENA(arena)
	{
		adata = GetArenaData(arena);
		if (adata->fakePlayer == p)
			adata->fakePlayer = NULL;
	}
	
	aman->Unlock();
	UnlockData();
}

static void MainLoopCB()
{
	Link *link;
	Player *p;
	Arena *arena;
	long *health;
	struct PlayerData *pdata;
	struct ArenaData *adata;
	int diff;
	int visible;
	ticks_t now;
	Target tgtPlayer;
	tgtPlayer.type = T_PLAYER;
	short ids[HPBAR_MAXPARTS];
	char ons[HPBAR_MAXPARTS];
	int size;
	int a;
	struct C2SPosition pos;
	bool sendpos;
	
	now = current_ticks();
	
	LockData();
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		arena = p->arena;
		if (!arena)
			continue;
		
		
		adata = GetArenaData(arena);
		pdata = GetPlayerData(p);
		tgtPlayer.u.p = p;
		
		if (pdata->config_dirty || pdata->config.ship != p->p_ship)
			UpdatePlayerConfig(p); // fake players do not fire PlayerAction callbacks
		
		if (!adata->enabled || !pdata->currentGroupData || p->p_ship == SHIP_SPEC)
		{
			continue;
		}
		sendpos = false;
		
		if (adata->lastSlaughterWeaponFire)
		{
			if (adata->fakePlayer && TICK_DIFF(now, adata->lastSlaughterWeaponFire) > (adata->needSlaughterWeaponFire ? SLAUGHTER_MINDELAY : SLAUGHTER_IDLEDELAY))
			{
				pos.type = C2S_POSITION;
				pos.rotation = 0;
				pos.x = adata->config.slaughterX * 16 + 8;
				pos.y = adata->config.slaughterY * 16 + 8;
				pos.xspeed = 0;
				pos.yspeed = 0;
				pos.bounty = 0;
				pos.status = STATUS_CLOAK | STATUS_STEALTH | STATUS_UFO;
				pos.time = now;
				pos.energy = 0;
				pos.weapon.type = W_BURST;
				pos.weapon.level = 0;
				pos.weapon.shraplevel = 0;
				pos.weapon.shrap = 0;
				pos.weapon.shrapbouncing = 0;
				pos.weapon.alternate = 0;
				
				if (adata->needSlaughterWeaponFire)
					pos.status |= STATUS_FLASH; // position packets with STATUS_FLASH get their position sent to everyone (game.c)
				
				sendpos = true;
				adata->lastSlaughterWeaponFire = now;
				adata->needSlaughterWeaponFire = false;
			}
		}
		else
		{
			adata->lastSlaughterWeaponFire = now;
		}
		
		health = &pdata->currentGroupData->ship[p->p_ship].health;
		if (*health > 0)
		{
			if (pdata->lastRecharge && pdata->config.rechargeRate > 0 && *health < pdata->config.maxHealth)
			{
				diff = (pdata->config.rechargeRate * TICK_DIFF(now, pdata->lastRecharge)) / 1000;
				if (diff > 0)
				{
					*health += diff;
					pdata->lastRecharge = now;					 
				}
				
				// do not update lastRecharge if diff is to small
			}
			else
				pdata->lastRecharge = now;
			
			
			if (*health > pdata->config.maxHealth)
				*health = pdata->config.maxHealth;
			
			
			if (*health < 1)
				*health = 1;
		}
		
		if (adata->config.healthBarCount && 
		     (!pdata->lastHealthBarUpdate || TICK_DIFF(now, pdata->lastHealthBarUpdate) > HPBAR_UPDATE_INTERVAL)
		   )
		{
			pdata->lastHealthBarUpdate = now;
			
			// integer division (round down). This is desired, only show all the bars when we have completely full health
			visible = adata->config.healthBarCount * (*health) / pdata->config.maxHealth;
			
			if (pdata->healthBarObjectsVisible != visible)
			{
				size = 0;
				
				if (visible < pdata->healthBarObjectsVisible)
				{
					for (a = visible; a < pdata->healthBarObjectsVisible; a++)
					{
						ids[size] = adata->config.healthBarStart + a;
						ons[size] = 0;
						size++;
					}
				}
				else // visible > pdata->healthBarObjectsVisible
				{
					for (a = pdata->healthBarObjectsVisible; a < visible; a++)
					{
						ids[size] = adata->config.healthBarStart + a;
						ons[size] = 1;
						size++;
					}
				}
				pdata->healthBarObjectsVisible = visible;
				
				objs->ToggleSet(&tgtPlayer, ids, ons, size);
			}
		}
		if (sendpos)
			game->FakePosition(adata->fakePlayer, &pos, sizeof(struct C2SPosition) - sizeof(struct ExtraPosData));
	}
	pd->Unlock();
	UnlockData();
}

static void UpdatePlayerConfig(Player *p)
{
	Arena *arena;
	struct PlayerData *pdata;
	struct ArenaData *adata;
	int a;
	ConfigHandle ch;
	
	arena = p->arena;
	ch = arena->cfg;
	adata = GetArenaData(arena);
	pdata = GetPlayerData(p);
	pdata->config_dirty = false; 
	
	// pdata->currentGroupData can be NULL 
	
	pdata->config.ship = p->p_ship;
	if (!adata->enabled || p->p_ship > 7)
	{
		pdata->config.rechargeRate = 0;
		pdata->config.maxHealth = 0;
		memset(pdata->config.damageSettings, 0, sizeof(int) * DAMAGE_SETTINGS);
		memset(pdata->config.bonusSettings, 0, sizeof(int) * DAMAGE_SETTINGS);
		memset(pdata->config.armorSettings, 0, sizeof(int) * DAMAGE_SETTINGS);
		return;
	}
	
	/* cfghelp: All:HealthRecharge, arena, int, def: 10
	 * The amount of health recharged every 10 seconds */
	pdata->config.rechargeRate = cfg->GetInt(ch, cfg->SHIP_NAMES[p->p_ship], "HealthRecharge", 10);
	
	/* cfghelp: All:Health, arena, int, def: 1000
	 * The amount of health this ship has */
	pdata->config.maxHealth = cfg->GetInt(ch, cfg->SHIP_NAMES[p->p_ship], "Health", 1000);
	
	#ifdef USE_HSCORE
	if (IS_STANDARD(p))
	{
		hcdatabase->lock();
		pdata->config.rechargeRate = hcitems->getPropertySumNoLock(p, p->p_ship, "HealthRecharge", pdata->config.rechargeRate);
		pdata->config.maxHealth = hcitems->getPropertySumNoLock(p, p->p_ship, "Health", pdata->config.maxHealth);
	}	
	#endif
	
	if (pdata->config.rechargeRate < 0)
		pdata->config.rechargeRate = 0;
	
	if (pdata->config.maxHealth < 1)
		pdata->config.maxHealth = 1;
	
	
	for (a = 0; a < DAMAGE_SETTINGS; a++)
	{
		int z;
		
		z = pdata->config.damageSettings[a] = cfg->GetInt(ch, "AdvDamage", ARENA_SETTING_NAMES[a], 0);
		#ifdef USE_HSCORE
		if (IS_STANDARD(p))
			pdata->config.damageSettings[a] = hcitems->getPropertySumNoLock(p, p->p_ship, ARENA_SETTING_NAMES[a], pdata->config.damageSettings[a]);
		#endif
	}
	
	#ifdef USE_HSCORE
	if (IS_STANDARD(p))
	{
		for (a = 0; a < DAMAGE_SETTINGS; a++)
		{
			pdata->config.bonusSettings[a] = hcitems->getPropertySumNoLock(p, p->p_ship, BONUS_NAMES[a], 0);
			pdata->config.armorSettings[a] = hcitems->getPropertySumNoLock(p, p->p_ship, ARMOR_NAMES[a], 0);
		}
		hcdatabase->unlock();
	}
	#endif
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	struct PlayerData* pdata;
	struct ArenaData *adata;
	int createFake;
	Player *destroyFake;
	createFake = -1;
	destroyFake = NULL;

	
	if (action == PA_ENTERARENA)
	{
		LockData();
		adata = GetArenaData(arena);
		pdata = GetPlayerData(p);
		
		
		if (IS_HUMAN(p))
		{
			adata->humancount++;
			if (!adata->fakePlayer && adata->enabled)
			{
				createFake = cfg->GetInt(arena->cfg, "AdvDamage", "SlaughterShip", 1) - 1;
				CLIP(createFake, SHIP_WARBIRD, SHIP_SHARK);
				
				lm->LogA(L_DRIVEL, "advdamage", arena, "Creating fake player because a human player entered");
			}
		}
		pdata->currentGroupData = NULL;
		pdata->healthBarObjectsVisible = 0;
		if (adata->enabled)
		{
			pdata->currentGroupData = HashGetOne(&adata->arenaGroup->playerData, p->name);
			if (!pdata->currentGroupData)
			{
				pdata->currentGroupData = NewArenaGroupPlayerData(p);
				HashAdd(&adata->arenaGroup->playerData, p->name, pdata->currentGroupData);
			}
			
			watchdamage->ModuleWatch(p, 1);
			UpdatePlayerConfig(p);
		}
		
		UnlockData();
		if (createFake >= 0)
			adata->fakePlayer = fake->CreateFakePlayer("", arena, createFake, 9999);
	}
	else if (action == PA_ENTERGAME) // download is complete
	{
		LockData();
		adata = GetArenaData(arena);
		pdata = GetPlayerData(p);
		if (adata->enabled)
			UpdatePlayerConfig(p);
		
		UnlockData();
	}
	else if (action == PA_LEAVEARENA)
	{
		LockData();
		pdata = GetPlayerData(p);
		adata = GetArenaData(arena);
		

		if (IS_HUMAN(p))
		{
			adata->humancount--;
			if (adata->humancount <= 0)
			{
				if (adata->fakePlayer)
				{
					destroyFake = adata->fakePlayer; 
					adata->fakePlayer = NULL;
				}
				lm->LogA(L_DRIVEL, "advdamage", arena, "Removing fake player because the last human player left");
			}
		}
		
		pdata->currentGroupData = NULL;
		if (adata->enabled)
		{
			watchdamage->ModuleWatch(p, 0);
		}
		
		UnlockData();
		if (destroyFake)
			fake->EndFaked(destroyFake);
	}
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq) // comes before CB_SPAWN
{
	struct PlayerData *pdata;
	LockData();
	pdata = GetPlayerData(p);
	UpdatePlayerConfig(p);
	UnlockData();
}

static void KillCB(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
	LockData();
	UnlockData();
}

static void SpawnCB(Player *p, int reason)
{
	struct PlayerData *pdata;
	long *health;
	if (reason & SPAWN_AFTERDEATH || reason & SPAWN_INITIAL || reason & SPAWN_SHIPCHANGE)
	{
		LockData();
		pdata = GetPlayerData(p);
		pdata->justKilledBy = NULL;
		if (!pdata->currentGroupData || p->p_ship > 7)
		{
			UnlockData();
			return;
		}
		
		health = &pdata->currentGroupData->ship[p->p_ship].health;
		
		if (reason & SPAWN_AFTERDEATH || *health < 1)
			*health = pdata->config.maxHealth;
		
		pdata->lastRecharge = current_ticks();
		UnlockData();
	}
}

static void PPKCB(Player *p, const struct C2SPosition *pos) // Comes from a different thread!
{
	//struct PlayerData *pdata;
	if (pos->weapon.type == W_NULL)
		return;
	
	LockData();
	UnlockData();
	// pdata = GetPlayerData(p);
		
	
}

static long CalculateDamage(Player *p, Player *shooter, struct Weapons *wpn, int nrgdamage, bool consult, bool consultBonus) // call with lock
{
	struct PlayerData *pdata;
	struct ArenaData *adata;
	bool emp, bounce;
	long damage, maxdamage;
	int setting;
	int lvl;
	struct PlayerData *shooter_pdata;
	LinkedList advisers = LL_INITIALIZER;
	Link *alink;
	Aadvdamage *dmgAdviser;
	Arena *arena;
	arena = p->arena;
	
	adata = GetArenaData(arena);
	pdata = GetPlayerData(p);
	shooter_pdata = GetPlayerData(shooter);
	
	if (pdata->config.ship != p->p_ship)
		UpdatePlayerConfig(p); // fake players do not fire PlayerAction callbacks
	
	if (shooter_pdata->config.ship != shooter->p_ship)
		UpdatePlayerConfig(p); // fake players do not fire PlayerAction callbacks
	
	damage = 0;
	lvl = wpn->level;
	mm->GetAdviserList(A_ADVDAMAGE, arena, &advisers);
	
	if (wpn->type == W_BULLET || wpn->type == W_BOUNCEBULLET)
	{
		setting = ADV_GUN(lvl);
		damage = shooter_pdata->config.bonusSettings[setting];
		if (consultBonus)
		{
			FOR_EACH(&advisers, dmgAdviser, alink)
			{
				if (dmgAdviser->GetWeaponBonus)
					dmgAdviser->GetWeaponBonus(shooter, &damage, setting);
			}
		}
		
		// easy, we do not care about the damage value passed
		damage += pdata->config.damageSettings[setting];
		damage -= damage * pdata->config.armorSettings[setting] / 1000;
		
		//lm->LogP(L_DRIVEL, "advdamage", p, "Took %d damage from a gun. Health is now %ld", damage, *health);			
	}
	else if (wpn->type == W_BOMB || wpn->type == W_PROXBOMB || wpn->type == W_THOR)
	{
		// harder, abuse the damage value to figure out how far the bomb was
		emp = adata->shipconfig[shooter->p_ship].emp;
		bounce = adata->shipconfig[shooter->p_ship].bounce;
		
		if (wpn->type == W_THOR)
		{
			setting = ADV_THOR;
			emp = false;
			bounce = false;
		}
		else if (emp && bounce)
			setting = ADV_BEBOMB(lvl);
		else if (emp)
			setting = ADV_EBOMB(lvl);
		else if (bounce)
			setting = ADV_BBOMB(lvl);
		else
			setting = ADV_BOMB(lvl);
		
		maxdamage = adata->config.cset_BombDamageLevel;
		
		// figure out the max damage
		if (emp)
			maxdamage = maxdamage * adata->config.cset_EBombDamagePercent / 1000;
		
		if (bounce)
			maxdamage = maxdamage * adata->config.cset_BBombDamagePercent / 1000;
		
		if (maxdamage < 1) // bad arena config; Should already have been warned about during settings read
		{
			mm->ReleaseAdviserList(&advisers);
			return 0;
		}
		
		damage = shooter_pdata->config.bonusSettings[setting];
		if (consultBonus)
		{
			FOR_EACH(&advisers, dmgAdviser, alink)
			{
				if (dmgAdviser->GetWeaponBonus)
					dmgAdviser->GetWeaponBonus(shooter, &damage, setting);
			}
		}
		
		damage += pdata->config.damageSettings[setting];
		damage -= damage * pdata->config.armorSettings[setting] / 1000;
		damage = damage * nrgdamage / maxdamage;
		
		//lm->LogP(L_DRIVEL, "advdamage", p, "Took %d damage from a bomb. Health is now %ld", damage, *health);
	}
	else if (wpn->type == 15) // active shrapnel; note that it is not possible to detect inactive shrapnel
	{
		// the level is always set to 0, so use the damage value to figure out what level it was (assume exact damage)
		
		for (lvl = 1; lvl < 4; lvl++)
		{
			if (adata->config.shrapneldamage[lvl] > nrgdamage)
				break;
		}
		lvl--;
		setting = ADV_SHRAPNEL(lvl);
		
		damage = shooter_pdata->config.bonusSettings[setting];
		if (consultBonus)
		{
			FOR_EACH(&advisers, dmgAdviser, alink)
			{
				if (dmgAdviser->GetWeaponBonus)
					dmgAdviser->GetWeaponBonus(shooter, &damage, setting);
			}
		}
		
		damage += pdata->config.damageSettings[setting];
		damage -= damage * pdata->config.armorSettings[setting] / 1000;
		
		//lm->LogP(L_DRIVEL, "advdamage", p, "Took %ld damage from a level %d shrap.", damage, lvl);	
	}
	
	if (damage < 0)
		damage = 0;
	
	if (damage && consult)
	{
		FOR_EACH(&advisers, dmgAdviser, alink)
		{
			if (!dmgAdviser->EditDamage)
				continue;
			
			dmgAdviser->EditDamage(p, shooter, setting, &damage);
			
		}
	}
	mm->ReleaseAdviserList(&advisers);
	return damage;
}

static long CalculateDamageInt(Player *p, Player *shooter, struct Weapons *wpn, int nrgdamage, bool consult, bool consultBonus)
{
	long ret;
	LockData();
	ret = CalculateDamage(p, shooter, wpn, nrgdamage, consult, consultBonus);
	UnlockData();
	return ret;
}

static void PlayerDamageCB(Arena *arena, Player *p, struct S2CWatchDamage *s2cdamage, int count) // Comes from a different thread!
{
	struct PlayerData *pdata;
	struct ArenaData *adata;
	long *health;
	long oldHealth;
	int a;
	Player *shooter;
	DamageData *damageData;
	struct Weapons *wpn;
	
	LockData();
	adata = GetArenaData(arena);
	pdata = GetPlayerData(p);
	assertlm(p->arena == arena);
	
	if (!adata->enabled || p->p_ship > 7 || !pdata->currentGroupData)
	{
		UnlockData();
		return;
	}
	
	health = &pdata->currentGroupData->ship[p->p_ship].health;
	
	for (a = 0; a < count; a++)
	{
		damageData = &s2cdamage->damage[a];
		wpn = &damageData->weapon;
		if (!wpn->type) // wormhole
			continue;
		
		shooter = pd->PidToPlayer(damageData->shooteruid);
		if (!shooter || shooter->arena != arena || shooter->p_ship > 7)
		{
			lm->LogP(L_MALICIOUS, "advdamage", p, "Invalid shooteruid (%d) in watchdamage packet", damageData->shooteruid);
			continue;
		}
		
		oldHealth = *health;
		
		lm->LogP(L_DRIVEL, "advdamage", p, "Took damage from %s: %d> type = %d, level = %d, shrap = %d, shraplevel = %d, alternate = %d", 
		         shooter->name, a, wpn->type, wpn->level, wpn->shrap, wpn->shraplevel, wpn->alternate);
		
			
		*health -= CalculateDamage(p, shooter, wpn, damageData->damage, true, true);
		
		
		if (*health <= 0)
		{
			*health = 0;
			if (oldHealth > 0) // only kill the player once
			{
				KillPlayer(p, shooter);
			}
		}
	}
	UnlockData();
}

static void HCItemsChanged(Player *p, int ship) // Might come from a different thread (depends on on other modules using hscore_items)
{
	struct PlayerData *pdata;
	
	LockData();
	pdata = GetPlayerData(p);
	pdata->config_dirty = true;
	UnlockData();
}

static void Cresetturrets(const char *tc, const char *params, Player *p, const Target *target)
{
	struct ArenaData *adata;
	LockData();
	adata = GetArenaData(p->arena);

	if (adata->fakePlayer)
	{
		fake->EndFaked(adata->fakePlayer);
		adata->fakePlayer = NULL;
	}
	UnlockData();
}

static void Chealth(const char *tc, const char *params, Player *p, const Target *target)
{
	struct PlayerData *pdata;
	
	LockData();
	pdata = GetPlayerData(p);
	if (!pdata->currentGroupData || p->p_ship > 7)
	{
		chat->SendCmdMessage(p, "You are not in a ship that has health");
		UnlockData();
		return;
	}
	
	chat->SendCmdMessage(p, "You have %ld health out of %ld total", pdata->currentGroupData->ship[p->p_ship].health, pdata->config.maxHealth);
	UnlockData();
}

static long GetHealthInt(Player *p)
{
	struct PlayerData *pdata;
	long val;
	
	LockData();
	pdata = GetPlayerData(p);
	if (!pdata->currentGroupData || p->p_ship > 7)
	{
		UnlockData();
		return 0;
	}
	
	val = pdata->currentGroupData->ship[p->p_ship].health;
	UnlockData();
	return val; 
}

static long GetMaxHealthInt(Player *p)
{
	struct PlayerData *pdata;
	long val;
	
	LockData();
	pdata = GetPlayerData(p);
	val = pdata->config.maxHealth;
	UnlockData();
	return val;
}
	
static void DealDamageInt(Player *p, Player *shooter, long damage)
{
	long *health;
	long oldHealth;
	
	struct PlayerData *pdata;
	
	LockData();
	pdata = GetPlayerData(p);
	if (!pdata->currentGroupData || p->p_ship > 7)
	{
		UnlockData();
		return;
	}
	
	health = &pdata->currentGroupData->ship[p->p_ship].health;
	oldHealth = *health;
	
	*health -= damage;
	
	if (*health < 0)
	{
		*health = 0;
		if (oldHealth > 0) // only kill the player once
		{
			KillPlayer(p, shooter);
		}
	}
	UnlockData();
}
	
static Iadvdamage advdamage =
{
        INTERFACE_HEAD_INIT(I_ADVDAMAGE, "advdamage")

        GetHealthInt,
        GetMaxHealthInt,
        DealDamageInt,
        CalculateDamageInt
};

static Akill killadv = 
{
	ADVISER_HEAD_INIT(A_KILL)
	
	NULL,
	EditDeathAdv
};

static Appk ppkadv = 
{
	ADVISER_HEAD_INIT(A_PPK)
	
	EditPPKAdv,
	NULL
};

EXPORT const char info_advdamage[] = "AdvDamage $Revision: 75 $ by JoWie\n";
static void ReleaseInterfaces()
{
	mm->ReleaseInterface(ml       );
        mm->ReleaseInterface(aman     );
        mm->ReleaseInterface(pd       );
        mm->ReleaseInterface(net      );
        mm->ReleaseInterface(game     );
        mm->ReleaseInterface(cmd      );
        mm->ReleaseInterface(lm       );
        mm->ReleaseInterface(mapdata  );
        mm->ReleaseInterface(cfg      );
        mm->ReleaseInterface(fake     );
        mm->ReleaseInterface(objs     );
        mm->ReleaseInterface(watchdamage);
        mm->ReleaseInterface(chat     );
        #ifdef USE_HSCORE
        mm->ReleaseInterface(hcitems  );
        mm->ReleaseInterface(hcdatabase);
        #endif
}

EXPORT int MM_advdamage(int action, Imodman *mm_, Arena *arena)
{
	struct ArenaData *adata;
	struct ArenaGroupData *arenaGroupData;
	const char *arenaIdent;
	Player *p;
	pthread_mutexattr_t attr;
	Link *link;

        if (action == MM_LOAD)
        {
                mm        = mm_;
                ml        = mm->GetInterface(I_MAINLOOP        , ALLARENAS);
                aman      = mm->GetInterface(I_ARENAMAN        , ALLARENAS);
                pd        = mm->GetInterface(I_PLAYERDATA      , ALLARENAS);
                net       = mm->GetInterface(I_NET             , ALLARENAS);
                game      = mm->GetInterface(I_GAME            , ALLARENAS);
                cmd       = mm->GetInterface(I_CMDMAN          , ALLARENAS);
                lm        = mm->GetInterface(I_LOGMAN          , ALLARENAS);
                mapdata   = mm->GetInterface(I_MAPDATA         , ALLARENAS);
                cfg       = mm->GetInterface(I_CONFIG	       , ALLARENAS);
                fake      = mm->GetInterface(I_FAKE	       , ALLARENAS);
                objs      = mm->GetInterface(I_OBJECTS	       , ALLARENAS);
                watchdamage = mm->GetInterface(I_WATCHDAMAGE   , ALLARENAS);
                chat      = mm->GetInterface(I_CHAT            , ALLARENAS);
                #ifdef USE_HSCORE
                hcitems   = mm->GetInterface(I_HSCORE_ITEMS    , ALLARENAS);
                hcdatabase = mm->GetInterface(I_HSCORE_DATABASE, ALLARENAS);
                #endif



                if (!ml || !aman || !pd || !net || !game || !cmd || !lm || !mapdata || !cfg || !fake || !objs || !watchdamage || !chat)
                {
                        ReleaseInterfaces();
                        printf("<advdamage> Missing interfaces\n");
                        return MM_FAIL;
                }
                
                #ifdef USE_HSCORE
                if (!hcitems || !hcdatabase)
                {
                	ReleaseInterfaces();
                        printf("<advdamage> Missing interfaces (hscore)\n");
                        return MM_FAIL;
                }
                #endif

                arenaKey = aman->AllocateArenaData(sizeof(struct ArenaData));
                playerKey = pd->AllocatePlayerData(sizeof(struct PlayerData));

                if (arenaKey == -1 || playerKey == -1)
                {
                        if (arenaKey  != -1)
                                aman->FreeArenaData(arenaKey);

                        if (playerKey != -1)
                                pd->FreePlayerData (playerKey);

                        ReleaseInterfaces();

                        return MM_FAIL;
                }
                
                
	        pthread_mutexattr_init(&attr);
	        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE); // so that advisers may use interface methods
	        pthread_mutex_init(&data_mtx, &attr);
	        pthread_mutexattr_destroy(&attr);
	        
		LockData();
                mm->RegCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);
                mm->RegCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);
		mm->RegInterface(&advdamage, ALLARENAS);
		mm->RegAdviser(&killadv, ALLARENAS);
		
		HashInit(&arenaGroups);
		UnlockData();

                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
        	if (mm->UnregInterface(&advdamage, ALLARENAS))
			return MM_FAIL;
        	
        	LockData();
        	HashEnum(&arenaGroups, DestroyArenaGroupData_enum, NULL);
        	HashDeinit(&arenaGroups);
        
                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);

		mm->UnregAdviser(&killadv, ALLARENAS);
		mm->UnregCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);
		mm->UnregCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);
		UnlockData();
                ReleaseInterfaces();

		pthread_mutex_destroy(&data_mtx);
                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
        	LockData();
		adata = GetArenaData(arena);
		adata->enabled = true;

		mm->RegCallback(CB_ARENAACTION, ArenaActionCB, arena); // AA_CREATE is fired after MM_ATACH (AA_PRECREATE is not)
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->RegCallback(CB_KILL, KillCB, arena);
		mm->RegCallback(CB_SPAWN, SpawnCB, arena);
		mm->RegCallback(CB_PPK, PPKCB, arena);
		mm->RegCallback(CB_PLAYERDAMAGE, PlayerDamageCB, arena);
		mm->RegCallback(CB_ITEMS_CHANGED, HCItemsChanged, arena);
		mm->RegAdviser(&ppkadv, arena);

		/* cfghelp: AdvDamage:ArenaIdentifier, arena, string, mod: hscore_database
		 * String to group arena's that share ship health. Defaults to the arena base name. */
		arenaIdent = cfg->GetStr(arena->cfg, "AdvDamage", "ArenaIdentifier");
		
		if (arenaIdent == NULL)
			arenaIdent = arena->basename; // default value
		
		arenaGroupData = HashGetOne(&arenaGroups, arenaIdent);
		if (!arenaGroupData)
		{
			arenaGroupData = NewArenaGroupData(arenaIdent);
			HashAdd(&arenaGroups, arenaIdent, arenaGroupData);
		}
		
		adata->arenaGroup = arenaGroupData;
		ReadConfig(arena);
		
		cmd->AddCommand("resetturrets", Cresetturrets, arena, NULL);
		cmd->AddCommand("health", Chealth, arena, NULL);
		
		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena)
			{
				PlayerActionCB(p, PA_ENTERARENA, arena);
				if (p->flags.sent_ppk)
					PlayerActionCB(p, PA_ENTERGAME, arena);
			}


		}
		pd->Unlock();
		UnlockData();

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
        	LockData();
		adata = GetArenaData(arena);
		
		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena)
			{
				PlayerActionCB(p, PA_LEAVEARENA, arena);
			}
		}
		pd->Unlock();
		
		if (adata->fakePlayer)
		{
			fake->EndFaked(adata->fakePlayer);
			adata->fakePlayer = NULL;
		}
		adata->enabled = false;
		adata->arenaGroup = NULL;

		cmd->RemoveCommand("resetturrets", Cresetturrets, arena);
		cmd->RemoveCommand("health", Chealth, arena);
		mm->UnregCallback(CB_ARENAACTION, ArenaActionCB, arena);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->UnregCallback(CB_KILL, KillCB, arena);
		mm->UnregCallback(CB_SPAWN, SpawnCB, arena);
		mm->UnregCallback(CB_PPK, PPKCB, arena);
		mm->UnregCallback(CB_PLAYERDAMAGE, PlayerDamageCB, arena);
		mm->UnregCallback(CB_ITEMS_CHANGED, HCItemsChanged, arena);
		mm->UnregAdviser(&ppkadv, arena);

		UnlockData();
                return MM_OK;
        }

        return MM_FAIL;
}

