#include "asss.h"


static Imodman   *mm;
static Ilogman   *lm;
static Inet      *net;
static Icmdman   *cmd;
static Ichat     *chat;


static helptext_t ufo_help =
        "Targets: player\n"
        "Args: none\n"
        "Toggles UFO on a player.\n";

static void Cufo(const char *command, const char *params, Player *p, const Target *target)
{
        unsigned char currentUFOStatus;

        if (target->type == T_PLAYER)
        {
                Player *t = target->u.p;

                currentUFOStatus = t->position.status & STATUS_UFO;

                struct SimplePacket ufo =
                {
                        S2C_UFO, !currentUFOStatus
                };
                net->SendToOne(t, (byte*)&ufo, 2, NET_RELIABLE);
                chat->SendMessage(p, "UFO set to %d for %s", !currentUFOStatus, t->name);

        }

}

static void ReleaseInterfaces()
{
        mm->ReleaseInterface(lm);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(net);
}

EXPORT const char info_ufo[] = "Dr Brain <drbrain@gmail.com>";
EXPORT int MM_ufo(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm      = mm_;
                lm      = mm->GetInterface(I_LOGMAN, ALLARENAS);
                cmd     = mm->GetInterface(I_CMDMAN, ALLARENAS);
                chat    = mm->GetInterface(I_CHAT, ALLARENAS);
                net     = mm->GetInterface(I_NET, ALLARENAS);

                if (!lm || !cmd || !chat || !net)
                {
                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                cmd->AddCommand("ufo", Cufo, ALLARENAS, ufo_help);

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                cmd->RemoveCommand("ufo", Cufo, ALLARENAS);
                ReleaseInterfaces();

                return MM_OK;
        }

        return MM_FAIL;
}
