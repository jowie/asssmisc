/*
 * trigger:autobrick
 *
 * [AutoBrick]
 * Brick0=x1,y1,x2,y2
 * Team0=8025
 *
 * MAX_BRICKS bricks for each arena. Brick time is read from the .conf
 *  No team means spec team (also read from .conf). Bricks must
 *  be consecutive, fx: Brick0, Brick1, etc. They must also be
 *  horizontal or vertical.
 *
 * 9/07/03 - Created by Smong
 *
 * 17/12/03 - Updated for 1.1.6 (bricks are now in their own module)
 *
 * 24/01/04 - Removed calls to Log()
 *
 * 27/08/04 - cleaned it up.
 *
 * 28/08/04 - oopsie, bad copy/paste job - fixed.
 *
 * 21/09/06 - increased brick limit from 8 to 32
 *
 */

#include <stdio.h>
#include "asss.h"

#define MAX_BRICKS 32

typedef struct
{
	int x1, y1;
	int x2, y2;
	int freq;
} BrickData;

typedef struct
{
	Arena *arena;
	int count; /* number of Bricks in *data */
	BrickData *data;
} ArenaData;


local void str_to_brick(const char *str, BrickData *bd);

local void conf_extract_bricks(ConfigHandle ch, ArenaData *ad);
local int  conf_count_bricks(ConfigHandle ch);

local int timer(void *ad_);
local void cleanup(void *ad_);


local Imodman     *mm;
local Iconfig     *cfg;
local Ibricks     *bricks;
local Imainloop   *mainloop;


local void str_to_brick(const char *str, BrickData *bd)
{
	sscanf(str, "%d,%d,%d,%d", &bd->x1, &bd->y1, &bd->x2, &bd->y2);
}

/* fill ad->data[] with brick info from a .conf */
local void conf_extract_bricks(ConfigHandle ch, ArenaData *ad)
{
	int i, specfreq;
	const char *value;
	char key[32];

	specfreq = cfg->GetInt(ch, "Team", "SpectatorFrequency", 8025);

	for(i = 0; i < ad->count; i++)
	{
		snprintf(key, 32, "Brick%d", i);
		value = cfg->GetStr(ch, "AutoBrick", key);
		if (!value) return;
		str_to_brick(value, &ad->data[i]);

		snprintf(key, 32, "Team%d", i);
		ad->data[i].freq = cfg->GetInt(ch, "AutoBrick", key, specfreq);
	}
}

/* find how many consecutive bricks there are in a .conf */
local int conf_count_bricks(ConfigHandle ch)
{
	int i;
	const char *value;
	char key[16];

	for(i = 0; i < MAX_BRICKS; i++)
	{
		snprintf(key, 16, "Brick%d", i);
		value = cfg->GetStr(ch, "AutoBrick", key);
		if (!value) break;
	}

	return i;
}

local int timer(void *ad_)
{
	ArenaData *ad = ad_;
	int i;

	for(i=0; i<ad->count; i++)
	{
		bricks->DropBrick(ad->arena, ad->data[i].freq, ad->data[i].x1,
			ad->data[i].y1, ad->data[i].x2, ad->data[i].y2);
	}

	return 1;
}

local void cleanup(void *ad_)
{
	ArenaData* ad = ad_;
	afree(ad->data);
	afree(ad);
}

EXPORT const char info_autobrick[] =
	"v1.3 smong <pfft.whatever@gmail.com> ("BUILDDATE")\n";

EXPORT int MM_autobrick(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;

		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		bricks = mm->GetInterface(I_BRICKS, ALLARENAS);
		mainloop = mm->GetInterface(I_MAINLOOP, ALLARENAS);
		if (!cfg || !bricks || !mainloop)
			return MM_FAIL;

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		mm->ReleaseInterface(mainloop);
		mm->ReleaseInterface(bricks);
		mm->ReleaseInterface(cfg);
		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		int count, bricktime;
		ArenaData *ad;

		/* allocate memory */
		count = conf_count_bricks(arena->cfg);
		ad = amalloc(sizeof(ArenaData));
		ad->arena = arena;
		ad->count = count;
		ad->data  = amalloc(count * sizeof(BrickData));

		/* read in bricks */
		conf_extract_bricks(arena->cfg, ad);

		/* add to timer */
		bricktime = cfg->GetInt(arena->cfg, "Brick", "BrickTime", 12000);
		bricktime = bricktime - 100; /* incase of lag */
		if (bricktime < 100) bricktime = 100; /* sanity check */
		mainloop->SetTimer(timer, 300, bricktime, ad, arena);
		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		/* clean-up timer */
		mainloop->CleanupTimer(timer, arena, cleanup);
		return MM_OK;
	}

	return MM_FAIL;
}

