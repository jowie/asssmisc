/* $Id: advbuy.h 35 2008-03-25 00:21:24Z joris $ */
#ifndef __ADVBUY_H
#define __ADVBUY_H

#define I_ADVBUY "advbuy-1"

typedef struct Iadvbuy
{
	INTERFACE_HEAD_DECL

	bool (*StartGame)(Arena *arena);
	void (*StopGame)(Arena *arena);

} Iadvbuy;

#endif
