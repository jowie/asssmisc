/* $Id: staticturret.c 363 2011-06-12 19:55:59Z joris $ */
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

//#define USE_ADVDAMAGE

#include "asss.h"
#include "fake.h"
#include "attrman.h"
#include "staticturret.h"
#include "packets/ppk.h"
#include "damage.h"
#include "objects.h"
#include "banners.h"
#ifdef USE_ADVDAMAGE
#include "advdamage.h"
#endif

#define STRUCTURES_FREQS 3

/* how far into the future to see. expected range 0-200. this setting should
 * make the bot shoot players coming towards it, rather than running away from
 * it. units: ticks */
#define BOT_PROJECT_FAVOUR (80)

/* how much to adjust the distance to another bot when scanning for targets.
 * higher numbers mean more likely to target humans. must be less than BOT_SIGHT
 * units: pixels */
#define BOT_HUMAN_FAVOUR (150)

/* same as above, but then for targets having stealth and / or cloak. Higher number means more likely to target a person without cloak / stealth */
#define STEALTH_FAVOUR (10)
#define CLOAK_FAVOUR (70)

/* One ship can be specified in the arena config to have a higher favour. Larger means more likely to target this ship */
#define SPECIFIC_SHIP_FAVOUR (100)

#define POSITIONPACKETINTERVAL (25)

/* How often to update the hpbar of a turret */
#define HPBAR_UPDATEINTERVAL (100)

#define PI (3.141592653589793)

#define TICK_GTE(a,b) (TICK_DIFF(a,b) >= 0)
#define tFREE(a) \
do \
{ \
        if (a) \
        { \
                afree(a); \
                a = NULL; \
        } \
} while (0)

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

static Imodman		*mm;
static Iarenaman 	*aman;
static Iplayerdata	*pd;
static Ilogman		*lm;
static Iattrman		*attrman;
static Iobjects 	*objs;
static Ifake 		*fake;
static Icmdman          *cmd;
static Iconfig		*cfg;
static Ichat		*chat;
static Idamage		*damage;
static Igame		*game;
static Imapdata		*mapdata;
static Ibanners		*banner;

static int arenaKey  = -1;
static int playerKey = -1;

// No longer needed to lock; Everything happends in the main thread (due to updates in the damage module)
//static pthread_mutex_t mylock_mtx = PTHREAD_MUTEX_INITIALIZER;
//#define LOCK   pthread_mutex_lock(&mylock_mtx)
//#define UNLOCK pthread_mutex_unlock(&mylock_mtx)


static LinkedList bots = LL_INITIALIZER; // active bots in this arena; (struct BotData*)
static LinkedList botsMarkedForRemoval = LL_INITIALIZER;

struct TurretType
{
	Arena *arena; // arena this turret type belongs to
	bool *objectsUsed; // array, same length as objectsallocated. must be free()d. 0 if no objectsAllocated
	int bot_count; // how many bots of this turret type are there?

	struct
	{
		const char *key;
		char *name; // Must be free()d; if NULL use a default name. This is the player name

		Banner *banner;

		int objectStart;
		int objectsAllocated; // 0 to use no lvz

		bool hpbar;
		bool doBuildSequence;

		int maxBots; // How many bots can this turret type have; -1 = infinite
	} config;
};

struct BotData
{
	Player *p; // the fake player
	Arena *arena;
	bool markedForRemoval;

	struct TurretType *turretType;

	bool lvzVisible;
	int relobjectid; // turretType->config.objectStart + hpbar_relobjectId; -1 if no lvz
	int hpbar_relobjectid; // adata->config.hpbar_objectStart + hpbar_relobjectId; -1 if no hpbar lvz
	ticks_t hpbar_lastUpdate;
	int hpbar_lastOffset;

	int spawns; //how many times has this bot spawned
	bool specced;
	ticks_t createdOn;
	ticks_t lastPositionUpdate;
	ticks_t spawnedOn;
	ticks_t deathOn; // 0 if currently alive; otherwise the bot is currently dead and sitting in spec waiting to respawn
	bool killed;

	int energy;
	ticks_t lastRecharge;
	ticks_t empshutdown_expiresat;

	bool buildSequence; // turret is currently in the build sequence? (can't fire weapons)
	int buildpoints; // goes up to config.energy; lastRecharge is used
	int buildbar_relobjectid; // adata->config.hpbar_objectStart + hpbar_relobjectId; -1 if no hpbar lvz
	ticks_t buildbar_lastUpdate;
	int buildbar_lastOffset;
	ticks_t lastBuild;

	struct C2SPosition pos;
	int arotation; // 0 - 40000
	int desiredRotation; // 0 - 40
	ticks_t lastFire;
	int weaponSpeed;

	Player *targetting; // Current player we are targeting

	struct
	{
		bool infiniteRespawn;
		int weaponspeed;
		int shipRadius;

		int x;
		int y;
		int freq;

		//from attrman:
		int energy;
		int recharge;
		int buildSpeed;

                int  weapon_type;
		int  weapon_level;
		int  weapon_delay;
		int  weapon_fireEnergy;
		int  weapon_sightPixels;
		int  weapon_shrapnelLevel;
		int  weapon_shrapnelCount;
		bool weapon_shrapnelBouncing;
		bool weapon_multifire;
		bool weapon_waitForGoodShot; // Continuum only has 40 rotations, this settings causes to wait with firing before the player can be actually hit


		int rotationSpeed;

		ticks_t timeout; // How long will this turret stay in the arena (0 = always)
		ticks_t respawnDelay; // How long before this turret respawns after death
		int respawnCount; // How often will this bot respawn (0 = infinite)

		bool ufo;
		bool xradar;

		int ship;
		int requiredPower;
		int bounty;


        } config;
};

struct ArenaData
{
	bool initialized;
	bool running;
	HashTable *turretTypes; // (struct TurretType*)
	bool *hpbar_objectsUsed; // array, same length as hpbar_objectsallocated
	bool *buildbar_objectsUsed; // array, same length as buildbar_objectsallocated
	int bot_count; // how many bots are there

	AttrmanSetter settingSetter; // Used to set our ini entries for config that can be modified using attrman


	bool respawn_frozen[STRUCTURES_FREQS];
	ticks_t respawn_frozen_time[STRUCTURES_FREQS];
	ticks_t respawn_unfrozen_time[STRUCTURES_FREQS];
	int power[STRUCTURES_FREQS];
	bool powerset[STRUCTURES_FREQS]; //has the power been set this game?

	struct
	{
		bool hosted; // Requires the game to be started by the interface; If this is false, the entire duration of the arena is seen as the game

		int hpbar_objectStart;
		int hpbar_objectsAllocated;
		int hpbar_imageStart;
		int hpbar_imagesAllocated;
		int hpbar_XOffset;
		int hpbar_YOffset;

		int buildbar_objectStart;
		int buildbar_objectsAllocated;
		int buildbar_imageStart;
		int buildbar_imagesAllocated;
		int buildbar_XOffset;
		int buildbar_YOffset;

		int maxBots; // will also have to check for hpbar_objectsAllocated
		int shipFavour;

		int turretPlacementRange;
	} config;
};

// there already is a struct PlayerData....?
#define PlayerData PlayerData_
struct PlayerData
{
	Arena *arena;
	bool logBanner;
};

static struct ArenaData* GetArenaData(Arena *arena);
static struct PlayerData* GetPlayerData(Player *p);
static void ReadSettings(Arena *arena);
static int CleanupTurretTypeData(const char *key, void *val, void *clos);
static void CleanupArenaSettings(Arena *arena);
static enum ADDBOT_ERROR AddBotByKey(Arena *arena, const char *key, int x, int y, int freq, bool infiniteRespawn, bool noLocationCheck);
static enum ADDBOT_ERROR AddBot(struct TurretType *turretType, int x, int y, int freq, bool infiniteRespawn, bool noLocationCheck);
static void RemoveBot(struct BotData *bot);
static void StartGameInt(Arena *arena);
static void StartGame(Arena *arena);
static void StopGameInt(Arena *arena);
static void StopGame(Arena *arena);
static void BotKill(Player *p, Player *killer, void *clos);
static void BotRespawn(Player *p, void *clos);
static void BotDamage(Player *fake, Player *firedBy, int dist, int damage, int wtype, int level, bool bouncing, int emptime, void *clos);
static void UpdateBot(struct BotData *bot);
static void MainLoop();
static void Cspawnturret(const char *command, const char *params, Player *p, const Target *target);
static void UpdateHPBar(struct BotData *bot);
static void UpdateBuildBar(struct BotData *bot);
static void UpdateBot(struct BotData *bot);
static void FireWeapon(struct BotData *bot);
static void RotateBot(struct BotData *bot);
static void UpdateHPBar(struct BotData *bot);
static unsigned char FireAngle(double x, double y);
static inline unsigned char FireControl (double srcx, double srcy, double dstx, double dsty, double dxspeed, double dyspeed, double projspeed);
static void CancelPlayerTarget(Player *p);
static void Kill(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green);
static void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq);
static void PlayerAction(Player *p, int action, Arena *arena);
static bool fitsOnMap(Arena *arena, int x, int y, int radius);
static bool is_pathclear(Arena *arena, int x1, int y1, int x2, int y2, bool isthor);
static Player *GetNearestPlayer(struct BotData *bot, bool enemyOnly);
static void DoDamage (Arena *arena, Player *killer, int x, int y, int damage, int radius, int immuneFreq);

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<staticturret> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}


static char* itoa(int value, char* result, int base)
{
	// check that the base if valid
	if (base < 2 || base > 36) { *result = '\0'; return result; }

	char* ptr = result, *ptr1 = result, tmp_char;
	int tmp_value;

	do
	{
		tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	} while ( value );

	// Apply negative sign
	if (tmp_value < 0) *ptr++ = '-';
	*ptr-- = '\0';
	while(ptr1 < ptr) {
		tmp_char = *ptr;
		*ptr--= *ptr1;
		*ptr1++ = tmp_char;
	}
	return result;
}


static struct ArenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        struct ArenaData *adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);


        return adata;
}

static struct PlayerData* GetPlayerData(Player *p)
{
        assertlm(p);
        struct PlayerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);

        if (p->arena && p->arena != pdata->arena)
        {
                pdata->arena = p->arena;
        	//...
        }
        return pdata;
}

static long LHypot(register long dx, register long dy)
{
        register unsigned long r, dd;

        dd = dx*dx+dy*dy;

        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;

        /* initial hypotenuse guess (from Gems) */
        r = (dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));

        if (r == 0) return (long)r;

        /* converge 3 times */
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;

        return (long)r;
}

static char* strcpyalloc(const char *s)
{
        char *ret = NULL;
        int len;
        size_t size;

        if (s)
        {
        	len = strlen(s) + 1; //including \0
                size = len * sizeof(char);
                if (size > 0)
                {
                        ret = amalloc(size);
                        astrncpy(ret, s, len);
                }
        }

        return ret;
}

static char* cfgGetStrAlloc(ConfigHandle ch, const char *section, const char *key)
{
        const char *s = cfg->GetStr(ch, section, key);

        return strcpyalloc(s);
}

static void ReadSettings(Arena *arena)
{
	struct ArenaData *adata;
	ConfigHandle ch;
	char buf[256];
	char buf2[256];
	char bannerBuf[3];
	int a, b;
	const char *s;
	struct TurretType *turretType;
	Target tgt;

	assertlm(arena);
	ch = arena->cfg;
	tgt.type = T_ARENA;
	tgt.u.arena = arena;
	adata = GetArenaData(arena);

	assertlm(!adata->initialized);

	adata->turretTypes = HashAlloc();

	adata->config.hosted = !!cfg->GetInt(ch, "staticturret", "hosted", false);

	adata->config.hpbar_objectStart = cfg->GetInt(ch, "staticturret", "HPBar_ObjectStart", -1);
	adata->config.hpbar_objectsAllocated = cfg->GetInt(ch, "staticturret", "HPBar_ObjectsAllocated", 0);
	adata->config.hpbar_imageStart = cfg->GetInt(ch, "staticturret", "HPBar_ImageStart", 0);
	adata->config.hpbar_imagesAllocated = cfg->GetInt(ch, "staticturret", "HPBar_ImagesAllocated", 0);
	adata->config.hpbar_XOffset = cfg->GetInt(ch, "staticturret", "HPBar_XOffset", 0);
	adata->config.hpbar_YOffset = cfg->GetInt(ch, "staticturret", "HPBar_YOffset", 0);

	adata->config.buildbar_objectStart = cfg->GetInt(ch, "staticturret", "BuildBar_ObjectStart", -1);
	adata->config.buildbar_objectsAllocated = cfg->GetInt(ch, "staticturret", "BuildBar_ObjectsAllocated", 0);
	adata->config.buildbar_imageStart = cfg->GetInt(ch, "staticturret", "BuildBar_ImageStart", 0);
	adata->config.buildbar_imagesAllocated = cfg->GetInt(ch, "staticturret", "BuildBar_ImagesAllocated", 0);
	adata->config.buildbar_XOffset = cfg->GetInt(ch, "staticturret", "BuildBar_XOffset", 0);
	adata->config.buildbar_YOffset = cfg->GetInt(ch, "staticturret", "BuildBar_YOffset", 0);

	adata->config.maxBots = cfg->GetInt(ch, "staticturret", "MaxBots", -1);
	adata->config.shipFavour = cfg->GetInt(ch, "staticturret", "ShipFavour", 0) - 1;
	adata->config.turretPlacementRange = cfg->GetInt(ch, "staticturret", "TurretPlacementRange", 0);

	attrman->Lock();
	adata->settingSetter = attrman->RegisterSetter();
	attrman->UnLock();


	CLIP(adata->config.hpbar_objectsAllocated, 0, 32767);
	CLIP(adata->config.hpbar_objectStart, -1, 32767 - adata->config.hpbar_objectsAllocated);
	CLIP(adata->config.hpbar_imagesAllocated, 0, 255);
	CLIP(adata->config.hpbar_imageStart, 0, 255 - adata->config.hpbar_imagesAllocated);

	if (adata->config.hpbar_objectsAllocated)
		adata->hpbar_objectsUsed = amalloc(sizeof(bool) * adata->config.hpbar_objectsAllocated);

	if (adata->config.buildbar_objectsAllocated)
		adata->buildbar_objectsUsed = amalloc(sizeof(bool) * adata->config.buildbar_objectsAllocated);

	for (a = 0; a < 100; a++)
	{
		snprintf(buf, sizeof(buf), "turret%d", a);
		s = cfg->GetStr(ch, "staticturret", buf);

		if (!s) break;
		if (HashGetOne(adata->turretTypes, s))
		{
			lm->LogA(L_WARN, "staticturret", arena, "Turret with the key %s is already defined while parsing staticturret:turret%d", s, a);
			continue;
		}


		turretType = amalloc(sizeof(struct TurretType));
		turretType->config.key = HashAdd(adata->turretTypes, s, turretType);
		turretType->arena = arena;

		snprintf(buf, sizeof(buf), "staticturret_%s", s);

		turretType->config.name = cfgGetStrAlloc(ch, buf, "Name");

		s = cfg->GetStr(ch, buf, "Banner");
		if (s)
		{
			assertlm(sizeof(Banner) == 96 * sizeof(u8));

			if (strlen(s) != 96 * 2)
			{
				lm->LogA(L_WARN, "staticturret", arena, "Incorrect length for %s:banner. Should be 192 hexadecimal characters long", buf);
			}
			else
			{
				bannerBuf[2] = 0;
				turretType->config.banner = amalloc(sizeof(Banner));
				for (b = 0; b < 96; b++)
				{
					bannerBuf[0] = s[b*2];
					bannerBuf[1] = s[b*2+1];
					turretType->config.banner->data[b] = (u8) strtoul(bannerBuf, NULL, 16);
				}
			}
		}

		turretType->config.objectStart = cfg->GetInt(ch, buf, "ObjectStart", -1);
		turretType->config.objectsAllocated = cfg->GetInt(ch, buf, "ObjectsAllocated", 0);
		turretType->config.hpbar = !!cfg->GetInt(ch, buf, "ShowHealthBar", 0);
		turretType->config.maxBots = cfg->GetInt(ch, buf, "MaxBots", -1);
		turretType->config.doBuildSequence = !!cfg->GetInt(ch, buf, "DoBuildSequence", 0);


		if (turretType->config.objectStart < 0 || !turretType->config.objectsAllocated)
		{ // No LVZ
			turretType->config.objectStart = 0;
			turretType->config.objectsAllocated = 0;
		}

		CLIP(turretType->config.objectsAllocated, 0, 32767);
		CLIP(turretType->config.objectStart, -1, 32767 - turretType->config.objectsAllocated);

		if (turretType->config.objectsAllocated)
			turretType->objectsUsed = amalloc(sizeof(bool) * turretType->config.objectsAllocated);
		else
			turretType->objectsUsed = NULL;

		attrman->Lock();
		#define tR(ATTR,DEF) \
		snprintf(buf2, sizeof(buf2), "staticturret::%s:" #ATTR, turretType->config.key); \
		attrman->SetValue(&tgt, adata->settingSetter, buf2, true, ATTRMAN_NO_SHIP, cfg->GetInt(ch, buf, #ATTR, DEF))

		tR(Ship, 1);
		tR(Energy, 1000);
		tR(Recharge, 1150);
		tR(BuildSpeed, 1000);
		tR(WeaponType, W_BULLET);
		tR(WeaponLevel, 1);
		tR(WeaponDelay, 100);
		tR(WeaponFireEnergy, 0);
		tR(WeaponSightPixels, 160);
		tR(WeaponShrapnelLevel, 1);
		tR(WeaponShrapnelCount, 0);
		tR(WeaponShrapnelBouncing, 0);
		tR(WeaponMultifire, 0);
		tR(WeaponWaitForGoodShot, 0);
		tR(RotationSpeed, -1);
		tR(Timeout, 1500);
		tR(RespawnDelay, 6000);
		tR(XRadar, 1);
		tR(RequiredPower, 0);
		tR(RespawnCount, 1);
		tR(Ufo, 0);
		tR(ShowHealthBar, 0);
		tR(Bounty, 0);

		#undef tR
		attrman->UnLock();
	}

	adata->initialized = true;
}


static void ReadBotConfig(struct BotData *bot, bool botCreation)
{
	char buf[256];
	const char *key;
	Target tgt;
	const char *shipName;

	key = bot->turretType->config.key;
	tgt.type = T_FREQ;
	tgt.u.freq.freq = bot->config.freq;
	tgt.u.freq.arena = bot->arena;


	attrman->Lock();
	#define tR(VAR,ATTR) \
	snprintf(buf, sizeof(buf), "staticturret::%s:" #ATTR, key); \
	bot->config.VAR = attrman->GetTotalValue(&tgt, buf, ATTRMAN_NO_SHIP, NULL)

	tR(energy, Energy);
	tR(recharge, Recharge);
	tR(buildSpeed, BuildSpeed);
	tR(weapon_type, WeaponType);
	tR(weapon_level, WeaponLevel);
	tR(weapon_delay, WeaponDelay);
	tR(weapon_fireEnergy, WeaponFireEnergy);
	tR(weapon_sightPixels, WeaponSightPixels);
	tR(weapon_shrapnelLevel, WeaponShrapnelLevel);
	tR(weapon_shrapnelCount, WeaponShrapnelCount);
	tR(weapon_shrapnelBouncing, WeaponShrapnelBouncing);
	tR(weapon_multifire, WeaponMultifire);
	tR(weapon_waitForGoodShot, WeaponWaitForGoodShot);
	tR(rotationSpeed, RotationSpeed);
	tR(timeout, Timeout);
	tR(respawnDelay, RespawnDelay);
	tR(xradar, XRadar);
	tR(requiredPower, RequiredPower);
	tR(bounty, Bounty);
	

	bot->config.timeout = TICK_MAKE(bot->config.timeout);

	if (botCreation) //it is dangerous to change the following while the bot is already running
	{
		tR(respawnCount, RespawnCount);
		tR(ufo, Ufo);
		tR(ship, Ship);

		bot->config.ship--;
		CLIP(bot->config.ship, 0, 7);
	}

	#undef tR
	
	attrman->UnLock();


	bot->config.weapon_level--;
	bot->config.weapon_shrapnelLevel--;


	CLIP(bot->config.weapon_type, W_BULLET, W_THOR);
	CLIP(bot->config.weapon_level, 0, 3);
	CLIP(bot->config.weapon_shrapnelLevel, 0, 3);
	bot->config.weapon_shrapnelBouncing = !!bot->config.weapon_shrapnelBouncing;
	bot->config.weapon_multifire = !!bot->config.weapon_multifire;
	bot->config.weapon_waitForGoodShot = !!bot->config.weapon_waitForGoodShot;

	shipName = cfg->SHIP_NAMES[bot->config.ship];
	bot->config.shipRadius = cfg->GetInt(bot->arena->cfg, shipName, "Radius", 14);
	if (bot->config.shipRadius <= 0) bot->config.shipRadius = 14;

	if (bot->config.weapon_type == W_BULLET || bot->config.weapon_type == W_BOUNCEBULLET)
	{
		bot->config.weaponspeed = cfg->GetInt(bot->arena->cfg, shipName, "BulletSpeed", 3000);
	}
	else if (bot->config.weapon_type == W_BOMB || bot->config.weapon_type == W_PROXBOMB || bot->config.weapon_type == W_THOR)
	{
		bot->config.weaponspeed = cfg->GetInt(bot->arena->cfg, shipName, "BombSpeed", 2000);
		bot->config.weapon_multifire = false;
	}
	else
	{
		bot->config.weaponspeed = 2500;
		bot->config.weapon_multifire = false;
	}
}


static int CleanupTurretTypeData(const char *key, void *val, void *clos)
{
	struct TurretType *turret;
	turret = (struct TurretType *) val;

	tFREE(turret->objectsUsed);
	tFREE(turret->config.name);
	tFREE(turret->config.banner);

	afree(turret);
	return 1;
}


static void CleanupArenaSettings(Arena *arena)
{
	struct ArenaData *adata;
	Link *l;
	Link *next;
	struct BotData *bot;

	assertlm(arena);
	adata = GetArenaData(arena);
	if (!adata->initialized) return;


	for (l = LLGetHead(&bots); l; l = next)
	{
		next = l->next;
		bot = l->data;

		if (bot->arena == arena)
			RemoveBot(l->data);
	}

	for (l = LLGetHead(&botsMarkedForRemoval); l; l = next)
	{
		next = l->next;
		bot = l->data;

		if (bot->arena == arena)
			RemoveBot(l->data);
	}



	HashEnum(adata->turretTypes, CleanupTurretTypeData, NULL);
	HashFree(adata->turretTypes);

	tFREE(adata->hpbar_objectsUsed);
	tFREE(adata->buildbar_objectsUsed);
	attrman->Lock();
	attrman->UnregisterSetter(adata->settingSetter);
	attrman->UnLock();
	adata->initialized = false;
}
static enum ADDBOT_ERROR AddBotByKeyInt(Arena *arena, const char *key, int x, int y, int freq, bool infiniteRespawn, bool noLocationCheck)
{
	enum ADDBOT_ERROR result;


	result = AddBotByKey(arena, key, x,  y, freq, infiniteRespawn, noLocationCheck);

	return result;
}

static enum ADDBOT_ERROR AddBotByKey(Arena *arena, const char *key, int x, int y, int freq, bool infiniteRespawn, bool noLocationCheck)
{
	struct ArenaData *adata;
	struct TurretType *turretType;

	assertlm(arena);
	assertlm(key);
	assertlm(freq >= 0 && freq <= 9999);

	adata = GetArenaData(arena);
	if (!adata->initialized)
		return ADDBOT_ILLEGAL_ARENA;

	turretType = (struct TurretType *) HashGetOne(adata->turretTypes, key);
	if (!turretType)
		return ADDBOT_UNKNOWN_TYPE;


	return AddBot(turretType, x, y, freq, infiniteRespawn, noLocationCheck);
}

static enum ADDBOT_ERROR AddBot(struct TurretType *turretType, int x, int y, int freq, bool infiniteRespawn, bool noLocationCheck)
{
	Arena *arena;
	struct ArenaData *adata;
	struct BotData *bot;
	Target tgt;
	int a;
	Link *l;
	int relobjectid, hpbar_relobjectid, buildbar_relobjectid;
	ticks_t now = current_ticks();

	assertlm(turretType);
	assertlm(freq >= 0 && freq <= 9999);
	CLIP(x, 0, 1023 << 4);

	arena = turretType->arena;
	assertlm(arena);
	adata = GetArenaData(arena);

	tgt.type = T_ARENA;
	tgt.u.arena = arena;

	if (turretType->config.maxBots >= 0 && turretType->bot_count >= turretType->config.maxBots) // -1 = infinite
		return ADDBOT_MAX_REACHED_FOR_BOTTYPE;


	if (adata->config.maxBots >= 0 && adata->bot_count > adata->config.maxBots)
		return ADDBOT_MAX_REACHED_FOR_ARENA;

	relobjectid = -1;
	if (turretType->config.objectsAllocated) //find a lvz we can use
	{
		for (a = 0; a < turretType->config.objectsAllocated; a++)
		{
			if (!turretType->objectsUsed[a])
			{
				relobjectid = a;
				break;
			}
		}

		if (relobjectid < 0) //no lvz left
			return ADDBOT_MAX_REACHED_FOR_BOTTYPE;

	}


	hpbar_relobjectid = -1;
	if (turretType->config.hpbar) // find a hpbar lvz we can use
	{
		for (a = 0; a < adata->config.hpbar_objectsAllocated; a++)
		{
			if (!adata->hpbar_objectsUsed[a])
			{
				hpbar_relobjectid = a;
				break;
			}
		}

		if (hpbar_relobjectid < 0)
			return ADDBOT_MAX_REACHED_FOR_ARENA;
	}

	buildbar_relobjectid = -1;
	if (turretType->config.doBuildSequence && !infiniteRespawn)
	{
		for (a = 0; a < adata->config.buildbar_objectsAllocated; a++)
		{
			if (!adata->buildbar_objectsUsed[a])
			{
				buildbar_relobjectid = a;
				break;
			}
		}

		if (buildbar_relobjectid < 0)
			return ADDBOT_BUILDING_IN_PROGRESS;
	}

	if (!noLocationCheck && adata->config.turretPlacementRange > 0)
	{

		for (l = LLGetHead(&bots); l; l = l->next)
		{
			bot = (struct BotData *) l->data;
			if (bot->arena == arena)
			{
				if (LHypot(x - bot->pos.x, y - bot->pos.y) < adata->config.turretPlacementRange)
					return ADDBOT_TO_CLOSE_TO_OTHER_BOT;
			}
		}

	}



	bot = amalloc(sizeof(struct BotData));
	bot->turretType = turretType;
	bot->arena = arena;
	bot->createdOn = now;
	bot->lastFire = now;
	bot->lastRecharge = now;
	bot->lastBuild = now;
	bot->lastPositionUpdate = now;
	bot->hpbar_lastUpdate = now;
	bot->buildbar_lastUpdate = now;
	bot->config.x = x;
	bot->config.y = y;
	bot->config.freq = freq;
	bot->config.infiniteRespawn = infiniteRespawn;
	bot->killed = false;

	ReadBotConfig(bot, true);
	if (!fitsOnMap(arena, x, y, bot->config.shipRadius))
	{
		afree(bot);
		return ADDBOT_CAN_NOT_BE_PLACED_ON_MAP;
	}

	bot->relobjectid = relobjectid;
	if (relobjectid >= 0)
		turretType->objectsUsed[relobjectid] = true;


	bot->hpbar_relobjectid = hpbar_relobjectid;
	if (hpbar_relobjectid >= 0)
		adata->hpbar_objectsUsed[hpbar_relobjectid] = true;

	bot->hpbar_lastOffset = -1;

	bot->buildbar_relobjectid = buildbar_relobjectid;
	if (buildbar_relobjectid >= 0)
	{
		adata->buildbar_objectsUsed[buildbar_relobjectid] = true;
		bot->buildSequence = true;
		bot->buildpoints = 0;
	}
	bot->buildbar_lastOffset = -1;

	bot->pos.type = C2S_POSITION;
	bot->pos.rotation = 0;
	bot->pos.x = x;
	bot->pos.y = y;
	bot->pos.xspeed = 0;
	bot->pos.yspeed = 0;
	bot->pos.bounty = bot->config.bounty;

	if (bot->config.ufo)
		bot->pos.status = STATUS_CLOAK | STATUS_STEALTH | STATUS_UFO;

	bot->lvzVisible = false;

	// ~ is ordered last in the player list
	bot->p = fake->CreateFakePlayer(turretType->config.name ? turretType->config.name : "~Turret", arena, bot->config.ship, freq);
	bot->p->flags.is_dead = 0;
	if (turretType->config.banner)
		banner->SetBanner(bot->p, turretType->config.banner);

	bot->pos.energy = bot->pos.extra.energy = bot->energy = bot->config.energy;
	bot->empshutdown_expiresat = 0;

	turretType->bot_count++;
	adata->bot_count++;

	LLAdd(&bots, bot);

	damage->AddFake(bot->p, &bot->pos, false, BotKill, BotRespawn, BotDamage, bot);
	bot->spawns = 1;

	BotRespawn(bot->p, bot);

	return ADDBOT_OK;
}

static void RemoveBot(struct BotData *bot)
{
	Arena *arena;
	struct ArenaData *adata;
	struct TurretType *turretType;

	arena = bot->arena;
	assertlm(arena);
	adata = GetArenaData(arena);
	turretType = bot->turretType;
	assertlm(turretType);

	if (bot->relobjectid >= 0) // free the LVZ for re use
		turretType->objectsUsed[bot->relobjectid] = false;

	if (bot->hpbar_relobjectid >= 0) // free the LVZ for re use
		adata->hpbar_objectsUsed[bot->hpbar_relobjectid] = false;

	if (bot->buildbar_relobjectid >= 0)
		adata->buildbar_objectsUsed[bot->buildbar_relobjectid] = false;

	if (bot->p)
	{
		fake->EndFaked(bot->p);
		damage->RemoveFake(bot->p);
	}

	if (bot->lvzVisible)
	{
		if (bot->relobjectid >= 0)
			objs->Reset(arena, turretType->config.objectStart + bot->relobjectid);

		if (bot->hpbar_relobjectid >= 0)
			objs->Reset(arena, adata->config.hpbar_objectStart + bot->hpbar_relobjectid);

		if (bot->buildbar_relobjectid >= 0)
			objs->Reset(arena, adata->config.buildbar_objectStart + bot->buildbar_relobjectid);

	}
	bot->lvzVisible = false;


	turretType->bot_count--;
	adata->bot_count--;


	LLRemove(&bots, bot);
	LLRemove(&botsMarkedForRemoval, bot);

	afree(bot);
}

static void NewPlayerCB(Player *p, int isnew)
{
	Arena *arena;
	Link *link, *l, *next;
	struct ArenaData *adata;
	struct BotData *bot;

	if (p->type != T_FAKE || isnew)
		return;

	aman->Lock();
	FOR_EACH_ARENA(arena)
	{
		adata = GetArenaData(arena);
		for (l = LLGetHead(&bots); l; l = next)
		{
			next = l->next; //link may get removed
			bot = (struct BotData *) l->data;

			if (bot->p == p)
			{
				damage->RemoveFake(bot->p);
				bot->p = NULL; // so that we do not call EndFake 2x
				RemoveBot(bot);

			}
		}
	}
	aman->Unlock();
}

static void StartGameInt(Arena *arena)
{
	struct ArenaData *adata;

	adata = GetArenaData(arena);
	if (!adata->config.hosted)
	{

		return;
	}
	StartGame(arena);

}

static void StartGame(Arena *arena)
{
	char buf[256];
	struct ArenaData *adata;
	const char *s;
	int a;
	int x, y, freq;
	int res;
	ConfigHandle ch;

	adata = GetArenaData(arena);
	if (!adata->initialized)
		return;

	for (a = 0; a < STRUCTURES_FREQS; a++)
	{
		adata->power[a] = 0;
		adata->powerset[a] = false;
		adata->respawn_frozen[a] = false;
		adata->respawn_frozen_time[a] = 0;
		adata->respawn_unfrozen_time[a] = 0;
	}


	ch = arena->cfg;

	for (a = 0; a < 100; a++)
	{
		snprintf(buf, sizeof(buf), "spawn%d", a);
		s = cfg->GetStr(ch, "staticturret", buf);
		if (!s) break;

		res = sscanf(s, "%d, %d, %d, %255s", &x, &y, &freq, buf);
		buf[255] = 0;

		if (res < 4)
		{
			lm->LogA(L_WARN, "staticturret", arena, "Invalid format for staticturret:spawn%d. Use: x, y, freq, type", a);
			continue;
		}

		if (freq < 0 || freq > 9999)
		{
			lm->LogA(L_WARN, "staticturret", arena, "Invalid value for staticturret:spawn%d. Illegal freq", a);
			continue;
		}

		AddBotByKey(arena, buf, (x << 4) + 8, (y << 4) + 8, freq, true, true);
	}

	adata->running = true;
}


static void StopGameInt(Arena *arena)
{
	struct ArenaData *adata;


	adata = GetArenaData(arena);
	if (!adata->config.hosted)
	{

		return;
	}
	StopGame(arena);

}

static void StopGame(Arena *arena)
{
	struct ArenaData *adata;
	struct BotData *bot;
	Link *l;
	Link *next;
	int a;

	assertlm(arena);

	adata = GetArenaData(arena);
	if (!adata->initialized)
		return;


	for (a = 0; a < STRUCTURES_FREQS; a++)
	{
		adata->power[a] = 0;
		adata->powerset[a] = false;
		adata->respawn_frozen[a] = false;
		adata->respawn_frozen_time[a] = 0;
		adata->respawn_unfrozen_time[a] = 0;
	}


	for (l = LLGetHead(&bots); l; l = next)
	{
		next = l->next; //node may get removed
		bot = (struct BotData *) l->data;

		if (bot->arena == arena)
		{
			LLRemove(&bots, bot);
			LLAdd(&botsMarkedForRemoval, bot);
			bot->markedForRemoval = true;
		}
	}


	adata->running = false;
}


// called by damage
static void BotKill(Player *p, Player *killer, void *clos)
{
	struct BotData *bot;
	struct TurretType *turretType;
	struct ArenaData *adata;
	Target tgt;


	bot = (struct BotData *) clos;
	turretType = bot->turretType;
	adata = GetArenaData(bot->arena);

	bot->deathOn = current_ticks();

	// speccing the bot will be done at a later moment in UpdateBot()
	bot->specced = false;

	tgt.type = T_ARENA;
	tgt.u.arena = bot->arena;
	if (bot->lvzVisible)
	{
		if (bot->relobjectid >= 0)
			objs->Toggle(&tgt, turretType->config.objectStart + bot->relobjectid, 0);

		if (bot->hpbar_relobjectid >= 0)
			objs->Toggle(&tgt, adata->config.hpbar_objectStart + bot->hpbar_relobjectid, 0);

		if (bot->buildbar_relobjectid >= 0)
			objs->Reset(bot->arena, adata->config.buildbar_objectStart + bot->buildbar_relobjectid);
	}
	bot->lvzVisible = false;

	if (bot->buildbar_relobjectid >= 0)
	{
		adata->buildbar_objectsUsed[bot->buildbar_relobjectid] = false;
		bot->buildbar_relobjectid = -1;
	}
}

// called by damage
static void BotRespawn(Player *p, void *clos)
{
	struct BotData *bot;
	struct TurretType *turretType;
	struct ArenaData *adata;
	Target tgt;


	bot = (struct BotData *) clos;
	turretType = bot->turretType;
	adata = GetArenaData(bot->arena);

	tgt.type = T_ARENA;
	tgt.u.arena = bot->arena;

	bot->deathOn = 0;
	bot->spawnedOn = current_ticks();

	game->SetShipAndFreq(p, bot->config.ship, bot->config.freq);
	bot->pos.x = bot->config.x;
	bot->pos.y = bot->config.y;
	bot->pos.bounty = bot->config.bounty;
	bot->pos.energy = bot->pos.extra.energy = bot->energy = bot->config.energy;
	bot->lastRecharge = current_ticks();
	bot->lastBuild = current_ticks();
	bot->empshutdown_expiresat = 0;
	bot->killed = false;
	bot->p->flags.is_dead = 0;

	if (!bot->lvzVisible)
	{
		if (bot->relobjectid >= 0)
		{
			objs->Move(&tgt, turretType->config.objectStart + bot->relobjectid, bot->pos.x - 16, bot->pos.y - 16, 0, 0);
			objs->Toggle(&tgt, turretType->config.objectStart + bot->relobjectid, 1);
		}

		if (bot->hpbar_relobjectid >= 0)
		{
			objs->Move(&tgt, adata->config.hpbar_objectStart + bot->hpbar_relobjectid, bot->pos.x + adata->config.hpbar_XOffset, bot->pos.y + adata->config.hpbar_YOffset, 0, 0);
			objs->Toggle(&tgt, adata->config.hpbar_objectStart + bot->hpbar_relobjectid, 1);
		}

		if (bot->buildbar_relobjectid >= 0)
		{
			objs->Move(&tgt, adata->config.buildbar_objectStart + bot->buildbar_relobjectid, bot->pos.x + adata->config.buildbar_XOffset, bot->pos.y + adata->config.buildbar_YOffset, 0, 0);
			objs->Toggle(&tgt, adata->config.buildbar_objectStart + bot->buildbar_relobjectid, 1);
		}
	}
	bot->lvzVisible = true;

}

static void BotDamage(Player *fake, Player *firedBy, int dist, int damagedealt, int wtype, int level, bool bouncing, int emptime, void *clos)
{
	struct BotData *bot;
	struct TurretType *turretType;
	struct ArenaData *adata;
	#ifdef USE_ADVDAMAGE
	Iadvdamage *advdamage;
	struct Weapons wpn;
	#endif

	bot = (struct BotData *) clos;
	turretType = bot->turretType;
	adata = GetArenaData(bot->arena);

	if (bot->killed || bot->config.freq == firedBy->p_freq)
	{
		return;
	}

	if (emptime) // do emp shutdown
	{
		if (TICK_GT(current_ticks() + emptime, bot->empshutdown_expiresat))
			bot->empshutdown_expiresat = TICK_MAKE(current_ticks() + emptime);
	}
	
	#ifdef USE_ADVDAMAGE
	
	advdamage = mm->GetInterface(I_ADVDAMAGE, bot->arena);
	if (advdamage)
	{
		wpn.type = wtype;
		wpn.level = level;
		wpn.shrapbouncing = 0;
		wpn.shraplevel = 0;
		wpn.shrap = 0;
		wpn.alternate = 0;
		damagedealt = advdamage->CalculateDamage(bot->p, firedBy, &wpn, damagedealt, true, true);
		mm->ReleaseInterface(advdamage);
	}
	#endif

	lm->LogP(L_DRIVEL, "staticturret", fake, "Took %d damage from %s", damagedealt, firedBy->name);
	bot->energy -= damagedealt;
	if (bot->energy <= 0)
	{
		bot->energy = 0;
		bot->pos.energy = bot->pos.extra.energy = 0;
		bot->lastRecharge = current_ticks();
		bot->killed = true;
		bot->p->flags.is_dead = 1;
		damage->KillFake(fake, firedBy);
	}
	else
	{
		bot->pos.energy = bot->pos.extra.energy = bot->energy;
	}

	if (bot->buildSequence) //no death obviously
	{
		bot->buildpoints -= damagedealt;
		if (bot->buildpoints < 0)
			bot->buildpoints = 0;
	}

}

static void UpdateBot(struct BotData *bot)
{
	struct TurretType *turretType;
	struct ArenaData *adata;
	ticks_t now;
	Arena *arena;
	int diff;

	turretType = bot->turretType;
	now = current_ticks();
	arena = bot->arena;
	adata = GetArenaData(arena);

	if (bot->markedForRemoval) return;

	if (bot->deathOn)
	{
		if (!bot->config.infiniteRespawn &&
		    ((bot->config.respawnCount && bot->spawns >= bot->config.respawnCount) || bot->buildSequence)&&
		    TICK_DIFF(now, bot->deathOn) >= 100
		    )
		{
			//Destroy the bot (leaves arena, etc)

			LLRemove(&bots, bot);
			LLAdd(&botsMarkedForRemoval, bot);
			bot->markedForRemoval = true;
		}
		else if (TICK_GTE(TICK_DIFF(now, bot->deathOn), bot->config.respawnDelay))
		{
			 //respawn the bot

			if (bot->config.freq >= STRUCTURES_FREQS || !adata->respawn_frozen[bot->config.freq]) // respawn frozen?
			{
				bot->spawns++;
				ReadBotConfig(bot, false);
				BotRespawn(bot->p, bot);
			}

		}
		else if (!bot->specced && TICK_DIFF(now, bot->deathOn) >= 100)
		{ //spec the bot, the bot just died a second ago
			bot->specced = true;
			game->SetShip(bot->p, SHIP_SPEC);
		}
	}
	else
	{
		//todo: track player feature, keeps firing at the player until he goes out of range
		//todo: wait for good shot

		if (bot->energy < bot->config.energy && !bot->buildSequence && TICK_GT(now, bot->empshutdown_expiresat))
		{
			diff = (bot->config.recharge * TICK_DIFF(now, bot->lastRecharge)) / 1000;
			if (diff)
			{
				bot->energy += diff;
				bot->lastRecharge = now;
			}
		}
		else
			bot->lastRecharge = now;

		if (bot->energy > bot->config.energy)
			bot->energy = bot->config.energy;

		bot->pos.energy = bot->pos.extra.energy = bot->energy;

		if (bot->buildSequence)
		{
			diff = (bot->config.buildSpeed * TICK_DIFF(now, bot->lastBuild)) / 1000;
			if (diff)
			{
				bot->buildpoints += diff;
				bot->lastBuild = now;

				if (bot->buildpoints > bot->config.energy)
				{
					bot->buildSequence = false;

					if (bot->buildbar_relobjectid >= 0)
					{
						objs->Reset(arena, adata->config.buildbar_objectStart + bot->buildbar_relobjectid);
						adata->buildbar_objectsUsed[bot->buildbar_relobjectid] = false;
						bot->buildbar_relobjectid = -1;
					}
				}
			}
		}

		if ((bot->config.freq > STRUCTURES_FREQS || !adata->powerset[bot->config.freq] || bot->config.requiredPower <= adata->power[bot->config.freq]) && // enough power?
		   !bot->buildSequence)
		{
			pd->Lock();
			bot->targetting = GetNearestPlayer(bot, true);
			pd->Unlock();

			if (bot->targetting)
			{
				bot->desiredRotation = FireControl(bot->p->position.x,
				                                   bot->p->position.y,
				                                   bot->targetting->position.x,
				                                   bot->targetting->position.y,
				                                   bot->targetting->position.xspeed - bot->p->position.xspeed,
				                                   bot->targetting->position.yspeed - bot->p->position.yspeed,
				                                   bot->config.weaponspeed);
			}
			RotateBot(bot);

			if (bot->targetting &&
			    TICK_DIFF(now, bot->lastFire) >= bot->config.weapon_delay &&
			    bot->pos.energy >= bot->config.weapon_fireEnergy)
			{
				FireWeapon(bot);
			}
		}

		if (TICK_DIFF(now, bot->lastPositionUpdate) >= POSITIONPACKETINTERVAL)
		{
			bot->lastPositionUpdate = now;

			bot->pos.rotation = bot->arotation / 1000;
			bot->pos.time = now;
			bot->pos.extra.energy = bot->pos.energy;
			bot->pos.weapon.type = W_NULL;
			bot->pos.bounty = bot->config.bounty;

			game->FakePosition(bot->p, &bot->pos, 22);
			//not needed: damage->FakePosition(bot->p, &bot->pos);
		}

		if (turretType->config.hpbar && TICK_DIFF(now, bot->hpbar_lastUpdate) >= HPBAR_UPDATEINTERVAL)
		{
			UpdateHPBar(bot);
		}

		if (bot->buildSequence && TICK_DIFF(now, bot->buildbar_lastUpdate) >= HPBAR_UPDATEINTERVAL)
		{
			UpdateBuildBar(bot);
		}
	}
}

static void FireWeapon(struct BotData *bot)
{
	struct TurretType *turretType;
	int newEnergy;

	turretType = bot->turretType;

	newEnergy = bot->pos.energy -  bot->config.weapon_fireEnergy;
	if (newEnergy < 0) newEnergy = 0;
	bot->pos.energy = newEnergy;
	bot->pos.extra.energy = newEnergy;
	bot->pos.rotation = bot->arotation / 1000;
	bot->pos.time = current_ticks();
	bot->pos.bounty = bot->config.bounty;

	bot->pos.weapon.type = bot->config.weapon_type;
	bot->pos.weapon.level = bot->config.weapon_level;
	bot->pos.weapon.shraplevel = bot->config.weapon_shrapnelLevel;
	bot->pos.weapon.shrap = bot->config.weapon_shrapnelCount;
	bot->pos.weapon.shrapbouncing = bot->config.weapon_shrapnelBouncing ? 1 : 0;
	bot->pos.weapon.alternate = bot->config.weapon_multifire ? 1 : 0;

	bot->lastFire = current_ticks();
	bot->lastPositionUpdate = current_ticks();

	game->FakePosition(bot->p, &bot->pos, 22);

}

static void RotateBot(struct BotData *bot)
{
	struct TurretType *turretType;
	int desiredRotation;

	int rotationSpeed;

	desiredRotation = bot->desiredRotation * 1000;

	if (bot->arotation != desiredRotation)
	{
		turretType = bot->turretType;
		rotationSpeed = bot->config.rotationSpeed;

		if (rotationSpeed < 0) //-1 = instant rotation
		{
			bot->arotation = desiredRotation;
			return;
		}

		if ((bot->desiredRotation - bot->pos.rotation + 40) % 40 < 20) //rotate right
		{
			bot->arotation = (bot->arotation + rotationSpeed) % 40000;
		}
		else // rotate left
		{
			bot->arotation = (bot->arotation + 40000 - rotationSpeed) % 40000;
		}
	}
}

static void UpdateHPBar(struct BotData *bot)
{
	int imageoffset;
	struct ArenaData *adata;
	Target tgt;
	tgt.type = T_ARENA;
	tgt.u.arena = bot->arena;

	adata = GetArenaData(bot->arena);
	if (bot->config.energy)
		imageoffset = bot->energy * (adata->config.hpbar_imagesAllocated - 1) / bot->config.energy;
	else
		imageoffset = adata->config.hpbar_imagesAllocated - 1;

	if (imageoffset != bot->hpbar_lastOffset)
	{
		objs->Image(&tgt, bot->hpbar_relobjectid + adata->config.hpbar_objectStart, adata->config.hpbar_imageStart + imageoffset);

		bot->hpbar_lastUpdate = current_ticks();
		bot->hpbar_lastOffset = imageoffset;
	}
}

static void UpdateBuildBar(struct BotData *bot)
{
	int imageoffset;
	struct ArenaData *adata;
	Target tgt;
	tgt.type = T_ARENA;
	tgt.u.arena = bot->arena;
	int buildpoints;

	adata = GetArenaData(bot->arena);

	if (bot->buildpoints > bot->config.energy) // the points can not be above the max in the blow formula
		buildpoints = bot->config.energy;
	else
		buildpoints = bot->buildpoints;

	imageoffset = bot->buildpoints * adata->config.buildbar_imagesAllocated / (bot->config.energy+1);

	if (imageoffset != bot->buildbar_lastOffset)
	{
		objs->Image(&tgt, bot->buildbar_relobjectid + adata->config.buildbar_objectStart, adata->config.buildbar_imageStart + imageoffset);

		bot->buildbar_lastUpdate = current_ticks();
		bot->buildbar_lastOffset = imageoffset;
	}
}

static inline unsigned char FireAngle(double x, double y)
{
	/* [-Pi, Pi] + Pi -> [0, 2Pi] */
	double angle = atan2(y, x) + PI;

	int a = round(angle * 40.0 / (2.0 * PI) + 30);
	/* 0 degrees is +y-axis for us, 0 degrees is +x-axis for atan2 */
	return (unsigned char) a % 40;
}

static inline unsigned char FireControl (double srcx, double srcy, double dstx, double dsty, double dxspeed, double dyspeed, double projspeed)
{
	double dx, dy, err;
	double bestdx, bestdy, besterr = 20000;
	double tt = 10, pt;

	bestdx = dstx - srcx;
	bestdy = dsty - srcy;

	do
	{
		dx = (dstx + dxspeed * tt / 1000) - srcx;
		dy = (dsty + dyspeed * tt / 1000) - srcy;
		pt = LHypot(dx, dy) * 1000 / projspeed;

		err = abs(pt - tt);
		if (err < besterr)
		{
			besterr = err;
			bestdx = dx;
			bestdy = dy;
		}
		else if (err > besterr)
			break;

		tt += 10;
	} while(pt > tt && tt <= 250);
	// get angle
	return FireAngle(bestdx, bestdy);
}

static Player *GetNearestPlayer(struct BotData *bot, bool enemyOnly)
{
	Link *link;
	Player *i, *bestp = NULL;
	Arena *arena = bot->p->arena;
	struct ArenaData *adata = GetArenaData(arena);
	struct PlayerData *pdata;
	int bestdist = INT_MAX, dist, x, y, x1, y1;
	int myfreq = bot->p->p_freq;

	x = bot->pos.x;
	y = bot->pos.y;

	FOR_EACH_PLAYER(i)
	{
		pdata = GetPlayerData(i);
		if (i->arena == arena &&
			i->p_ship != SHIP_SPEC &&
			(i->p_freq != myfreq || !enemyOnly) &&
			!i->flags.is_dead &&
			!(i->position.status & STATUS_SAFEZONE) &&
			!(!bot->config.xradar && !(i->position.status & STATUS_UFO) && i->position.status & STATUS_CLOAK && i->position.status & STATUS_STEALTH) // do not shoot at cloakers if we dont have xradar; but still shoot at players with UFO since these are most likely other turrets
			)
		{
			x1 = i->position.x + (i->position.xspeed * BOT_PROJECT_FAVOUR) / 1000;
			y1 = i->position.y + (i->position.yspeed * BOT_PROJECT_FAVOUR) / 1000;

			dist = LHypot(x - x1, y - y1);

			if (dist > bot->config.weapon_sightPixels)
				continue;

			if (!IS_HUMAN(i))
			    dist += BOT_HUMAN_FAVOUR;

			if (!(i->position.status & STATUS_UFO))
			{
				if (i->position.status & STATUS_CLOAK)
					dist += CLOAK_FAVOUR;

				if (i->position.status & STATUS_STEALTH)
					dist += STEALTH_FAVOUR;
			}

			if (i->p_ship != adata->config.shipFavour)
				dist += SPECIFIC_SHIP_FAVOUR;

			if (dist < bestdist &&
				is_pathclear(arena, x >> 4, y >> 4, x1 >> 4, y1 >> 4, (bot->config.weapon_type == W_THOR)) )
			{
				bestp = i;
				bestdist = dist;
			}
		}
	}

	return bestp;
}

static bool fitsOnMap(Arena *arena, int x_, int y_, int radius)
{
	int startTileX, endTileX;
	int startTileY, endTileY;
	enum map_tile_t tile;
	int x, y;

	startTileX = (x_ - radius) >> 4;
	endTileX = (x_ + radius) >> 4;

	startTileY = (y_ - radius) >> 4;
	endTileY = (y_ + radius) >> 4;

	for (x = startTileX; x <= endTileX; x++)
	{
		for (y = startTileY; y <= endTileY; y++)
		{
			tile = mapdata->GetTile(arena, x, y);

			if ((tile >= 1 && tile <= 169) || // normal solid tiles, doors
			    (tile >= 191 && tile <= 252) || // special solid tiles or anything that warps ship or removes weapons
			    tile == 171 // safe zone
			    )
				return false;
		}
	}



	return true;
}


/* http://www.gamedev.net/reference/articles/article767.asp */
static bool is_pathclear(Arena *arena, int x1, int y1, int x2, int y2, bool isthor)
{
	int i, dx, dy, numpixels;
	int d, dinc1, dinc2;
	int x, xinc1, xinc2;
	int y, yinc1, yinc2;

	/* calculate deltax and deltay for initialisation */
	dx = x2 - x1;
	dy = y2 - y1;
	if (dx < 0) dx = -dx;
	if (dy < 0) dy = -dy;

	/* initialize all vars based on which is the independent variable */
	if (dx > dy)
	{
		/* x is independent variable */
		numpixels = dx + 1;
		d = (2 * dy) - dx;
		dinc1 = dy << 1;
		dinc2 = (dy - dx) << 1;
		xinc1 = 1;
		xinc2 = 1;
		yinc1 = 0;
		yinc2 = 1;
	}
	else
	{
		/* y is independent variable */
		numpixels = dy + 1;
		d = (2 * dx) - dy;
		dinc1 = dx << 1;
		dinc2 = (dx - dy) << 1;
		xinc1 = 0;
		xinc2 = 1;
		yinc1 = 1;
		yinc2 = 1;
	}

	/* make sure x and y move in the right directions */
	if (x1 > x2)
	{
		xinc1 = - xinc1;
		xinc2 = - xinc2;
	}
	if (y1 > y2)
	{
		yinc1 = - yinc1;
		yinc2 = - yinc2;
	}

	/* start drawing at */
	x = x1;
	y = y1;

	/* trace the line */
	for(i = 1; i < numpixels; i++)
	{
		/* map stuff */
		enum map_tile_t tile = mapdata->GetTile(arena, x, y);

	        if (isthor)
		{
			if (tile == 242 || tile == 220)
				return false;
	        }
	        else
	        {
	        	if ((tile >= 1 && tile <= 161) ||
	                    (tile >= 191 && tile <= 251))
	        		return false;
	        }

		/* bresenham stuff */
		if (d < 0)
		{
			d = d + dinc1;
			x = x + xinc1;
			y = y + yinc1;
		}
		else
		{
			d = d + dinc2;
			x = x + xinc2;
			y = y + yinc2;
		}
	}

	return true;
}

static void CancelPlayerTarget(Player *p)
{
	Link *l;
	struct BotData *bot;


	for (l = LLGetHead(&bots); l; l = l->next)
	{
		bot = (struct BotData *) l->data;
		if (bot->targetting == p)
			bot->targetting = NULL;
	}

}

static void Kill(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
	CancelPlayerTarget(killed);

}

static void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	CancelPlayerTarget(p);
}

static void PlayerAction(Player *p, int action, Arena *arena)
{
	Link *link;
	Player *i;
	int count;

	if (action == PA_ENTERGAME)
	{

	}
	else if (action == PA_LEAVEARENA)
	{

		CancelPlayerTarget(p);

		count = 0;

		/* kill all bots? */
		pd->Lock();
		FOR_EACH_PLAYER(i)
		{
			if (i->arena == arena &&
				i != p && IS_HUMAN(i))
				count++;
		}
		pd->Unlock();

		if (count == 0)
			StopGame(arena);

	}
}

static void MainLoop()
{
	Link *l;
	Link *next;

	// Bots are never removed immediately, this is to avoid PID problems

	for (l = LLGetHead(&botsMarkedForRemoval); l; l = next)
	{
		next = l->next;
		RemoveBot(l->data);
	}

	for (l = LLGetHead(&bots); l; l = next)
	{
		next = l->next;
		UpdateBot(l->data);
	}

}







static helptext_t Cspawnturret_help =
	"Targets: none\n"
	"Args: ['-f'] <turret type>\n"
	"Places a staticturret of the specified turret type. Type's are based on arena configuration\n"
	"if -f is given, the bot may be placed anywhere (with the exception of solid tiles)";

static void Cspawnturret(const char *command, const char *params, Player *p, const Target *target)
{
	enum ADDBOT_ERROR error;
	bool noLocationCheck;

	if (!p->arena || !params) return;

	noLocationCheck = false;

	if (strstr(params, "-f "))
	{
		noLocationCheck = true;
		params += 3;
	}

	error = AddBotByKey(p->arena, params,  p->position.x, p->position.y, p->p_freq, false, noLocationCheck);

	switch (error)
	{
		case ADDBOT_OK:
			chat->SendCmdMessage(p, "Turret %s spawned at %d, %d on freq %d", params, p->position.x, p->position.y, p->p_freq);
		break;
		case ADDBOT_ILLEGAL_ARENA:
			chat->SendCmdMessage(p, "You can not spawn any turrets in this arena (staticturret module must be attached first)");
		break;
		case ADDBOT_UNKNOWN_TYPE:
			chat->SendCmdMessage(p, "Unknown turret type: %s", params);
		break;
		case ADDBOT_MAX_REACHED_FOR_BOTTYPE:
			chat->SendCmdMessage(p, "There are no available bots for that turret type");
		break;
		case ADDBOT_MAX_REACHED_FOR_ARENA:
			chat->SendCmdMessage(p, "There are no available bots for this arena");
		break;
		case ADDBOT_CAN_NOT_BE_PLACED_ON_MAP:
			chat->SendCmdMessage(p, "Turret can not be placed there (an illegal map tile is nearby)");
		break;
		case ADDBOT_TO_CLOSE_TO_OTHER_BOT:
			chat->SendCmdMessage(p, "Turret is to close to another turret");
		break;
		case ADDBOT_BUILDING_IN_PROGRESS:
			chat->SendCmdMessage(p, "Unable to comply, building in progress");
		break;
		default:
			chat->SendCmdMessage(p, "Unknown error");
	}

}

static void Cresetturrets(const char *tc, const char *params, Player *p, const Target *target)
{
	if (!p->arena) return;

	StopGame(p->arena);

}

static void Clogbanner(const char *tc, const char *params, Player *p, const Target *target)
{
	if (target->type != T_PLAYER)
	{
		chat->SendCmdMessage(p, "Please send to a player");
		return;
	}
	struct PlayerData *pdata = GetPlayerData(target->u.p);
	pdata->logBanner = !pdata->logBanner;

	if (pdata->logBanner)
		chat->SendCmdMessage(p, "Logging banner...");
	else
		chat->SendCmdMessage(p, "No longer logging banner");
}

static void SetBannerCB(Player *p, Banner *banner)
{
	struct PlayerData *pdata = GetPlayerData(p);

	if (pdata->logBanner)
	{
		assertlm(sizeof(Banner) == 96 * sizeof(u8));
		char buf[96*2+1]; //96*2+1
		buf[192] = 0;
		int a;

		for (a = 0; a < 96; a++)
		{
			assertlm(banner->data[a] <= 0xFF);

			if (banner->data[a] <= 0xF)
			{
				buf[a*2] = '0';
				itoa(banner->data[a], &buf[a*2+1], 16);
			}
			else
			{
				itoa(banner->data[a], &buf[a*2], 16);
			}
		}

		chat->SendArenaMessage(p->arena, "Banner of %s=%s", p->name, buf);
	}
}

static void FreezeRespawn(Arena *arena, int freq, bool freeze)
{
	assertlm(arena);

	struct ArenaData *adata = GetArenaData(arena);
	if (!adata->running)
	{

		return;
	}
	ticks_t now = current_ticks();
	Link *l;
	assertlm(freq >= 0);
	assertlm(freq < STRUCTURES_FREQS);

	if (freeze && !adata->respawn_frozen[freq])
	{
		adata->respawn_frozen_time[freq] = now;
		adata->respawn_unfrozen_time[freq] = 0;
		adata->respawn_frozen[freq] = 1;
	}
	else if (!freeze && adata->respawn_frozen[freq])
	{
		adata->respawn_unfrozen_time[freq] = now;

		if (adata->respawn_frozen_time[freq])
		{

			for (l = LLGetHead(&bots); l; l = l->next)
			{
				struct BotData *bot = l->data;

				if (bot->arena == arena && bot->config.freq == freq)
					bot->deathOn += TICK_MAKE(adata->respawn_unfrozen_time[freq] - adata->respawn_frozen_time[freq]);
			}

		}
		adata->respawn_frozen[freq] = 0;
	}

}

static void SetPower(Arena *arena, int freq, int power)
{
	assertlm(arena);
	assertlm(freq >= 0);
	assertlm(freq < STRUCTURES_FREQS);


	struct ArenaData *adata = GetArenaData(arena);
	if (!adata->running)
	{

		return;
	}

	adata->powerset[freq] = true;
	adata->power[freq] = power;

}

static void DoDamage(Arena *arena, Player *killer, int x, int y, int maxdamage, int radius, int immuneFreq)
{
	assertlm(arena);
        Link *l;

	if (!radius)
		return;

	for (l = LLGetHead(&bots); l; l = l->next)
	{
		struct BotData *bot = l->data;
		if (bot->killed || bot->arena != arena || bot->config.freq == immuneFreq)
			continue;
		int dist = LHypot(x - bot->pos.x, y - bot->pos.y);
		if (dist < radius)
		{
			int damagedealt = dist * - maxdamage / radius + maxdamage;
			bot->energy -= damagedealt;
			if (bot->energy <= 0)
			{
				bot->energy = 0;
				bot->pos.energy = bot->pos.extra.energy = 0;
				bot->lastRecharge = current_ticks();
				bot->killed = true;
				bot->p->flags.is_dead = 1;

				damage->KillFake(bot->p, killer);
			}
			else
			{
				bot->pos.energy = bot->pos.extra.energy = bot->energy;
			}
		}
	}

}

static Istaticturret staticturret =
{
	INTERFACE_HEAD_INIT(I_STATICTURRET, "staticturret")
	StartGameInt, StopGameInt,
	AddBotByKeyInt,
	FreezeRespawn,
	SetPower,
	DoDamage
};

EXPORT const char info_staticturret[] = "StaticTurret 2 (" ASSSVERSION ", " BUILDDATE ") by JoWie (Rewrite from original by Smong)\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(aman   );
        mm->ReleaseInterface(pd     );
        mm->ReleaseInterface(lm     );
        mm->ReleaseInterface(attrman);
        mm->ReleaseInterface(objs   );
        mm->ReleaseInterface(fake   );
        mm->ReleaseInterface(cmd    );
        mm->ReleaseInterface(chat   );
        mm->ReleaseInterface(damage );
        mm->ReleaseInterface(cfg    );
        mm->ReleaseInterface(game   );
        mm->ReleaseInterface(mapdata);
        mm->ReleaseInterface(banner );
}

EXPORT int MM_staticturret(int action, Imodman *mm_, Arena *arena)
{
	struct ArenaData *adata;
	Link *l, *next;

        if (action == MM_LOAD)
        {
                mm      = mm_;
                aman    = mm->GetInterface(I_ARENAMAN        , ALLARENAS);
                pd      = mm->GetInterface(I_PLAYERDATA      , ALLARENAS);
                lm      = mm->GetInterface(I_LOGMAN          , ALLARENAS);
                attrman = mm->GetInterface(I_ATTRMAN         , ALLARENAS);
                objs	= mm->GetInterface(I_OBJECTS         , ALLARENAS);
                fake	= mm->GetInterface(I_FAKE            , ALLARENAS);
                cmd 	= mm->GetInterface(I_CMDMAN          , ALLARENAS);
                chat	= mm->GetInterface(I_CHAT            , ALLARENAS);
                damage  = mm->GetInterface(I_DAMAGE          , ALLARENAS);
                game    = mm->GetInterface(I_GAME            , ALLARENAS);
                cfg     = mm->GetInterface(I_CONFIG          , ALLARENAS);
                mapdata = mm->GetInterface(I_MAPDATA         , ALLARENAS);
                banner  = mm->GetInterface(I_BANNERS          , ALLARENAS);

                if (!aman || !pd || !lm || !attrman || !objs || !fake || !cmd || !chat || !damage || !game || !cfg || !mapdata || !banner)
                {
                        ReleaseInterfaces();
                        printf("<staticturret> Missing interfaces");
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct ArenaData));
                playerKey = pd->AllocatePlayerData(sizeof(struct PlayerData));

                if (arenaKey == -1 || playerKey == -1) // check if we ran out of memory
                {
                        if (arenaKey  != -1) // free data if it was allocated
                                aman->FreeArenaData(arenaKey);

                        if (playerKey != -1) // free data if it was allocated
                                pd->FreePlayerData (playerKey);

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

		mm->RegCallback(CB_MAINLOOP, MainLoop, ALLARENAS);
		mm->RegCallback(CB_SET_BANNER, SetBannerCB, ALLARENAS);
		mm->RegCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);
		mm->RegCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);
		cmd->AddCommand("logbanner", Clogbanner, ALLARENAS, NULL);
		
                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
        	MainLoop(); // make sure all the bots removed by MM_DETACH are actually removed
        	mm->UnregCallback(CB_MAINLOOP, MainLoop, ALLARENAS);
        	mm->UnregCallback(CB_SET_BANNER, SetBannerCB, ALLARENAS);
        	mm->UnregCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);
        	mm->UnregCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);

                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);


		for (l = LLGetHead(&bots); l; l = next)
		{
			next = l->next;
			RemoveBot(l->data);
		}

		for (l = LLGetHead(&botsMarkedForRemoval); l; l = next)
		{
			next = l->next;
			RemoveBot(l->data);
		}
		cmd->RemoveCommand("logbanner", Clogbanner, ALLARENAS);


                ReleaseInterfaces();

                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {

		mm->RegInterface(&staticturret, arena);

		mm->RegCallback(CB_KILL, Kill, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChange, arena);
		mm->RegCallback(CB_PLAYERACTION, PlayerAction, arena);

		cmd->AddCommand("spawnturret", Cspawnturret, arena, Cspawnturret_help);
		cmd->AddCommand("resetturrets", Cresetturrets, arena, NULL);
		


		ReadSettings(arena);
		adata = GetArenaData(arena);
		if (!adata->config.hosted)
			StartGame(arena);


                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
		if (mm->UnregInterface(&staticturret, arena))
			return MM_FAIL;


		StopGame(arena);

		cmd->RemoveCommand("spawnturret", Cspawnturret, arena);
		cmd->RemoveCommand("resetturrets", Cresetturrets, arena);


		mm->UnregCallback(CB_KILL, Kill, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChange, arena);
		mm->UnregCallback(CB_PLAYERACTION, PlayerAction, arena);

		CleanupArenaSettings(arena);


                return MM_OK;
        }

        return MM_FAIL;
}
