/* $Id: staticturret.h 361 2011-06-12 19:24:26Z joris $ */
#ifndef __STATICTURRET_H
#define __STATICTURRET_H

#include <stdbool.h>

/** the interface id for staticturret */
#define I_STATICTURRET "staticturret-2"


enum ADDBOT_ERROR
{
	ADDBOT_OK = 0,
	ADDBOT_ILLEGAL_ARENA, // module has not been attached in this arena
	ADDBOT_UNKNOWN_TYPE,
	ADDBOT_MAX_REACHED_FOR_BOTTYPE,
	ADDBOT_MAX_REACHED_FOR_ARENA,
	ADDBOT_CAN_NOT_BE_PLACED_ON_MAP, // bot is being placed on a tiled section of the map
	ADDBOT_TO_CLOSE_TO_OTHER_BOT, // only if noLocationCheck = false
	ADDBOT_BUILDING_IN_PROGRESS, // build sequence is enabled for the bot and to many other bots are currently being constructed
};

/** the staticturret interface struct */
typedef struct Istaticturret
{
	INTERFACE_HEAD_DECL

	void (*StartGame)(Arena *arena);
	void (*StopGame)(Arena *arena);

	// x and y are in pixels. (to convert tiles to pixels, use: (tilex << 4) + 8
	enum ADDBOT_ERROR (*AddBot)(Arena *arena, const char *key, int x, int y, int freq, bool infiniteRespawn, bool noLocationCheck);


	void (*FreezeRespawn)(Arena *arena, int freq, bool freeze);

	//Set the amount of power that a freq has. Note: this has to be called atleast once to work
	void (*SetPower)(Arena *arena, int freq, int power);


	void (*DoDamage)(Arena *arena, Player *killer, int x, int y, int damage, int radius, int immuneFreq);

} Istaticturret;

#endif

