/* $Id: warpdrive.c 25 2010-09-12 19:14:16Z jowie $ */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "asss.h"
#include "warpdrive.h"
#include "fake.h"
#include "packets/flags.h"

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

/*
	WarpDrive by JoWie

	In this module "warp" has 2 meanings:
	1) Normal subspace warp
	2) High speed you receive when you get a flag

	Good luck
*/

static Imodman		*mm;
static Imainloop	*ml;
static Iarenaman 	*aman;
static Iplayerdata	*pd;
static Inet		*net;
static Icmdman		*cmd;
static Ilogman		*lm;
static Iconfig		*cfg;
static Imapdata		*mapdata;
static Iflagcore	*flagcore;
static Ifake		*fake;

static int arenaKey  = -1;
static int playerKey = -1;


struct ArenaData
{
	bool enabled;
	Player *fakePlayer;
	int humancount;

	struct
	{
		int sendPositionDelay; // misc:sendpositiondelay
		bool useFakeRepel;
		bool allowWeapons;
		bool sendAntiWarpPacket;
	} config;
	struct
	{
		int warpSpeed;
		int warpDelay;
	} shipconfig[8];
};

static pthread_mutex_t playerdatamtx = PTHREAD_MUTEX_INITIALIZER;
#define PlayerData PlayerData_
struct PlayerData
{
	bool inWarp; //aka the player has a flag
	ticks_t lastWarpChange; // last time the player entered or dropped out of warp
	ticks_t speededAt; // When did the player met the speed requirement?
	bool firedWeapon;
	ticks_t sentFakeRepelAt;
	ticks_t fakePlayer_lastPos; // last position packet when config.sendAntiWarpPacket
	
	int warpSpeedOverride;
	int warpDelayOverride;
};



static void ReadConfig(Arena *arena);

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<warpdrive> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct ArenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey >= 0);
        struct ArenaData *adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);
        return adata;
}

static struct PlayerData* GetPlayerData(Player *p)
{
        assertlm(p);
        assertlm(playerKey >= 0);
        struct PlayerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);
        return pdata;
}

static inline void LockPD()
{
        pthread_mutex_lock(&playerdatamtx);
}
static inline void UnlockPD()
{
        pthread_mutex_unlock(&playerdatamtx);
}


static void ArenaActionCB(Arena *arena, int action)
{
	if (action == AA_CONFCHANGED)
	{
		ReadConfig(arena);
	}
}

static long LHypot(register long dx, register long dy)
{
        register unsigned long r, dd;

        dd = dx*dx+dy*dy;

        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;

        /* initial hypotenuse guess (from Gems) */
        r = (dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));

        if (r == 0) return (long)r;

        /* converge 3 times */
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;

        return (long)r;
}

static void DoWeaponChecksum(struct S2CWeapons *pkt)
{
	int i;
	u8 ck = 0;
	pkt->checksum = 0;
	for (i = 0; i < sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData); i++)
		ck ^= ((unsigned char*)pkt)[i];
	pkt->checksum = ck;
}


static bool DoWarp(Player *p, bool warp) // call with lock
{
	struct PlayerData *pdata;
	struct ArenaData *adata;
	struct S2CFlagPickup flagPickupPacket;
	struct S2CFlagDrop flagDropPacket;
	struct S2CWeapons wpn;
	int speed;
	ticks_t now;

	now = current_ticks();
	pdata = GetPlayerData(p);
	adata = GetArenaData(p->arena);
	if (!warp)
		pdata->speededAt = 0;

	if (pdata->inWarp == warp)
		return warp;

	pdata->inWarp = warp;
	pdata->lastWarpChange = current_ticks();

	if (warp)
	{
		flagPickupPacket.type = S2C_FLAGPICKUP;
		flagPickupPacket.fid = 0;
		flagPickupPacket.pid = p->pid;
		net->SendToOne(p, (byte*)&flagPickupPacket, sizeof(flagPickupPacket), NET_RELIABLE);

		if (adata->fakePlayer)
		{
			#define REPEL_DIST (1)
			speed = LHypot(p->position.xspeed,  p->position.yspeed);

			wpn.type = S2C_WEAPON;
			wpn.rotation = 0;
			wpn.time = now & 0xFFFF;
			wpn.x = p->position.x - (p->position.xspeed / speed) * REPEL_DIST;
			wpn.y = p->position.y - (p->position.yspeed / speed) * REPEL_DIST;
			wpn.xspeed = -p->position.xspeed;
			wpn.yspeed = -p->position.yspeed;
			wpn.playerid = adata->fakePlayer->pid;
			wpn.checksum = 0;
			wpn.status = STATUS_STEALTH | STATUS_CLOAK | STATUS_UFO;
			if (adata->config.sendAntiWarpPacket)
				wpn.status |= STATUS_ANTIWARP;
			wpn.c2slatency = 0;
			wpn.bounty = 0;
			wpn.weapon = (struct Weapons) {W_REPEL,0,0,0,0,0};
			wpn.extra = (struct ExtraPosData) {0,0,0,0,0,0,0,0,0,0,0,0,0};

			DoWeaponChecksum(&wpn);
			net->SendToOne(p, (byte*)&wpn, sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData), NET_RELIABLE);
			pdata->sentFakeRepelAt = now;
		}
		DO_CBS(CB_WARPDRIVE_WARP, p->arena, WarpDriveWarpFunc, (p, true));
		return true;
	}
	else
	{
		flagDropPacket.type = S2C_FLAGDROP;
		flagDropPacket.pid = p->pid;
		net->SendToOne(p, (byte*)&flagDropPacket, sizeof(flagDropPacket), NET_RELIABLE);
		DO_CBS(CB_WARPDRIVE_WARP, p->arena, WarpDriveWarpFunc, (p, false));
		return false;
	}
}

static bool DoWarpInt(Player *p, bool warp)
{
	bool ret;
	struct ArenaData *adata;
	adata = GetArenaData(p->arena);
	
	if (!adata->enabled)
		return false;
	
	if (!p->flags.sent_ppk || p->status != S_PLAYING || !IS_STANDARD(p))
		return false;
	
	LockPD();
	ret = DoWarp(p, warp);
	UnlockPD();
	return ret;
}

static bool IsWarpedInt(Player *p, bool warp)
{
	bool ret;
	struct ArenaData *adata;
	struct PlayerData *pdata;
	adata = GetArenaData(p->arena);
	pdata = GetPlayerData(p);
	
	
	if (!adata->enabled)
		return false;
	
	LockPD();
	ret = pdata->inWarp;
	UnlockPD();
	return ret;
}

static void SetWarpSpeedInt(Player *p, int speed)
{
	struct ArenaData *adata;
	struct PlayerData *pdata;
	adata = GetArenaData(p->arena);
	pdata = GetPlayerData(p);
	
	
	if (!adata->enabled)
		return;
	
	LockPD();
	pdata->warpSpeedOverride = speed;
	UnlockPD();
}

static void SetWarpDelayInt(Player *p, int delay)
{
	struct ArenaData *adata;
	struct PlayerData *pdata;
	adata = GetArenaData(p->arena);
	pdata = GetPlayerData(p);
	
	
	if (!adata->enabled)
		return;
	
	LockPD();
	pdata->warpDelayOverride = delay;
	UnlockPD();
}



static void ReadConfig(Arena *arena)
{
	ConfigHandle ch;
	struct ArenaData *adata;
	ch = arena->cfg;
	int a;

	adata = GetArenaData(arena);


	adata->config.sendPositionDelay = cfg->GetInt(ch, "Misc", "SendPositionDelay", 20);

	/* cfghelp: WarpDrive:AllowWeapons, arena, int, def: 0, range:0-1
         * When set, a fake player fires a repel behind the player when he enters warp */
	adata->config.allowWeapons = !!cfg->GetInt(ch, "WarpDrive", "AllowWeapons", 1);

	/* cfghelp: WarpDrive:UseFakePlayer, arena, int, def: 0, range:0-1
         * When set, a fake player fires a repel behind the player when he enters warp */
	adata->config.useFakeRepel = !!cfg->GetInt(ch, "WarpDrive", "UseFakePlayer", 0);

	/* cfghelp: WarpDrive:SendAntiWarpPacket, arena, int, def: 0, range:0-1
         * When set, a fake player constantly sends a position packet with antiwarp from the position -1 -1 */
	adata->config.sendAntiWarpPacket = !!cfg->GetInt(ch, "WarpDrive", "SendAntiWarpPacket", 0);

	for (a = 0; a < 8; a++)
	{
		/* cfghelp: All:WarpSpeed, arena, int, def: 0
		 * Speed required to enter warp speed (pixels/second) */
		adata->shipconfig[a].warpSpeed = cfg->GetInt(ch, cfg->SHIP_NAMES[a], "WarpSpeed", 0);

		/* cfghelp: All:WarpDelay, arena, int, def: 0
		 * How long the player has to maintain the WarpSpeed (ticks) */
		adata->shipconfig[a].warpDelay = cfg->GetInt(ch, cfg->SHIP_NAMES[a], "WarpDelay", 0);
	}

	if (adata->config.useFakeRepel || adata->config.sendAntiWarpPacket)
	{
		if (!adata->fakePlayer && adata->enabled && adata->humancount)
		{
			adata->fakePlayer = fake->CreateFakePlayer("", arena, SHIP_SHARK, 9999);
			lm->LogA(L_DRIVEL, "warpdrive", arena, "Recreating fake player after a config update");
		}
	}
	else
	{
		if (adata->fakePlayer)
		{
			fake->EndFaked(adata->fakePlayer);
			adata->fakePlayer = NULL;
			lm->LogA(L_DRIVEL, "warpdrive", arena, "Removing fake player after a config update");
		}
	}
}


static void NewPlayerCB(Player *p, int isnew)
{
	Arena *arena;
	Link *link;
	struct ArenaData *adata;

	if (p->type != T_FAKE || isnew)
		return;

	aman->Lock();
	FOR_EACH_ARENA(arena)
	{
		adata = GetArenaData(arena);
		if (adata->fakePlayer == p)
			adata->fakePlayer = NULL;
	}
	aman->Unlock();
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	struct PlayerData* pdata;
	struct ArenaData *adata;

	if (action == PA_ENTERARENA)
	{
		if (IS_HUMAN(p))
		{
			adata = GetArenaData(arena);
			adata->humancount++;
			if ((adata->config.useFakeRepel || adata->config.sendAntiWarpPacket) && !adata->fakePlayer && adata->enabled)
			{
				adata->fakePlayer = fake->CreateFakePlayer("", arena, SHIP_SHARK, 9999);
				lm->LogA(L_DRIVEL, "warpdrive", arena, "Creating fake player because a human player entered");
			}
		}
	}
	else if (action == PA_ENTERGAME) // download is complete
	{
		adata = GetArenaData(arena);
		pdata = GetPlayerData(p);

		LockPD();
		pdata->warpSpeedOverride = -1;
		pdata->warpDelayOverride = -1;
		DoWarp(p, false);
		UnlockPD();
	}
	else if (action == PA_LEAVEARENA)
	{
		pdata = GetPlayerData(p);
		adata = GetArenaData(arena);
		LockPD();
		pdata->warpSpeedOverride = -1;
		pdata->warpDelayOverride = -1;
		DoWarp(p, false);
		UnlockPD();

		if (IS_HUMAN(p))
		{
			adata->humancount--;
			if (adata->humancount <= 0)
			{
				if (adata->fakePlayer)
				{
					fake->EndFaked(adata->fakePlayer);
					adata->fakePlayer = NULL;
				}
				lm->LogA(L_DRIVEL, "warpdrive", arena, "Removing fake player because the last human player left");
			}
		}
	}
}

static void FlagGameInit(Arena *arena)
{
	flagcore->SetCarryMode(arena, CARRY_ONE);
	flagcore->ReserveFlags(arena, 1);
}

static void FlagGameFlagTouch(Arena *arena, Player *p, int fid)
{
	// no need to do anything here
}

static void FlagGameCleanup(Arena *arena, int fid, int reason, Player *oldcarrier, int oldfreq)
{
	// no need to do anything here
}

static int UpdateWarpTimer(Arena *arena)
{
	struct ArenaData *adata;
	struct PlayerData *pdata;
	struct S2CPosition pos;
	bool firedWeapon;
	ticks_t now;
	Link *link;
	struct S2CWeapons wpn;
	Player *p;

	int speed, requiredSpeed, warpDelay;

	if (arena->status  != ARENA_RUNNING)
		return 1;

	adata = GetArenaData(arena);
	if (!adata->enabled)
		return 0;

	now = current_ticks();
	pd->Lock();
	LockPD();
	FOR_EACH_PLAYER(p)
	{
		if (p->arena != arena || !p->flags.sent_ppk || p->status != S_PLAYING || !IS_STANDARD(p))
			continue;

		pdata = GetPlayerData(p);
		firedWeapon = !adata->config.allowWeapons && pdata->firedWeapon;
		pdata->firedWeapon = false;
		if (p->p_ship == SHIP_SPEC)
		{
			DoWarp(p, false);
		}
		else
		{
			speed = LHypot(p->position.xspeed,  p->position.yspeed);
			
			if (pdata->warpSpeedOverride >= 0)
				requiredSpeed = pdata->warpSpeedOverride;
			else
				requiredSpeed = adata->shipconfig[p->p_ship].warpSpeed;
			
			if (requiredSpeed <= 0)
			{
				DoWarp(p, false);
			}
			else
			{
				if (!pdata->lastWarpChange || TICK_DIFF(now, pdata->lastWarpChange) >= 100)
				{
					if (speed > requiredSpeed)
					{
						if (!pdata->speededAt)
							pdata->speededAt = now;
						
						if (pdata->warpDelayOverride >= 0)
							warpDelay = pdata->warpDelayOverride;
						else
							warpDelay = adata->shipconfig[p->p_ship].warpDelay;
						 
						if (firedWeapon)
							DoWarp(p, false);
						else if (TICK_DIFF(now, pdata->speededAt) > warpDelay)
							DoWarp(p, true);
					}
					else
					{
						DoWarp(p, false);
					}
				}
			}
		}

		if (adata->fakePlayer && now != pdata->sentFakeRepelAt) // do not send a position packet if we already sent one with the same timestamp
		{
			if ( (adata->config.useFakeRepel && pdata->sentFakeRepelAt && TICK_GT(now, pdata->sentFakeRepelAt)) ||
			     (adata->config.sendAntiWarpPacket && (!pdata->fakePlayer_lastPos || TICK_DIFF(now, pdata->fakePlayer_lastPos) > 50) )
			   )
			{
				pdata->sentFakeRepelAt = 0;

				if (p->pid & 0xFF00) // pid is to large
				{
					wpn.type = S2C_WEAPON;
					wpn.rotation = 0;
					wpn.time = now & 0xFFFF;
					wpn.x = -1;
					wpn.y = -1;
					//wpn.x = 50;
					//wpn.y = 50;
					wpn.xspeed = 0;
					wpn.yspeed = 0;
					wpn.playerid = adata->fakePlayer->pid;
					wpn.checksum = 0;
					wpn.status = STATUS_STEALTH | STATUS_CLOAK | STATUS_UFO;
					if (adata->config.sendAntiWarpPacket)
						wpn.status |= STATUS_ANTIWARP;
					wpn.c2slatency = 0;
					wpn.bounty = 0;
					wpn.weapon = (struct Weapons) {W_NULL,0,0,0,0,0};
					wpn.extra = (struct ExtraPosData) {0,0,0,0,0,0,0,0,0,0,0,0,0};

					DoWeaponChecksum(&wpn);
					net->SendToOne(p, (byte*)&wpn, sizeof(struct S2CWeapons) - sizeof(struct ExtraPosData), NET_UNRELIABLE | NET_DROPPABLE | NET_PRI_P3);
				}
				else
				{
					pos.type = S2C_POSITION;
					pos.rotation = 0;
					pos.time = now & 0xFFFF;
					pos.x = -1;
					pos.y = -1;
					//pos.x = 50;
					//pos.y = 50;
					pos.xspeed = 0;
					pos.yspeed = 0;
					pos.c2slatency = 0;
					pos.bounty = 0;
					pos.playerid = (u8) adata->fakePlayer->pid;
					pos.status = STATUS_STEALTH | STATUS_CLOAK | STATUS_UFO;
					if (adata->config.sendAntiWarpPacket)
						pos.status |= STATUS_ANTIWARP;
					pos.extra = (struct ExtraPosData) {0,0,0,0,0,0,0,0,0,0,0,0,0};
					net->SendToOne(p, (byte*)&pos, sizeof(struct S2CPosition) - sizeof(struct ExtraPosData), NET_UNRELIABLE | NET_DROPPABLE | NET_PRI_P3);
				}
			}
		}
	}

	pd->Unlock();
	UnlockPD();

	return 1;
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	LockPD();
	DoWarp(p, false);
	UnlockPD();
}

static void SpawnCB(Player *p, int reason)
{
	if (reason & SPAWN_AFTERDEATH || reason & SPAWN_SHIPCHANGE || reason & SPAWN_INITIAL)
	{
		LockPD();
		DoWarp(p, false);
		UnlockPD();
	}
}

static void PPKCB(Player *p, const struct C2SPosition *pos)
{
	struct PlayerData *pdata;
	if (pos->weapon.type != W_NULL)
	{
		LockPD();
		pdata = GetPlayerData(p);
		pdata->firedWeapon = true;
		UnlockPD();
	}
}

static void Cresetturrets(const char *tc, const char *params, Player *p, const Target *target)
{
	struct ArenaData *adata;
	adata = GetArenaData(p->arena);

	if (adata->fakePlayer)
	{
		fake->EndFaked(adata->fakePlayer);
		adata->fakePlayer = NULL;
	}
}


static Iflaggame flaggame =
{
	INTERFACE_HEAD_INIT(I_FLAGGAME, "warpdrivefg")
	FlagGameInit,
	FlagGameFlagTouch,
	FlagGameCleanup
};


static Iwarpdrive warpdrive =
{
        INTERFACE_HEAD_INIT(I_WARPDRIVE, "warpdrive")

        DoWarpInt,
        IsWarpedInt,
        SetWarpSpeedInt,
        SetWarpDelayInt
};

EXPORT const char info_warpdrive[] = "WarpDrive $Revision: 25 $ by JoWie\n";
static void ReleaseInterfaces()
{
	mm->ReleaseInterface(ml       );
        mm->ReleaseInterface(aman     );
        mm->ReleaseInterface(pd       );
        mm->ReleaseInterface(net      );
        mm->ReleaseInterface(cmd      );
        mm->ReleaseInterface(lm       );
        mm->ReleaseInterface(mapdata  );
        mm->ReleaseInterface(cfg      );
        mm->ReleaseInterface(flagcore );
        mm->ReleaseInterface(fake     );
}

EXPORT int MM_warpdrive(int action, Imodman *mm_, Arena *arena)
{
	struct ArenaData *adata;

        if (action == MM_LOAD)
        {
                mm        = mm_;
                ml        = mm->GetInterface(I_MAINLOOP        , ALLARENAS);
                aman      = mm->GetInterface(I_ARENAMAN        , ALLARENAS);
                pd        = mm->GetInterface(I_PLAYERDATA      , ALLARENAS);
                net       = mm->GetInterface(I_NET             , ALLARENAS);
                cmd       = mm->GetInterface(I_CMDMAN          , ALLARENAS);
                lm        = mm->GetInterface(I_LOGMAN          , ALLARENAS);
                mapdata   = mm->GetInterface(I_MAPDATA         , ALLARENAS);
                cfg       = mm->GetInterface(I_CONFIG	       , ALLARENAS);
                flagcore  = mm->GetInterface(I_FLAGCORE	       , ALLARENAS);
                fake      = mm->GetInterface(I_FAKE	       , ALLARENAS);



                if (!ml || !aman || !pd || !net || !cmd || !lm || !mapdata || !cfg || !flagcore || !fake)
                {
                        ReleaseInterfaces();
                        printf("<warpdrive> Missing interfaces\n");
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct ArenaData));
                playerKey = pd->AllocatePlayerData(sizeof(struct PlayerData));

                if (arenaKey == -1 || playerKey == -1) // check if we ran out of memory
                {
                        if (arenaKey  != -1) // free data if it was allocated
                                aman->FreeArenaData(arenaKey);

                        if (playerKey != -1) // free data if it was allocated
                                pd->FreePlayerData (playerKey);

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                mm->RegCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);


                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);

		mm->UnregCallback(CB_NEWPLAYER, NewPlayerCB, ALLARENAS);
                ReleaseInterfaces();

                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
        	mm->RegInterface(&flaggame, arena); // flagcore is not really used, however this module is incompatible with other flag games so register an interface so it errors out when fg_wz or something is attached to
        	mm->RegInterface(&warpdrive, arena);

		adata = GetArenaData(arena);
		adata->enabled = true;

		mm->RegCallback(CB_ARENAACTION, ArenaActionCB, arena); // AA_CREATE is fired after MM_ATACH (AA_PRECREATE is not)
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->RegCallback(CB_SPAWN, SpawnCB, arena);
		mm->RegCallback(CB_PPK, PPKCB, arena);


		ReadConfig(arena);
		ml->SetTimer((TimerFunc) UpdateWarpTimer, adata->config.sendPositionDelay, adata->config.sendPositionDelay, arena, arena);
		cmd->AddCommand("resetturrets", Cresetturrets, arena, NULL);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
        	if (mm->UnregInterface(&flaggame, arena))
			return MM_FAIL;
		
		if (mm->UnregInterface(&warpdrive, arena))
		{
			mm->RegInterface(&flaggame, arena);
			return MM_FAIL;
		}
		
		ml->ClearTimer((TimerFunc) UpdateWarpTimer, arena);
		adata = GetArenaData(arena);
		if (adata->fakePlayer)
		{
			fake->EndFaked(adata->fakePlayer);
			adata->fakePlayer = NULL;
		}
		adata->enabled = false;

		cmd->RemoveCommand("resetturrets", Cresetturrets, arena);
		mm->UnregCallback(CB_ARENAACTION, ArenaActionCB, arena);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->UnregCallback(CB_SPAWN, SpawnCB, arena);
		mm->UnregCallback(CB_PPK, PPKCB, arena);

		ArenaActionCB(arena, AA_DESTROY);

                return MM_OK;
        }

        return MM_FAIL;
}

