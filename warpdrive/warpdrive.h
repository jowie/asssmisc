/* $Id: warpdrive.h 25 2010-09-12 19:14:16Z jowie $ */
#ifndef WARPDRIVE_H_INCLUDED
#define WARPDRIVE_H_INCLUDED

#include <stdbool.h>
#define I_WARPDRIVE "warpdrive-1"

#define CB_WARPDRIVE_WARP "warpdrive-warp-1"
typedef void (*WarpDriveWarpFunc)(Player *p, bool warp);

typedef struct Iwarpdrive
{
	INTERFACE_HEAD_DECL

	// Returns true if the player is now in warp, returns false if the player is not in warp
	bool (*DoWarp)(Player *p, bool warp);
	
	
	bool (*IsWarped)(Player *p);
	
	// Overrides the arena config's required warp speed or delay for the player
	// Note that this is regardless of ship; The value only clears when the player changes arena
	// Pass -1 to reset to the default value
	// Pass 0 to SetWarpSpeed to disable warp
	void (*SetWarpSpeed)(Player *p, int speed);
	void (*SetWarpDelay)(Player *p, int delay);
} Iwarpdrive;

#endif
