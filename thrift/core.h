/* 
ASSS Thrift 
Copyright (c) 2011  Joris v/d Wel
jowie@welcome-to-the-machine.com

This file is part of ASSS Thrift

   ASSS Thrift is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Thrift is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with ASSS Thrift.  If not, see <http://www.gnu.org/licenses/>.
  
   In addition, the following supplemental terms based on section 7 of
   the GNU General Public License (version 3):
   a) Preservation of all legal notices and author attributions
   b) Prohibition of misrepresentation of the origin of this material, and
      modified versions are required to be marked in reasonable ways as
      different from the original version
*/

#ifndef ASSS_THRIFT_CORE_H
#define ASSS_THRIFT_CORE_H

#pragma GCC diagnostic warning "-Wall"

#include <protocol/TBinaryProtocol.h>
#include "MyTNonblockingServer.h"
#include <transport/TServerSocket.h>
#include <transport/TBufferTransports.h>
#include <concurrency/PosixThreadFactory.h>

#include <list>
#include <queue>
#include <sys/time.h>

#include "ASSSCore.h"
#include "asss_constants.h"
#include "asss_types.h"
#include "core_constants.h"
#include "core_types.h"
#include "event_constants.h"
#include "event_types.h"
#include "TASSS.h"

// Include all C++ headers before the C includes
extern "C" 
{
#include "asss.h"
#include "fake.h"
#include "objects.h"
#include "packets/objects.h"
}
// Make sure any "using namespace" go after the C includes


namespace ThriftAsss {

using boost::shared_ptr;

Iarenaman *aman;
Ibricks *bricks; // optional
Ichat *chat;
Icmdman *cmd;
Iconfig *cfg;
Ifake *fake;  // optional
Igame *game;
Ilogman *lm;
Imainloop *ml;
Imapdata *mapdata;
Imodman *mm;
Inet *net;
Iobjects *objects; // optional
Iplayerdata *pd;
Istats *stats; // optional

void logman_backtrace();
void fireEvent(shared_ptr<TPoll> event, TArenaID isPosition);
void throwUnknownPlayer();
void throwUnknownArena();
void throwNotImplemented();
void throwMissingModule(const char *module);
void throwInvalidArgument(const std::string& message);
Player * TPlayerID_to_Player(TPlayerID id); // throwUnknownPlayer
Arena * TArenaID_to_Arena(TArenaID id);  // throwUnknownArena
char TLogLevel_to_level(const TLogLevel::type level);
void Target_to_TTarget(const Target *tgt, TTarget *tTarget);
void TTarget_to_Target(const TTarget *tTarget, Target *tgt); // throwUnknownPlayer / throwUnknownArena
TPoint createPoint(int16_t x, int16_t y);
TTime milliTime();
ticks_t TTime_to_ticks(TTime tTime);
TTime ticks_to_TTime(ticks_t time);

class Client : virtual public TASSSIf // Our thrift service
{
public:
	TConnection *connection;
	ticks_t lastPoll;
	std::string modulename;
	bool authenticated;
	std::queue< shared_ptr<TPoll> > queuedEvents; // lock with queuedEvents_mtx // todo: queuedLargeEvents
	std::map<TArenaID, bool> sendPositionEvents; // lock with queuedEvents_mtx
	pthread_mutex_t queuedEvents_mtx;
	
	
	std::list<Player*> myFakePlayers;
	pthread_mutex_t myFakePlayers_mtx;
	
	Client(TConnection *connection_);
	~Client();
	void inline lockQueuedEvents();
	void inline unlockQueuedEvents();
	void inline lockMyFakePlayers();
	void inline unlockMyFakePlayers();
	
	void connectionClosed(bool pollTimeout, int debug_line); // Called (indirectly) by the TNonBlockingServer  
	void throwIfNotAuthenticated();
	
protected:
	
	void authenticate(const std::string& modulename_, const std::string& password) __attribute__((optimize(0))); // http://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html#Function-Attributes
	void poll(std::vector<TPoll> & _return);
	void sendPositionData(const TArenaID arena, const bool send);
	bool recycleArena(const TArenaID arena);
	void addCommand(const std::string& command, const TArenaID arena, const std::string& helptext);
	void removeCommand(const std::string& command, const TArenaID arena);
	void dropBrick(const TArenaID arena, const TFreqID freq, const TPoint& from, const TPoint& to);
	void sendChatMessage(const TTarget& target, const TChatType::type type, const std::string& message, const TPlayerID from, const int8_t sound);
	TPlayerID createFakePlayer(const std::string& name, const TArenaID arena, const TShip::type ship, const TFreqID freq);
	void endFakePlayer(const TPlayerID player);
	void setShipFreq(const TPlayerID player, const TShip::type ship, const TFreqID freq);
	void warpTo(const TTarget& target, const TPoint& tile);
	void sendToArena(const TPlayerID player, const std::string& arenaname, const TPoint& spawn);
	void givePrize(const TTarget& target, const int16_t type, const int16_t count);
	void lockPlayers(const TTarget& target, const bool notify, const bool spec, const int16_t timeout);
	void unlockPlayers(const TTarget& target, const bool notify);
	void lockArena(const TArenaID arena, const bool notify, const bool onlyarenastate, const bool initial, const bool spec);
	void unlockArena(const TArenaID arena, const bool notify, const bool onlyarenastate);
	void fakePosition(const TPlayerID player, const TPosition& position);
	void fakeKill(const TPlayerID killer, const TPlayerID killed, const int16_t pts);
	void shipReset(const TTarget& target);
	void setPlayerEnergyViewing(const TPlayerID player, const int16_t value);
	void setSpectatorEnergyViewing(const TPlayerID player, const int16_t value);
	void resetPlayerEnergyViewing(const TPlayerID player);
	void resetSpectatorEnergyViewing(const TPlayerID player);
	void log(const TLogLevel::type tlevel, const std::string& line);
	void logA(const TLogLevel::type level, const std::string& mod, const TArenaID arena, const std::string& line);
	void logP(const TLogLevel::type level, const std::string& mod, const TPlayerID player, const std::string& line);
	void getMapChecksum(TMapName& _return, const TArenaID arena);
	bool getMapData(const TArenaID arena);
	void statIncrement(const TPlayerID player, const int16_t stat, const int16_t amount);
	void statStartTimer(const TPlayerID player, const int16_t stat);
	void statStopTimer(const TPlayerID player, const int16_t stat);
	void statSet(const TPlayerID player, const int16_t stat, const int16_t interval, const int16_t value);
	int16_t statGet(const TPlayerID player, const int16_t stat, const int16_t interval);
	void statSendUpdates(const TPlayerID exclude);
	void statScoreReset(const TPlayerID player, const int16_t interval);
	void getConfig(std::vector<std::string> & _return, const TArenaID arena, const std::vector<std::string> & keys);
	void lvzToggle(const TTarget& target, const std::map<TObjectID, bool> & objects);
	void lvzAttr(const TTarget& target, const TObjectID id, const TLVZPosition& position, const int16_t image, const TLVZLayer::type layer, const int16_t displayTime, const TLVZMode::type mode);
	void lvzTogglePersistent(const TArenaID arena, const std::map<TObjectID, bool> & objects);
	void lvzMovePersistent(const TArenaID arena, const TObjectID id, const TPoint& position, const TLVZScreenOffset& offset);
	void lvzImagePersistent(const TArenaID arena, const TObjectID id, const int16_t image);
	void lvzLayerPersistent(const TArenaID arena, const TObjectID id, const TLVZLayer::type layer);
	void lvzTimerPersistent(const TArenaID arena, const TObjectID id, const int16_t time);
	void lvzModePersistent(const TArenaID arena, const TObjectID id, const TLVZMode::type mode);
};


}

#endif