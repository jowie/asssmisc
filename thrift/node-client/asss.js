// todo try catch instead of isConnected

var thrift = require('thrift');
var events = require("events");
var util = require('util');
var swa = require('./swissarmyknife.js');
var ASSS = exports;

var asss_types = require('./gen-nodejs/asss_types.js');
var core_types = require('./gen-nodejs/core_types.js');
var event_types = require('./gen-nodejs/event_types.js');
var TASSS = require('./gen-nodejs/TASSS.js');

// todo: make player id's fully transparent (receiving a target, sending a target, etc)

global.core_ttypes = core_types; // https://issues.apache.org/jira/browse/THRIFT-1402

var Client;
var Arena;
var Player;

var types = ASSS.types = {};
(function()
{
	var a;
	for (a in asss_types){ASSS.types[a] = asss_types[a]; }
	for (a in core_types){ASSS.types[a] = core_types[a]; }
	for (a in event_types){ASSS.types[a] = event_types[a]; }
}());

ASSS.ConnectionError = function(message, errno, syscall){this.message = message; this.errno = errno; this.syscall = syscall};
ASSS.ConnectionError.prototype = new Error();


ASSS.ZoneTarget = new types.TTarget();
ASSS.ZoneTarget.type = types.TTargetType.ZONE;

ASSS.Client = Client = function(modulename, password, ip, port)
{
	events.EventEmitter.call(this);
	
	this.modulename = modulename;
	this.password = password;
	this.ip = ip || '127.0.0.1'; 
	this.port = port || 7487;
};
Client.prototype = new events.EventEmitter();

// (re)connect to the server. Note that when reconnecting, arena data, player data, fake players, et cetera will be gone
Client.prototype.connect = function(onConnect)
{
	this._connectionError_emitted = false;
	this._connection = thrift.createConnection(this.ip, this.port);
	this._connection.on('error', CB(this, this._thriftConnectionError));
	this.thrift = thrift.createClient(TASSS, this._connection);
	this._connection.on('connect', CB(this, this._connected));
	if (onConnect)
	{
		this._connection.on('connect', onConnect);
	}
};

Client.prototype.disconnect = function()
{
	if (this._pollTimer)
	{
		clearInterval(this._pollTimer);
		this._pollTimer = 0;
	}
	
	if (this._connection)
	{
		this._connection.connection.end();
	}
};

function THRIFT1297(obj) // https://issues.apache.org/jira/browse/THRIFT-1297
{
	var a;
	if (Array.isArray(obj))
	{
		for (a = 0; a < obj.length; a++)
		{
			obj[a] = THRIFT1297(obj[a]);
		}
		return obj;
	}
	
	switch(typeof obj)
	{
		case 'boolean':
			if (!obj)
			{
				return new Boolean(obj);
			}
			return obj;
		case 'number':
			if (!obj)
			{
				return new Number(obj);
			}
			return obj;
		case 'string':
			if (!obj)
			{
				return new String(obj);
			}
			return obj;
		case 'object':
			for (a in obj)
			{
				if (obj.hasOwnProperty(a))
				{
					obj[a] = THRIFT1297(obj[a]);
				}
			}
			return obj;
	}
	return obj;
}
require('thrift/lib/thrift/protocol.js').TBinaryProtocol.prototype.writeBool = function(bool) { // https://issues.apache.org/jira/browse/THRIFT-1297
  if (bool.valueOf()) {
    this.writeByte(1);
  } else {
    this.writeByte(0);
  }
}

Client.prototype.isConnected = function()
{
	return this._connection &&
	       this._connection.connection &&
	       this._connection.connection.writable &&
	       this._connection.connection.readable
	       ;
};

Client.prototype._handleNetException = function(ex, callback)
{
	var err;
	if (ex.errno && ex.errno[0] == 'E') // Some kind of UV error from node (like ECONNRESET)
	{
		err = new ASSS.ConnectionError(ex.message, ex.errno, ex.syscall);
		
		if (typeof callback == 'function')
		{
			callback(err);
		}
		this._connectionError(err);
	}
	else
	{
		throw ex; // An error that is not related to the connection. This should not happen
	}
};

Client.prototype._emitIfNotConnected = function(callback)
{
	var err;
	if (!this.isConnected())
	{
		err = new ASSS.ConnectionError('Connection was dropped', null, null);
		
		if (typeof callback == 'function')
		{
			setTimeout(callback, 0, err);
		}
		this._connectionError(err);
		return true;
	}
	
	return false; // ok
};

Client.prototype._doThriftCall = function(fn, args)
{
	var a;
	args = Array.prototype.slice.call(args);

	if (!args.length || typeof args[args.length-1] != 'function') // no callback given
	{
		args[args.length] = CB(this, function(err)
		{
			if (err)
			{
				d('Uncaught thrift exception', err, 'from', fn);
				this.emit('thrift-exception', err);
			}
		});
	}
	args = THRIFT1297(args);
	
	if (this._emitIfNotConnected(args[args.length-1]))
	{
		return;
	}
	
	try
	{
		fn.apply(this.thrift, args);
	}
	catch(ex)
	{
		this._handleNetException(ex, args[args.length-1]);
	}
};


// ^\s*virtual\s+\w+\s(\w+).*    ->    Client.prototype.$1 = function() { this._doThriftCall(this.thrift.$1, arguments); };
Client.prototype.sendPositionData = function(arenaid, send)
{
	this._doThriftCall(this.thrift.sendPositionData, arguments);
	this.arenas[arenaid].sendingPosition = send;
};
Client.prototype.recycleArena = function() { this._doThriftCall(this.thrift.recycleArena, arguments); };
Client.prototype.addCommand = function() { this._doThriftCall(this.thrift.addCommand, arguments); };
Client.prototype.removeCommand = function() { this._doThriftCall(this.thrift.removeCommand, arguments); };
Client.prototype.dropBrick = function() { this._doThriftCall(this.thrift.dropBrick, arguments); };
Client.prototype.sendChatMessage = function() { this._doThriftCall(this.thrift.sendChatMessage, arguments); };
Client.prototype.createFakePlayer = function(name, arena, ship, freq, callback)
{
	if (this._emitIfNotConnected(callback))
	{
		return;
	}
	
	try
	{
		this.thrift.createFakePlayer(THRIFT1297(name), THRIFT1297(arena), THRIFT1297(ship), THRIFT1297(freq), CB(this, function(err, fakeid)
		{
			var player;
			if (err)
			{
				if (callback)
				{
					callback(err);
				}
				else
				{
					d('Uncaught thrift exception', err, 'from', this.thrift.createFakePlayer);
					this.emit('thrift-exception', err);	
				}
				return;
			}
			
			player = this.players[fakeid];
			if (!player)
			{
				player = new Player(this, {id: fakeid,
				                     name: name,
						     ship: ship,
						     freq: freq,
						     arena: arena,
						     type: types.TClientType.FAKE
						     });
				this.players[player.id] = player;
			}
			
			if (callback)
			{
				callback(null, player);
			}
		}));
	}
	catch(ex)
	{
		this._handleNetException(ex, callback);
	}
};
Client.prototype.endFakePlayer = function() { this._doThriftCall(this.thrift.endFakePlayer, arguments); };
Client.prototype.setShipFreq = function() { this._doThriftCall(this.thrift.setShipFreq, arguments); };
Client.prototype.warpTo = function() { this._doThriftCall(this.thrift.warpTo, arguments); };
Client.prototype.sendToArena = function() { this._doThriftCall(this.thrift.sendToArena, arguments); };
Client.prototype.givePrize = function() { this._doThriftCall(this.thrift.givePrize, arguments); };
Client.prototype.lockPlayers = function() { this._doThriftCall(this.thrift.lockPlayers, arguments); };
Client.prototype.unlockPlayers = function() { this._doThriftCall(this.thrift.unlockPlayers, arguments); };
Client.prototype.lockArena = function() { this._doThriftCall(this.thrift.lockArena, arguments); };
Client.prototype.unlockArena = function() { this._doThriftCall(this.thrift.unlockArena, arguments); };
Client.prototype.fakePosition = function() { this._doThriftCall(this.thrift.fakePosition, arguments); };
Client.prototype.fakeKill = function() { this._doThriftCall(this.thrift.fakeKill, arguments); };
Client.prototype.shipReset = function() { this._doThriftCall(this.thrift.shipReset, arguments); };
Client.prototype.setPlayerEnergyViewing = function() { this._doThriftCall(this.thrift.setPlayerEnergyViewing, arguments); };
Client.prototype.setSpectatorEnergyViewing = function() { this._doThriftCall(this.thrift.setSpectatorEnergyViewing, arguments); };
Client.prototype.resetPlayerEnergyViewing = function() { this._doThriftCall(this.thrift.resetPlayerEnergyViewing, arguments); };
Client.prototype.resetSpectatorEnergyViewing = function() { this._doThriftCall(this.thrift.resetSpectatorEnergyViewing, arguments); };
Client.prototype.log = function() { this._doThriftCall(this.thrift.log, arguments); };
Client.prototype.logA = function() { this._doThriftCall(this.thrift.logA, arguments); };
Client.prototype.logP = function() { this._doThriftCall(this.thrift.logP, arguments); };
Client.prototype.getMapChecksum = function() { this._doThriftCall(this.thrift.getMapChecksum, arguments); };
Client.prototype.getMapData = function() { this._doThriftCall(this.thrift.getMapData, arguments); };
Client.prototype.statIncrement = function() { this._doThriftCall(this.thrift.statIncrement, arguments); };
Client.prototype.statStartTimer = function() { this._doThriftCall(this.thrift.statStartTimer, arguments); };
Client.prototype.statStopTimer = function() { this._doThriftCall(this.thrift.statStopTimer, arguments); };
Client.prototype.statSet = function() { this._doThriftCall(this.thrift.statSet, arguments); };
Client.prototype.statGet = function() { this._doThriftCall(this.thrift.statGet, arguments); };
Client.prototype.statSendUpdates = function() { this._doThriftCall(this.thrift.statSendUpdates, arguments); };
Client.prototype.statScoreReset = function() { this._doThriftCall(this.thrift.statScoreReset, arguments); };
Client.prototype.getConfig = function() { this._doThriftCall(this.thrift.getConfig, arguments); };
Client.prototype.lvzToggle = function() { this._doThriftCall(this.thrift.lvzToggle, arguments); };
Client.prototype.lvzAttr = function() { this._doThriftCall(this.thrift.lvzAttr, arguments); };
Client.prototype.lvzTogglePersistent = function() { this._doThriftCall(this.thrift.lvzTogglePersistent, arguments); };
Client.prototype.lvzMovePersistent = function() { this._doThriftCall(this.thrift.lvzMovePersistent, arguments); };
Client.prototype.lvzImagePersistent = function() { this._doThriftCall(this.thrift.lvzImagePersistent, arguments); };
Client.prototype.lvzLayerPersistent = function() { this._doThriftCall(this.thrift.lvzLayerPersistent, arguments); };
Client.prototype.lvzTimerPersistent = function() { this._doThriftCall(this.thrift.lvzTimerPersistent, arguments); };
Client.prototype.lvzModePersistent = function() { this._doThriftCall(this.thrift.lvzModePersistent, arguments); };


Client.prototype._thriftConnectionError = function(ex)
{
	this._connectionError(new ASSS.ConnectionError(ex.message, ex.errno, ex.syscall));
};

Client.prototype._connectionError = function(err)
{
	d('ASSS Connection Error: ', err);
	
	if (this._pollTimer)
	{
		clearInterval(this._pollTimer);
		this._pollTimer = 0;
	}
	
	if (!this._connectionError_emitted)
	{
		this.emit('error', err); // throws without handlers
		this._connectionError_emitted = true;
	}
};

Client.prototype._connected = function(err)
{
	this.arenas = {};
	this.players = {};
	
	if (this._pollTimer)
	{
		clearInterval(this._pollTimer);
		this._pollTimer = 0;
	}
	
	if (this._emitIfNotConnected())
	{
		return;
	}
	
	try 
	{
		this.thrift.authenticate(this.modulename, this.password, CB(this, function(err)
		{
			if (err)
			{
				d('Authentication error:', err);
				this._connection.connection.end();
				this.emit('error', err);
				return;
			}
			
			this._pollTimer = setInterval(CB(this, function()
			{
				if (this._emitIfNotConnected())
				{
					return;
				}
				
				
				try
				{
					this.thrift.poll(CB(this, this._pollReceived));
				}
				catch(ex)
				{
					this._handleNetException(ex);
				}
			}), 10);
			
			this.emit('connected');
		}));
	}
	catch(ex)
	{
		this._handleNetException(ex);
	}
	
};

Client.prototype._pollReceived = function(err, events)
{
	var a, polls, player, killed;
	if (err)
	{
		d('Poll error', err);
	}
	
	if (events)
	{
		for (a = 0; a < events.length; a++)
		{
			poll = events[a];
			if (poll.ArenaChanged)
			{
				this._arenaChanged(poll.ArenaChanged);
			}
			
			if (poll.PlayerChanged)
			{
				this._playerChanged(poll.PlayerChanged);
			}
			
			if (poll.Kill)
			{
				killed = this.players[poll.Kill.killed];
				if (killed.arena.sendingPosition)
				{
					killed.isDead = true;
				}
				this.emit('kill', this.players[poll.Kill.killer], killed, poll.Kill.bounty);
			}
			
			if (poll.Spawn)
			{
				this.emit('spawn', this.players[poll.Spawn.player], poll.Spawn.reason);
			}
			
			if (poll.Position)
			{
				player = this.players[poll.Position.position.player];
				if (player.arena.id != poll.Position.arena)
				{
					d('WARNING: Received a position packet with an incorrect player+arena combination', player.arena.id, poll.Position.arena);
				}
				
				player.position = poll.Position.position;
				if (player.arena.sendingPosition && player.position.position.x >= 0)
				{
					player.isDead = false;
				}
				this.emit('position', player, poll.Position.position);
			}
			
			if (poll.Attach)
			{
				player = this.players[poll.Attach.player];
				this.emit('attach', player, poll.Attach.to ? this.players[poll.Attach.to] : null);
			}
			
			if (poll.Goal)
			{
				this.emit('goal', this.players[poll.Goal.player], poll.Goal.position);
			}
			
			if (poll.Chat)
			{
				this.emit('chat', poll.Chat.player ? this.players[poll.Chat.player] : null, 
				                  poll.Chat.type, poll.Chat.sound, 
						  poll.Chat.target ? this.players[poll.Chat.target] : null, 
						  poll.Chat.freq, 
						  poll.Chat.text);
			}
			
			if (poll.MapData)
			{
			
			}
			
			if (poll.Command)
			{
				this.emit('command', 
				          poll.Command.command,
				          poll.Command.params,
				          this.players[poll.Command.player],
				          poll.Command.target);
				
				this.emit('command-'+poll.Command.command, 
				          poll.Command.command,
				          poll.Command.params,
				          this.players[poll.Command.player],
				          poll.Command.target);
			}
		}
	}
};

Client.prototype._arenaChanged = function(arenaChangedEvent)
{
	var arena, prop, val;
	
	arena = this.arenas[arenaChangedEvent.arena.id];
	if (arena)
	{
		for (prop in arenaChangedEvent.arena)
		{
			if (!arenaChangedEvent.arena.hasOwnProperty(prop)){ continue; }
			val = arenaChangedEvent.arena[prop];
			if (val === null || val === undefined)
			{
				continue;
			}
			
			arena[prop] = val;
		}
	}
	else
	{
		arena = this.arenas[arenaChangedEvent.arena.id] = new Arena(this, arenaChangedEvent.arena);
	}
	
	if (arena.destroyed)
	{
		delete this.arenas[arena.id];
	}
	
	switch(arenaChangedEvent.reason)
	{
		case core_types.TArenaChangeReason.INITIAL:
			d(arena +' was already present');
			this.emit('arena-initial', arena);
		break;
		case core_types.TArenaChangeReason.CREATED:
			d(arena +' created');
			this.emit('arena-created', arena);
		break;
		case core_types.TArenaChangeReason.DESTROYED:
			d(arena +' destroyed');
			this.emit('arena-destroyed', arena);
		break;
		case core_types.TArenaChangeReason.CONFCHANGED:
			d(arena +' asss configuration changed');
			this.emit('arena-confchanged', arena);
		break;
		default:
			d(arena +' changed for an unknown or unspecified reason');
			this.emit('arena-changed-unspecified', arena);
	}
	
};

Client.prototype._playerChanged = function(playerChangedEvent)
{
	var player, prop, val;
	var oldarena, oldship, oldfreq;
	
	player = this.players[playerChangedEvent.player.id];
	if (player)
	{
		oldarena = player.arena;
		oldship = player.ship;
		oldfreq = player.freq;
		
		for (prop in playerChangedEvent.player)
		{
			if (!playerChangedEvent.player.hasOwnProperty(prop)){ continue; }
			val = playerChangedEvent.player[prop];
			if (val === null || val === undefined)
			{
				continue;
			}
			
			if (prop === 'arena')
			{
				player[prop] = this.arenas[val];
			}
			else
			{
				player[prop] = val;
			}
		}
	}
	else
	{
		player = this.players[playerChangedEvent.player.id] = new Player(this, playerChangedEvent.player);
	}
	
	if (player.destroyed)
	{
		delete this.players[player.id];
	}
	
	switch(playerChangedEvent.reason)
	{
		case core_types.TPlayerChangeReason.INITIAL:
			d(player +' was already present');
			this.emit('player-initial', player);
		break;
		case core_types.TPlayerChangeReason.CONNECT:
			d(player +' connected');
			this.emit('player-connected', player);
		break;
		case core_types.TPlayerChangeReason.DISCONNECT:
			d(player +' disconnected');
			this.emit('player-disconnected', player);
		break;
		case core_types.TPlayerChangeReason.ENTERARENA:
			d(player +' entered the arena', player.arena.name);
			player.position = null;
			player.isDead = null;
			this.emit('player-enterarena', player, oldarena);
		break;
		case core_types.TPlayerChangeReason.ENTERGAME:
			d(player +' has started sending position packets in', player.arena.name);
			this.emit('player-entergame', player);
		break;
		case core_types.TPlayerChangeReason.LEAVEARENA:
			d(player +' left the arena', oldarena.name);
			player.position = null;
			player.isDead = null;
			this.emit('player-leavearena', player, oldarena);
		break;
		case core_types.TPlayerChangeReason.SHIPFREQCHANGE:
			d(player +' changes ship/freq to', player.ship, player.freq);
			this.emit('player-shipfreqchange', player, oldship, oldfreq);
		break;
		default:
			d(player +' changes for an unspecified or unknown reason');
			this.emit('player-changed-unspecified', player);
		break;
	}
	
};

ASSS.Arena = Arena = function(asssClient, tArenaData)
{
	this.asssClient = asssClient;
	for (prop in tArenaData) // TArenaData
	{
		if (!tArenaData.hasOwnProperty(prop)){ continue; }
		this[prop] = tArenaData[prop];
	}
	
	this.sendingPosition = false;
};

Arena.prototype.toString = function()
{
	return 'Arena "' + this.name + '"';
};

Arena.prototype.toTarget = function(freq)
{
	var target;
	
	target = new types.TTarget();
	target.arena = this.id;
	
	if (freq == undefined)
	{
		target.type = types.TTargetType.ARENA;
	}
	else
	{
		target.type = types.TTargetType.FREQ;
		target.freq = freq;
	}
	
	return target;
};

ASSS.Player = Player = function(asssClient, tPlayerData)
{
	this.asssClient = asssClient;
	for (prop in tPlayerData) // TPlayerData
	{
		if (!tPlayerData.hasOwnProperty(prop)){ continue; }
		
		if (prop === 'arena')
		{
			this[prop] = this.asssClient.arenas[tPlayerData[prop]];
		}
		else
		{
			this[prop] = tPlayerData[prop];
		}
	}
	this.isDead = null; // null = not sending position for this arena; false = player is not dead; true = player is dead
	
	// see struct TPlayerData in core.thrift for attributes
};

Player.prototype.toString = function()
{
	var ret;
	
	if (this.type == types.TClientType.FAKE)
	{
		ret = 'Fake Player ';
	}
	else if (this.type == types.TClientType.CHAT)
	{
		ret = 'ChatNet';
	}
	else
	{
		ret = 'Player ';
	}
	
	ret += '"' + this.name + '"';
	
	
	return ret;
};

Player.prototype.toTarget = function()
{
	var target;
	
	target = new types.TTarget();
	target.type = types.TTargetType.PLAYER;
	target.player = this.id;
	return target;
};

ASSS.createPoint = function(x, y)
{
	return new types.TPoint({x: x, y: y});
};