global.CB = function(thisObject, callback)
{
	return function() { return callback.apply(thisObject, arguments); };	
};

global.d = function()
{
	var now = new Date();
	console.log.apply(global.$console, ['['+now.getUTCFullYear() + '/' + now.getUTCMonth() + '/' + now.getUTCDate() + ' ' + now.getUTCHours() + ':' + now.getUTCMinutes() + ':' + now.getUTCSeconds()+']'].concat(Array.prototype.slice.call(arguments)));
};

global.Timer = function(time, repeat, callback, thisObj, args)
{
	this.time = time;
	this.repeat = repeat;
	this.callback = callback;
	this.thisObj = thisObj;
	this.args = args;
};

global.Timer.prototype.start = function()
{
	this.stop();
	if (this.repeat)
	{
		this._interval = setInterval(CB(this, function()
		{
			this.callback.apply(this.thisObj || this, this.args || []);
		}), this.time);
	}
	else
	{
		this._timeout = setTimeout(CB(this, function()
		{
			this._timeout = null;
			this.callback.apply(this.thisObj || this, this.args || []);
		}), this.time);
	}
	return this;
};

global.Timer.prototype.stop = function()
{
	if (this._timeout)
	{
		clearTimeout(this._timeout);
		this._timeout = null;
	}
	
	if (this._interval)
	{
		clearInterval(this._interval);
		this._interval = null;
	}
	return this;
};