var ASSS = require('./asss.js');
var types = ASSS.types;
var asss;

asss = new ASSS.Client('test', 'AJ43yAGfrjAo237');
asss.on('arena-created', initArena);
asss.on('arena-initial', initArena);
asss.on('arena-confchanged', readConfig);

function connect()
{
	asss.connect(function()
	{
		asss.log(types.TLogLevel.ERROR, '<js_test> Hello from javascript!');
		asss.addCommand('jstest', 0, 'Displays some test messages');
	});
}

asss.on('error', function(err)
{
	if (err instanceof ASSS.ConnectionError)
	{
		d('Reconnecting...');
		setTimeout(connect, 1000);
	}
	else
	{
		d('wtf');
	}
});
connect();

asss.on('command-jstest', function(command, params, player, target)
{
	asss.sendChatMessage(player.toTarget(), types.TChatType.ARENA, "Test test test!", 0, 0);
});

asss.on('chat', function(player, type, sound, target, freq, text)
{
	if (player && player.arena && text == 'hi')
	{
		asss.sendChatMessage(player.toTarget(), types.TChatType.PRIV, "Hi!", player.arena._example_myFake.id, 0);
	}
});

function readConfig(arena)
{
	asss.getConfig(arena.id, ['Warbird:BulletSpeed'], function(err, values)
	{
		arena._example_warbirdBulletSpeed = (values[0] * 1) || 2000;
		d('Warbird:BulletSpeed', arena._example_warbirdBulletSpeed);
	});
}

function initArena(arena)
{
	readConfig(arena);
	return;
	asss.sendPositionData(arena.id, true);

	function testBot(err, fake)
	{
		var rot;
		if (err)
		{
			d('Error creating fake', err);
			return;
		}
		
		arena._example_myFake = fake;
		rot = Math.floor(Math.random() * 40);
		
		arena._example_fakeTimer = new Timer(1000 / 5, true, function()
		{
			var pos, a, targetting;
			pos = new types.TPosition();
			
			pos.player = fake.id;
			pos.time = 0;
			pos.time = new Date().getTime();
			pos.position = ASSS.createPoint(512 * 16, 512 * 16);
			pos.speed = ASSS.createPoint(0, 0);
			pos.status = new types.TPositionStatus();
			
			pos.status.stealth = false;
			pos.status.cloak = false;
			pos.status.xradar = false;
			pos.status.antiwarp = false;
			pos.status.flash = false;
			pos.status.safezone = false;
			pos.status.ufo = false;
			pos.status.unknown = false;
			
			pos.bounty = 1337;
			pos.energy = Math.floor(Math.random() * 32767);
			
			targetting = nearestPlayer(fake, pos.position);
			if (targetting)
			
			if (targetting)
			{
				rot = fireControl(pos.position, targetting, arena._example_warbirdBulletSpeed);
			
				pos.weapon = new types.TPositionWeapon();
				pos.weapon.type = types.TWeapon.BOUNCEBULLET;
				pos.weapon.level = Math.floor(Math.random() * 3);
				pos.weapon.alternate = true;
				pos.weapon.shrap = 0;
				pos.weapon.shraplevel = 0;
				pos.weapon.shrapbouncing = false;
			}
			
			pos.rotation = rot;
			
			rot = (rot + 1) % 40;
			
			asss.fakePosition(fake.id, pos, function(err)
			{
				if (err)
				{
					d('fake position error', err);
				}
			});
		}).start();
		
		setTimeout(function()
		{
			asss.sendChatMessage(arena.toTarget(), types.TChatType.PUB, "Hello!", fake.id, 0);
		}, 250);
	}
	
	asss.createFakePlayer('~Annoying Bot', arena.id, types.TShip.WARBIRD, 100, testBot);
}

asss.on('arena-destroyed', function(arena)
{
	arena._example_fakeTimer.stop();
	
});

function hypot(dx, dy) // make sure dx and dy are integers!
{
        var r, dd;

        dd = dx*dx+dy*dy;

        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;

        /* initial hypotenuse guess (from Gems) */
        r = (dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));

        if (r == 0) return r;

        /* converge 3 times */
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;

        return r;
}

function nearestPlayer(bot, botPosition)
{
	var x1, y1, dist;
	var PROJECT_FAVOUR = 80;
	var WEAPON_SIGHT = 500;
	var bestPlayer, bestDist;
	
	bestPlayer = null;
	bestDist = 1024 * 16;
	
	for (a in asss.players)
	{
		player = asss.players[a];
		if (!player.position || 
		    player.arena != bot.arena ||
		    player.ship == types.TShip.SPEC ||
		    player.freq == bot.freq ||
		    player.type == types.TClientType.FAKE ||
		    player.position.status.safezone ||
		    player.isDead
		    )
		{
			continue;
		}
		
		x1 = Math.round(player.position.position.x + player.position.speed.x * PROJECT_FAVOUR / 1000);
		y1 = Math.round(player.position.position.y + player.position.speed.y * PROJECT_FAVOUR / 1000);
		
		
		dist = hypot(botPosition.x - x1, botPosition.y - y1);
		
		if (dist > WEAPON_SIGHT)
		{
			continue;
		}
		
		if (dist < bestDist)
		{
			dist = bestDist;
			bestPlayer = player;
		}
	}
	
	return bestPlayer;
}

function fireAngle(x, y)
{
	var angle, a;
	/* [-Pi, Pi] + Pi -> [0, 2Pi] */
	angle = Math.atan2(y, x) + Math.PI;

	a = Math.round(angle * 40 / (2 * Math.PI) + 30);
	/* 0 degrees is +y-axis for us, 0 degrees is +x-axis for atan2 */
	return Math.floor(a % 40);
}

function fireControl(botPosition, targetting, projspeed)
{
	var dx, dy, err;
	var bestdx, bestdy, besterr = 20000;
	var tt = 10, pt;

	bestdx = targetting.position.position.x - botPosition.x;
	bestdy = targetting.position.position.y - botPosition.y;

	do
	{
		dx = Math.round((targetting.position.position.x + targetting.position.speed.x * tt / 1000) - botPosition.x);
		dy = Math.round((targetting.position.position.y + targetting.position.speed.y * tt / 1000) - botPosition.y);
		pt = hypot(dx, dy) * 1000 / projspeed;

		err = Math.abs(pt - tt);
		if (err < besterr)
		{
			besterr = err;
			bestdx = dx;
			bestdy = dy;
		}
		else if (err > besterr)
		{
			break;
		}
		
		tt += 10;
		
	} while(pt > tt && tt <= 250);
	
	
	return fireAngle(bestdx, bestdy);
}