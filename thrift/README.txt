##########################
# Building thrift 0.7.0: #
##########################
# I have no idea if a newer version than 0.7 works
https://thrift.apache.org/download/

On ubuntu you could use the following commands

$ apt-get install libboost-dev libevent-dev pkg-config

Java:
$ apt-get install ant

Perl:
$ apt-get install perl
$ cpan Bit::Vector

building thrift:
$ cd thrift-0.7.0
$ chmod +x configure
$ ./configure
Check the output! (To compile the asss module, the C++ thrift lib must be compiled)
$ make
$ sudo make install

####################
# Building Module: #
####################

You may need to change the include directories in the Makefile, in my ubuntu system, the directories were:
* /usr/local/include
* /usr/local/include/thrift/

And the following library paths
* /usr/lib
* /usr/local/lib

The thrift module will be dynamically linked with a number of external libraries, 
I listed the location on my ubuntu system for the dynamic libraries. (the static libraries 
will end in .a)
 
* thrift (/usr/local/lib/libthrift.so)
* thriftnb (/usr/local/lib/libthriftnb.so)
* thriftz (/usr/local/lib/libthriftz.so)
* event (/usr/lib/libevent.so)

Supposedly it is possible to statically link these into your .so asss library/module, but I have not been 
able to get this to work.


Requires https://bitbucket.org/grelminar/asss/issue/116/configh-evil-typedef

Based on Thrift 0.7.0
Hit make on the module directory (not just asss src directory)
$ cd asss/src/thrift
$ make

# symlink the .so:
$ make install

# remove all build files:
$ make clean

###########
# Config: #
###########
Note: the intention is that thrift clients are run on the same machine or at most on the same lan network.
Note: Also, data is not encrypted.

## global.conf: ##
[Thrift]
Listen = 7487

[ThriftClients]
# Passwords are used by thrift clients to connect
modulename = password
modulename2 = password2
modulename3 = password3

[log_console]
thrift_core = DIMWE

[log_file]
thrift_core = DIMWE

[log_sysop]
thrift_core = DIMWE



## modules.conf: ##
thrift:thrift_core

##############################################
# Running on a server without root /compiler #
##############################################
Upload all the library files (libthrift.so libthrift.so.0 whatever) to your asss folder.
For example asss/sharedlib

Modify your run-asss script:
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ASSSHOME"/sharedlib"

Upload thrift.so (this module) to /bin