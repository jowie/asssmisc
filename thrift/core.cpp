/* 
ASSS Thrift 
Copyright (c) 2011  Joris v/d Wel
jowie@welcome-to-the-machine.com

This file is part of ASSS Thrift

   ASSS Thrift is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Thrift is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with ASSS Thrift.  If not, see <http://www.gnu.org/licenses/>.
  
   In addition, the following supplemental terms based on section 7 of
   the GNU General Public License (version 3):
   a) Preservation of all legal notices and author attributions
   b) Prohibition of misrepresentation of the origin of this material, and
      modified versions are required to be marked in reasonable ways as
      different from the original version
*/
/*
TODO:
 + Brick event
 + Mapdata
 
*/

#warning "todo: brick event"

#pragma GCC diagnostic warning "-Wall"

#include "core.h"
#include <cmath>
#include <execinfo.h>

namespace ThriftAsss {
// But "using namespace" must go after the C includes


using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
//using namespace ::apache::thrift::server;
using namespace ::apache::thrift::concurrency;

using boost::shared_ptr;
using boost::weak_ptr;

/*
locking order:

ClientManager::clients_mtx
commands_mtx
Client::myFakePlayers
Client::queuedEvents
known_mtx
*/

#define  u2_MAX (3)
#define  u4_MAX (15)
#define  u5_MAX (31)
#define  u8_MAX (255)
#define  i8_MAX (127)
#define i16_MIN (-32768)
#define i16_MAX (32767)
#define u16_MAX (65535)
#define i32_MIN (-2147483648)
#define i32_MAX (2147483647)
#define TILE_MAX (1023)
#define POS_MIN (-1)
#define POS_MAX (16383)
#define ROT_MAX (39)
#define SHIP_MAX (8)
#define FREQ_MAX (9999)

#define SANE(val, min, max) ((val) < (min) ? (min) : ((val) > (max) ? (max) : (val)) )



Player * (*originalCreateFakePlayer)(const char *name, Arena *arena, int ship, int freq);

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

#pragma GCC diagnostic ignored "-Wwrite-strings"
void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<thrift_core> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);

	// Ignore -Wwrite-strings because Error uses a char* instead of a const char*
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}
#pragma GCC diagnostic warning "-Wwrite-strings"

class CoreRunner;

shared_ptr<PosixThreadFactory> threadFactory;
shared_ptr<CoreRunner> coreRunner;
shared_ptr<Thread> coreThread;
std::map<TPlayerID, Player*> knownPlayers;
std::map<TArenaID, Arena*> knownArenas;
pthread_mutex_t known_mtx; // for knownPlayers and knownArenas
std::map<TArenaID, std::map<std::string /*command*/, std::map<const Client*, std::string /*helptext*/> > > commands; // do not dereference Client, use it for comparing only
pthread_mutex_t commands_mtx;

void myCommand(const char *command, const char *params, Player *p, const Target *target);

typedef struct playerData
{
	TPlayerID id; // > 0
} *PlayerD;
int playerKey = -1;

typedef struct arenaData
{
	TArenaID id; // > 0
} *ArenaD;
int arenaKey = -1;



inline PlayerD GetPlayerData(Player *p)
{
        struct playerData *pdata;
        pdata = (playerData *) PPDATA(p, playerKey);
        return pdata;
}
inline ArenaD GetArenaData(Arena *arena)
{
	struct arenaData *adata;
	adata = (arenaData *) P_ARENA_DATA(arena, arenaKey);
	return adata;
}

void logman_backtrace()
{
	void *array[10];
	size_t size;
	char **strings;
	size_t i;
	
	size = backtrace (array, 10);
	strings = backtrace_symbols (array, size);
	
	for (i = 0; i < size; i++)
	{
		lm->Log(L_ERROR, "<thrift_core> %s", strings[i]);
	}
}


TTime milliTime()
{
	timeval now;
	TTime ret;
        gettimeofday(&now, NULL);
        ret = now.tv_sec;
        ret += now.tv_usec / 1000; // integer division
        return ret; // positive integer as a double
}

ticks_t TTime_to_ticks(TTime tTime)
{
	ticks_t now;
	TTime tNow;
	long diff;
	
	now = current_ticks();
	tNow = milliTime();
	
	diff = (long) ceil((tNow - tTime) / 10.0);
	return TICK_MAKE(now - diff);
}

TTime ticks_to_TTime(ticks_t time)
{
	ticks_t now;
	TTime tNow;
	
	now = current_ticks();
	tNow = milliTime();
	
	return tNow - (now - time) * 10;
}



void throwUnknownPlayer()
{
	TGeneralException generalEx;
	generalEx.__set_reason(TGeneralExceptionReason::UnknownPlayer);
	generalEx.__set_message("Unknown TPlayerID specified");
	throw generalEx;
}

void throwUnknownArena()
{
	TGeneralException generalEx;
	generalEx.__set_reason(TGeneralExceptionReason::UnknownArena);
	generalEx.__set_message("Unknown TArenaID specified");
	throw generalEx;
}

void throwNotImplemented()
{
	TGeneralException generalEx;
	generalEx.__set_reason(TGeneralExceptionReason::NotImplemented);
	generalEx.__set_message("This function has not been implemented yet");
	throw generalEx;
}

void throwMissingModule(const char* module)
{
	TGeneralException generalEx;
	std::string message("An interface is missing: ");
	message += module;
	generalEx.__set_reason(TGeneralExceptionReason::MissingModule);
	generalEx.__set_message(message);
	throw generalEx;
}

void throwInvalidArgument(const std::string& message)
{
	TGeneralException generalEx;
	generalEx.__set_reason(TGeneralExceptionReason::InvalidArgument);
	generalEx.__set_message(message);
	throw generalEx;
}

Player * TPlayerID_to_Player(TPlayerID id)
{
	Player *ret;

	pthread_mutex_lock(&known_mtx);
	if (!knownPlayers.count(id))
	{
		pthread_mutex_unlock(&known_mtx);
		throwUnknownPlayer();
	}
	
	ret = knownPlayers[id];
	pthread_mutex_unlock(&known_mtx);
	return ret;
}

Arena * TArenaID_to_Arena(TArenaID id)
{
	Arena *arena;
	pthread_mutex_lock(&known_mtx);
	if (!knownArenas.count(id))
	{
		pthread_mutex_unlock(&known_mtx);
		throwUnknownArena();
	}
	
	arena = knownArenas[id];
	pthread_mutex_unlock(&known_mtx);
	return arena;
}

char TLogLevel_to_level(const TLogLevel::type level)
{
	switch (level)
	{
		case TLogLevel::DRIVEL:
			return L_DRIVEL;
		case TLogLevel::INFO:
			return L_INFO;
		case TLogLevel::MALICIOUS:
			return L_MALICIOUS;
		case TLogLevel::WARN:
			return L_WARN;
		case TLogLevel::ERROR:
			return L_ERROR;
	}
	return 0;
}

void Target_to_TTarget(const Target *tgt, TTarget *tTarget)
{
	PlayerD pdata;
	ArenaD adata;
	Link *link;
	void *cst;
	
	assertlm(tgt);
	assertlm(tTarget);
	
	switch(tgt->type)
	{
		case Target::T_NONE:
			tTarget->__set_type(TTargetType::NONE);
		break;
		case Target::T_PLAYER:
			tTarget->__set_type(TTargetType::PLAYER);
			pdata = GetPlayerData(tgt->u.p);
			tTarget->__set_player(pdata->id);
		break;
		case Target::T_ARENA:
			tTarget->__set_type(TTargetType::ARENA);
			adata = GetArenaData(tgt->u.arena);
			tTarget->__set_arena(adata->id);
		break;
		case Target::T_FREQ:
			tTarget->__set_type(TTargetType::FREQ);
			adata = GetArenaData(tgt->u.freq.arena);
			tTarget->__set_arena(adata->id);
			tTarget->__set_freq(tgt->u.freq.freq);
		break;
		case Target::T_ZONE:
			tTarget->__set_type(TTargetType::ZONE);
		break;
		case Target::T_LIST:
			tTarget->__set_type(TTargetType::LIST);
			tTarget->__isset.players = true;
			FOR_EACH(&tgt->u.list, cst, link)
			{
				pdata = GetPlayerData((Player*) cst);
				tTarget->players.push_back(pdata->id);
			}	
		break;
	}
}

void TTarget_to_Target(const TTarget *tTarget, Target *tgt) // throws UnknownPlayer / UnknownArena
{
	std::vector<TPlayerID>::const_iterator it;
	switch (tTarget->type)
	{
		case TTargetType::NONE:
			tgt->type = Target::T_NONE;
		break;
		case TTargetType::PLAYER:
			tgt->type = Target::T_PLAYER;
			tgt->u.p = TPlayerID_to_Player(tTarget->player);
		break;
		case TTargetType::ARENA:
			tgt->type = Target::T_ARENA;
			tgt->u.arena = TArenaID_to_Arena(tTarget->arena);
		break;
		case TTargetType::FREQ:
			tgt->type = Target::T_FREQ;
			tgt->u.freq.arena = TArenaID_to_Arena(tTarget->arena);
			tgt->u.freq.freq = tTarget->freq;
		break;
		case TTargetType::ZONE:
			tgt->type = Target::T_ZONE;
		break;
		case TTargetType::LIST:
			tgt->type = Target::T_LIST;
			for (it = tTarget->players.begin(); it != tTarget->players.end(); it++)
			{
				LLAdd(&tgt->u.list, (void*) TPlayerID_to_Player(*it));
			}
		break;
	}
}

TPoint createPoint(int16_t x, int16_t y)
{
	TPoint p;
	p.__set_x(x);
	p.__set_y(y);
	return p;
}



Client::Client(TConnection *connection_) 
{
	pthread_mutex_init(&queuedEvents_mtx, NULL);
	pthread_mutex_init(&myFakePlayers_mtx, NULL);
	
	authenticated = false;
	connection = connection_;
	
	lm->Log(L_INFO, "<thrift_core> Connection established: %s", connection->getTSocket()->getSocketInfo().c_str());
	lastPoll = current_ticks();
}

Client::~Client()
{
	pthread_mutex_destroy(&queuedEvents_mtx);
	pthread_mutex_destroy(&myFakePlayers_mtx);
}

void Client::connectionClosed(bool pollTimeout, int debug_line)
{
	std::list<Player*>::iterator it;
	Arena *arena;
	std::map<TArenaID, std::map<std::string /*command*/, std::map<const Client*, std::string /*helptext*/> > >::iterator iter;
	                   std::map<std::string /*command*/, std::map<const Client*, std::string /*helptext*/> >  ::iterator iter2;

	if (pollTimeout)
	{
		lm->Log(L_ERROR, "<thrift_core> Last poll() was over 30 seconds ago, dropping the connection: %s", connection->getTSocket()->getSocketInfo().c_str());
	}
	else
	{
		lm->Log(L_INFO, "<thrift_core> Connection dropped (line:%d): %s", debug_line, connection->getTSocket()->getSocketInfo().c_str());
	}
	
	connection = NULL;
	
	if (fake)
	{
		lockMyFakePlayers();
		
		for (it = myFakePlayers.begin(); it != myFakePlayers.end(); it++)
		{
			fake->EndFaked(*it);
		}
		myFakePlayers.clear();
		
		unlockMyFakePlayers();
	}
	
	pthread_mutex_lock(&commands_mtx);
	for (iter = commands.begin();
	     iter != commands.end();
	     iter++)
	{
		// first = arena
		try
		{
			arena = iter->first ? TArenaID_to_Arena(iter->first) : NULL;
		}
		catch(TGeneralException &ex) // arena was just removed and we are experiencing some bad timing
		{
			commands.erase(iter->first);
			continue;
		}
		
		for (iter2 =  iter->second.begin();
		     iter2 != iter->second.end();
		     iter2++)
		{
			// first = command name
			
			// second = map<const Client*, helptext>
			if (iter2->second.count(this)) // we have registered this command
			{
				cmd->RemoveCommand(iter2->first.c_str(), myCommand, arena);
				iter2->second.erase(this);
			}
		}
	}
	pthread_mutex_unlock(&commands_mtx);
};

void inline Client::lockQueuedEvents() { pthread_mutex_lock(&queuedEvents_mtx); }
void inline Client::unlockQueuedEvents() { pthread_mutex_unlock(&queuedEvents_mtx); }

void inline Client::lockMyFakePlayers() { pthread_mutex_lock(&myFakePlayers_mtx); }
void inline Client::unlockMyFakePlayers() { pthread_mutex_unlock(&myFakePlayers_mtx); }


void Client::throwIfNotAuthenticated()
{
	TGeneralException generalEx;
	
	if (!authenticated)
	{
		generalEx.__set_reason(TGeneralExceptionReason::AuthenticationRequired);
		throw generalEx;
	}
}

void Client::authenticate(const std::string& modulename_, const std::string& password)
{
	unsigned a; 
	unsigned checked;
	bool failed;
	const char *pw;
	const char *ch;
	Link *link;
	void *cst;
	Player *p;
	Arena *arena;
	PlayerD pdata;
	ArenaD adata;
	shared_ptr<TPoll> event;
	std::string str;

	pw = cfg->GetStr(GLOBAL, "ThriftClients", modulename_.c_str());
	
	// check manually to prevent time attacks
	failed = false;
	checked = 0;
	do
	{
		for (ch = pw, a = 0; 
		     *ch; 
		     ch++, a++, checked++
		)
		{
			if (a < password.size())
			{
				if (password[a] != *ch)
				{
					failed = true;
				}
			}
			else
			{
				failed = true;
			}
		}
		if (a != password.size())
		{
			failed = true;
		}
		
		
		if (!checked)
		{
			break;
		}
	} while (checked < 50);
	
	authenticated = !failed;
	
	if (failed)
	{
		TAuthenticateFailed authEx;
		lm->Log(L_INFO, "<thrift_core> <%s> Authentication failed!: %s", modulename_.c_str(), connection->getTSocket()->getSocketInfo().c_str());
		throw authEx;
	}
	else
	{
		lm->Log(L_INFO, "<thrift_core> <%s> Authenticated: %s", modulename_.c_str(), connection->getTSocket()->getSocketInfo().c_str());
		
		modulename = modulename_;
		
		aman->Lock();
		lockQueuedEvents();
		FOR_EACH_ARENA(cst)
		{
			arena = (Arena *) cst;
			adata = GetArenaData(arena);
			
			event = shared_ptr<TPoll>(new TPoll());
			event->__isset.ArenaChanged = true;
			event->ArenaChanged.__set_reason(TArenaChangeReason::INITIAL);
			
			event->ArenaChanged.arena.__set_id(adata->id);
			str.assign(arena->name);
			event->ArenaChanged.arena.__set_name(str);
			str.assign(arena->basename);
			event->ArenaChanged.arena.__set_basename(str);
			
			queuedEvents.push(event);
		}
		unlockQueuedEvents();
		aman->Unlock();
		
		
		
		pd->Lock();
		lockQueuedEvents();
	        FOR_EACH_PLAYER(cst)
	        {
	        	p = (Player *) cst;
	        	pdata = GetPlayerData(p);
	        	adata = p->arena ? GetArenaData(p->arena) : NULL;
	        	
	        	event = shared_ptr<TPoll>(new TPoll());
			event->__isset.PlayerChanged = true;
			event->PlayerChanged.__set_reason(TPlayerChangeReason::INITIAL);
	        	
	        	event->PlayerChanged.player.__set_id(pdata->id);
	        	str.assign(p->name);
			event->PlayerChanged.player.__set_name(str);
			str.assign(p->squad);
			event->PlayerChanged.player.__set_squad(str);
			event->PlayerChanged.player.__set_type((TClientType::type) p->type); // enum in thrift is defined so that it has the same values
			event->PlayerChanged.player.__set_arena(adata ? adata->id : 0);
			if (adata)
			{
				event->PlayerChanged.player.__set_arena(adata->id);
			}
			else
			{
				event->PlayerChanged.player.__isset.arena = false;
			}
			
			event->PlayerChanged.player.__set_freq(p->p_freq);
			event->PlayerChanged.player.__set_ship((TShip::type) p->p_ship);
			if (p->xres || p->yres)
			{
				event->PlayerChanged.player.__set_resolution(createPoint(p->xres, p->yres));
			}
			else
			{
				event->PlayerChanged.player.__isset.resolution = false;
			}
			
			queuedEvents.push(event);
	        }
	        unlockQueuedEvents();
		pd->Unlock();
		
	}
}

void Client::poll(std::vector<TPoll> & _return) 
{
	shared_ptr<TPoll> event;
	int a;

	throwIfNotAuthenticated();
	lastPoll = current_ticks(); // a client that does not authenticate will disconnect after 30 seconds because the authenticate check is done before updating the timestamp
	
	a = 0;
	lockQueuedEvents();
	while (!queuedEvents.empty() &&
	       a < 200) // tweak this later
	{
		event = queuedEvents.front();
		_return.push_back(*event);
		queuedEvents.pop();
		a++;
	}
	unlockQueuedEvents();
}

void Client::sendPositionData(const TArenaID arena, const bool send)
{
	throwIfNotAuthenticated();
	lockQueuedEvents();
	sendPositionEvents[arena] = send;
	unlockQueuedEvents();
}

bool Client::recycleArena(const TArenaID arena) 
{
	throwIfNotAuthenticated();
	return aman->RecycleArena(TArenaID_to_Arena(arena));
}



void Client::addCommand(const std::string& command, const TArenaID tArena, const std::string& helptext)
{
	Arena *arena;
	const char *help;
	throwIfNotAuthenticated();
	
	arena = tArena ? TArenaID_to_Arena(tArena) : NULL;
	
	// arena = 0 is zone wide
	
	pthread_mutex_lock(&commands_mtx);
	if (commands[tArena][command].count(this))
	{
		pthread_mutex_unlock(&commands_mtx);
		throwInvalidArgument("The command has already been registered by your client");
	}
	commands[tArena][command][this] = helptext;
	help = commands[tArena][command][this].c_str(); // cmdman does not copy the helptext, so store it here
	pthread_mutex_unlock(&commands_mtx);
	
	cmd->AddCommand(command.c_str(), myCommand, arena, help);
}

void Client::removeCommand(const std::string& command, const TArenaID tArena)
{
	Arena *arena;
	throwIfNotAuthenticated();
	
	arena = tArena ? TArenaID_to_Arena(tArena) : NULL;
	
	cmd->RemoveCommand(command.c_str(), myCommand, arena);
	
	pthread_mutex_lock(&commands_mtx);
	commands[tArena][command].erase(this);
	pthread_mutex_unlock(&commands_mtx);
}

void Client::dropBrick(const TArenaID arena, const TFreqID freq, const TPoint& from, const TPoint& to)
{
	throwIfNotAuthenticated();
	if (!bricks)
	{
		throwMissingModule("I_BRICKS");
	}
	
	bricks->DropBrick(TArenaID_to_Arena(arena), 
	                  SANE(freq, 0, FREQ_MAX), 
			  SANE(from.x, 0, TILE_MAX),
			  SANE(from.y, 0, TILE_MAX), 
			  SANE(to.x, 0, TILE_MAX), 
			  SANE(to.y, 0, TILE_MAX));
}

void Client::sendChatMessage(const TTarget& tTarget, const TChatType::type type, const std::string& message, const TPlayerID from, const int8_t sound)
{
	Target tgt;
	LinkedList players = LL_INITIALIZER;
	
	throwIfNotAuthenticated();
	TTarget_to_Target(&tTarget, &tgt);
	
	pd->Lock();
	pd->TargetToSet(&tgt, &players);
	pd->Unlock();
	chat->SendAnyMessage(&players, (char) type, sound, from ? TPlayerID_to_Player(from) : NULL, "%s", message.c_str());
	LLEmpty(&players);
}

TPlayerID Client::createFakePlayer(const std::string& name, const TArenaID arena, const TShip::type ship, const TFreqID freq)
{
	Player *p;
	PlayerD pdata;
	
	throwIfNotAuthenticated();
	if (!fake)
	{
		throwMissingModule("I_FAKE");
	}
	
	p = fake->CreateFakePlayer(name.c_str(), TArenaID_to_Arena(arena), (int) SANE(ship, 0, SHIP_MAX), (int) SANE(freq, 0, FREQ_MAX)); // fires CB_NEWPLAYER
	pdata = GetPlayerData(p);
	lockMyFakePlayers();
	myFakePlayers.push_back(p);
	unlockMyFakePlayers();
	return pdata->id;
}

void Client::endFakePlayer(const TPlayerID player)
{
	Player *p;
	std::list<Player*>::iterator it;
	bool found;
	
	throwIfNotAuthenticated();
	if (!fake)
	{
		throwMissingModule("I_FAKE");
	}
	
	p = TPlayerID_to_Player(player);
	if (p->type != T_FAKE)
	{
		throwUnknownPlayer();
	}
	
	lockMyFakePlayers();
	
	found = false;
	for (it = myFakePlayers.begin(); it != myFakePlayers.end(); it++)
	{
		if (p == *it)
		{
			found = true;
			break;
		}
	}
	myFakePlayers.clear();
	
	unlockMyFakePlayers();
	
	if (found)
	{	
		fake->EndFaked(p); // CB_NEWPLAYER will remove it from myFakePlayers
	}
	else // The fake player is not owned by this module
	{
		throwUnknownPlayer();
	}
}

void Client::setShipFreq(const TPlayerID player, const TShip::type ship_, const TFreqID freq_)
{
	int ship;
	int freq;
	throwIfNotAuthenticated();
	ship = (int) SANE(ship_, -1, SHIP_MAX);
	freq = (int) SANE(freq_, -1, FREQ_MAX);
	
	if (freq >= 0)
	{
		if (ship >= 0)
		{
			game->SetShipAndFreq(TPlayerID_to_Player(player), ship,  freq);
		}
		else
		{
			game->SetFreq(TPlayerID_to_Player(player),  freq);
		}
	}
	else if (ship >= 0)
	{
		game->SetShip(TPlayerID_to_Player(player), ship);
	}
}

void Client::warpTo(const TTarget& tTarget, const TPoint& tile)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	
	TTarget_to_Target(&tTarget, &tgt);
	game->WarpTo(&tgt, SANE(tile.x, 0, TILE_MAX), SANE(tile.y, 0, TILE_MAX));
}

void Client::sendToArena(const TPlayerID player, const std::string& arenaname, const TPoint& spawn)
{
	throwIfNotAuthenticated();
	aman->SendToArena(TPlayerID_to_Player(player), arenaname.c_str(), SANE(spawn.x, 0, TILE_MAX), SANE(spawn.y, 0, TILE_MAX));
}

void Client::givePrize(const TTarget& tTarget, const int16_t type, const int16_t count)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	
	TTarget_to_Target(&tTarget, &tgt);
	game->GivePrize(&tgt, type, count);
}

void Client::lockPlayers(const TTarget& tTarget, const bool notify, const bool spec, const int16_t timeout)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	
	TTarget_to_Target(&tTarget, &tgt);
	game->Lock(&tgt, notify, spec, timeout);
}

void Client::unlockPlayers(const TTarget& tTarget, const bool notify)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	
	TTarget_to_Target(&tTarget, &tgt);
	game->Unlock(&tgt, notify);
}

void Client::lockArena(const TArenaID arena, const bool notify, const bool onlyarenastate, const bool initial, const bool spec)
{
	throwIfNotAuthenticated();
	game->LockArena(TArenaID_to_Arena(arena), notify, onlyarenastate, initial, spec);
}

void Client::unlockArena(const TArenaID arena, const bool notify, const bool onlyarenastate)
{
	throwIfNotAuthenticated();
	game->UnlockArena(TArenaID_to_Arena(arena), notify, onlyarenastate);
}

void Client::fakePosition(const TPlayerID player, const TPosition& position)
{
	struct C2SPosition pos;
	throwIfNotAuthenticated();
	
	memset(&pos, 0, sizeof(pos));

	pos.type = C2S_POSITION;
	pos.rotation = SANE(position.rotation, 0, 39);
	pos.time = position.time ? TTime_to_ticks(position.time) : current_ticks();
	pos.xspeed = SANE(position.speed.x, i16_MIN, i16_MAX);
	pos.y = SANE(position.position.y, POS_MIN, POS_MAX);
	
	pos.status = 0;
	
	if (position.status.stealth ) { pos.status |= STATUS_STEALTH; }
	if (position.status.cloak   ) { pos.status |= STATUS_CLOAK; }
	if (position.status.xradar  ) { pos.status |= STATUS_XRADAR; }
	if (position.status.antiwarp) { pos.status |= STATUS_ANTIWARP; }
	if (position.status.flash   ) { pos.status |= STATUS_FLASH; }
	if (position.status.safezone) { pos.status |= STATUS_SAFEZONE; }
	if (position.status.ufo     ) { pos.status |= STATUS_UFO; }
	if (position.status.unknown ) { pos.status |= 0x80U; }
	
	pos.x = SANE(position.position.x, POS_MIN, POS_MAX);
	pos.yspeed = SANE(position.speed.y, i16_MIN, i16_MAX);
	pos.bounty = SANE(position.bounty, 0, u16_MAX);
	pos.extra.energy = pos.energy = SANE(position.energy, 0, i16_MAX);
	pos.weapon.type = W_NULL;
	if (position.__isset.weapon)
	{
		pos.weapon.type = SANE(position.weapon.type, 0, 8);
		pos.weapon.level = SANE(position.weapon.level, 0, u2_MAX);
		pos.weapon.shrapbouncing = !!position.weapon.shrapbouncing;
		pos.weapon.shraplevel = SANE(position.weapon.shraplevel, 0, u2_MAX);
		pos.weapon.shrap = SANE(position.weapon.shrap, 0, u5_MAX);
		pos.weapon.alternate = !!position.weapon.alternate; 
	}
	
	if (position.__isset.extra)
	{
		pos.extra.s2cping = SANE(position.extra.s2cping, 0, u16_MAX); 
		pos.extra.timer   = SANE(position.extra.timer, 0, u16_MAX);   
		pos.extra.shields = !!position.extra.shields;
		pos.extra.super   = !!position.extra.tempSuper;
		pos.extra.bursts  = SANE(position.extra.bursts, 0, u4_MAX);
		pos.extra.repels  = SANE(position.extra.repels, 0, u4_MAX);
		pos.extra.thors   = SANE(position.extra.thors, 0, u4_MAX);
		pos.extra.bricks  = SANE(position.extra.bricks, 0, u4_MAX);
		pos.extra.decoys  = SANE(position.extra.decoys, 0, u4_MAX);  
		pos.extra.rockets = SANE(position.extra.rockets, 0, u4_MAX);
		pos.extra.portals = SANE(position.extra.portals, 0, u4_MAX);
	}
	
	game->FakePosition(TPlayerID_to_Player(player), &pos, sizeof(pos));
}

void Client::fakeKill(const TPlayerID killer, const TPlayerID killed, const int16_t pts)
{
	throwIfNotAuthenticated();
	game->FakeKill(TPlayerID_to_Player(killer), TPlayerID_to_Player(killed), pts, 0);
}

void Client::shipReset(const TTarget& tTarget)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	
	TTarget_to_Target(&tTarget, &tgt);
	game->ShipReset(&tgt);
}

void Client::setPlayerEnergyViewing(const TPlayerID player, const int16_t value)
{
	throwIfNotAuthenticated();
	game->SetPlayerEnergyViewing(TPlayerID_to_Player(player), value);
}

void Client::setSpectatorEnergyViewing(const TPlayerID player, const int16_t value)
{
	throwIfNotAuthenticated();
	game->SetSpectatorEnergyViewing(TPlayerID_to_Player(player), value);
}

void Client::resetPlayerEnergyViewing(const TPlayerID player)
{
	throwIfNotAuthenticated();
	game->ResetPlayerEnergyViewing(TPlayerID_to_Player(player));
}

void Client::resetSpectatorEnergyViewing(const TPlayerID player)
{
	throwIfNotAuthenticated();
	game->ResetSpectatorEnergyViewing(TPlayerID_to_Player(player));
}

void Client::log(const TLogLevel::type tlevel, const std::string& line) 
{
	char level;
	
	throwIfNotAuthenticated();
	
	level = TLogLevel_to_level(tlevel);
	if (!level)
	{
		throwInvalidArgument("Unknown log level");
		return;
	}

	lm->Log(level, "%s", line.c_str());
}

void Client::logA(const TLogLevel::type tlevel, const std::string& mod, const TArenaID arena, const std::string& line)
{
	char level;
	
	throwIfNotAuthenticated();
	
	level = TLogLevel_to_level(tlevel);
	if (!level)
	{
		throwInvalidArgument("Unknown log level");
		return;
	}
	
	lm->LogA(level, mod.c_str(), TArenaID_to_Arena(arena), "%s", line.c_str());
}

void Client::logP(const TLogLevel::type tlevel, const std::string& mod, const TPlayerID player, const std::string& line)
{
	char level;
	
	throwIfNotAuthenticated();
	
	level = TLogLevel_to_level(tlevel);
	if (!level)
	{
		throwInvalidArgument("Unknown log level");
		return;
	}
	
	lm->LogP(level, mod.c_str(), TPlayerID_to_Player(player), "%s", line.c_str());
}

void Client::getMapChecksum(TMapName& _return, const TArenaID arena)
{
	throwIfNotAuthenticated();
	throwNotImplemented();
}

bool Client::getMapData(const TArenaID arena)
{
	throwIfNotAuthenticated();
	throwNotImplemented();
	#warning "Todo: mapdata"
	return false;
}

void Client::statIncrement(const TPlayerID player, const int16_t stat, const int16_t amount)
{
	throwIfNotAuthenticated();
	if (!stats)
	{
		throwMissingModule("I_STATS");
	}
	
	stats->IncrementStat(TPlayerID_to_Player(player), stat, amount);
}

void Client::statStartTimer(const TPlayerID player, const int16_t stat)
{
	throwIfNotAuthenticated();
	if (!stats)
	{
		throwMissingModule("I_STATS");
	}
	
	stats->StartTimer(TPlayerID_to_Player(player), stat);
}

void Client::statStopTimer(const TPlayerID player, const int16_t stat)
{
	throwIfNotAuthenticated();
	if (!stats)
	{
		throwMissingModule("I_STATS");
	}
	
	stats->StopTimer(TPlayerID_to_Player(player), stat);
}

void Client::statSet(const TPlayerID player, const int16_t stat, const int16_t interval, const int16_t value)
{
	throwIfNotAuthenticated();
	if (!stats)
	{
		throwMissingModule("I_STATS");
	}
	
	stats->SetStat(TPlayerID_to_Player(player), stat, interval, value);
}

int16_t Client::statGet(const TPlayerID player, const int16_t stat, const int16_t interval) 
{
	throwIfNotAuthenticated();
	if (!stats)
	{
		throwMissingModule("I_STATS");
	}
	
	return stats->GetStat(TPlayerID_to_Player(player), stat, interval);
}

void Client::statSendUpdates(const TPlayerID exclude)
{
	throwIfNotAuthenticated();
	if (!stats)
	{
		throwMissingModule("I_STATS");
	}
	
	stats->SendUpdates(TPlayerID_to_Player(exclude));
}

void Client::statScoreReset(const TPlayerID player, const int16_t interval)
{
	throwIfNotAuthenticated();
	if (!stats)
	{
		throwMissingModule("I_STATS");
	}
	
	stats->ScoreReset(TPlayerID_to_Player(player), interval);
}

void Client::getConfig(std::vector<std::string> & _return, const TArenaID tArena, const std::vector<std::string> & keys)
{
	std::vector<std::string>::const_iterator it;
	const char * val;
	ConfigHandle ch;
	std::string str;
	
	throwIfNotAuthenticated();
	
	if (!tArena)
	{
		throwInvalidArgument("Reading the global configuration is not allowed");
	}
	
	ch = TArenaID_to_Arena(tArena)->cfg;
	if (ch == GLOBAL)
	{
		throwInvalidArgument("Reading the global configuration is not allowed");
	}
	
	for (it = keys.begin(); it != keys.end(); it++)
	{
		val = cfg->GetStr(ch, it->c_str(), NULL);
		if (val)
		{
			str.assign(val);
		}
		else
		{
			str.assign("");
		}
		_return.push_back(str);
	}
}

void Client::lvzToggle(const TTarget& tTarget, const std::map<TObjectID, bool> & tObjects)
{
	Target tgt;
	std::map<TObjectID, bool>::const_iterator it;
	byte *pkt;
	int objectsLeft;
	int i;
	int pktlen;
	struct ToggledObject *obj;
	const int REL_HEADER = 6;
	const int MAX_OBJS_SIZE = (MAXPACKET - REL_HEADER - 1);
	const int MAX_OBJS = MAX_OBJS_SIZE / sizeof(struct ToggledObject);
	
	throwIfNotAuthenticated();
	
	TTarget_to_Target(&tTarget, &tgt);
	
	objectsLeft = tObjects.size();
	
	pkt = NULL;
	
	for (it = tObjects.begin(); 
	     it != tObjects.end(); 
	     it++)
	{
		if (!pkt)
		{
			i = 1;
			if (objectsLeft > MAX_OBJS)
			{
				pktlen = 1 + MAX_OBJS_SIZE;
			}
			else
			{
				pktlen = 1 + sizeof(struct ToggledObject)*objectsLeft;
			}
			
			pkt = (byte*) alloca(pktlen);
			pkt[0] = S2C_TOGGLEOBJ;
		}
		obj = (struct ToggledObject*) pkt[i];
		
		obj->id = it->first;
		obj->off = !it->second;
		objectsLeft--;
		i += sizeof(struct ToggledObject);
		
		if (i >= MAX_OBJS_SIZE)
		{
			net->SendToTarget(&tgt, pkt, pktlen, NET_RELIABLE);
			pkt = NULL;
		}
	}
	
	if (pkt)
	{
		net->SendToTarget(&tgt, pkt, pktlen, NET_RELIABLE);
		pkt = NULL;
	}
	
	
}

void Client::lvzAttr(const TTarget& tTarget, const TObjectID id, const TLVZPosition& position, const int16_t image, const TLVZLayer::type layer, const int16_t displayTime, const TLVZMode::type mode)
{
	struct ObjectMove objMove;
	throwIfNotAuthenticated();
	
	memset(&objMove, 0, sizeof(objMove));
	
	objMove.data.id = id;
	
	if (position.position.x >= 0 && position.position.y > 0)
	{
		objMove.change_xy = 1;
		if (position.mapObject)
		{
			objMove.data.mapobj = 1; // todo: is this only needed when change_xy is set? Or must it always be set? Or is it completely ignored?
			objMove.data.map_x = position.position.x;
			objMove.data.map_y = position.position.y;
		}
		else
		{
			if (!position.__isset.offset)
			{
				throwInvalidArgument("position.offset must be set for screen objects");
			}
			objMove.data.mapobj = 0;
			objMove.data.rela_x = position.offset.x;
			objMove.data.scrn_x = position.position.x;
			objMove.data.rela_y = position.offset.y;
			objMove.data.scrn_y = position.position.y;
		}
	}
	
	if (image >= 0)
	{
		objMove.change_image = 1;
		objMove.data.image = image;
	}
	
	if (layer >= 0)
	{
		objMove.change_layer = 1;
		objMove.data.layer = layer;
	}
	
	if (displayTime >= 0)
	{
		objMove.change_time = 1;
		objMove.data.time = displayTime;
	}
	
	if (mode >= 0)
	{
		objMove.change_mode = 1;
		objMove.data.mode = mode;
	}
}

void Client::lvzTogglePersistent(const TArenaID arena, const std::map<TObjectID, bool> & tObjects)
{
	Target tgt;
	unsigned size;
	int i;
	short *id;
	char *ons;
	std::map<TObjectID, bool>::const_iterator it;
	
	throwIfNotAuthenticated();
	if (!objects)
	{
		throwMissingModule("I_OBJECTS");
	}
	
	
	tgt.type = Target::T_ARENA;
	tgt.u.arena = TArenaID_to_Arena(arena);
	
	size = tObjects.size();
	if (!size)
	{
		return;
	}
	
	id = new short[size];
	ons = new char[size];
	
	for (i = 0, it = tObjects.begin(); 
	     it != tObjects.end(); 
	     it++, i++)
	{
		id[i] = it->first;
		ons[i] = it->second;
	}
	
	objects->ToggleSet(&tgt, id, ons, size);
}

void Client::lvzMovePersistent(const TArenaID arena, const TObjectID id, const TPoint& position, const TLVZScreenOffset& offset)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	if (!objects)
	{
		throwMissingModule("I_OBJECTS");
	}
	
	tgt.type = Target::T_ARENA;
	tgt.u.arena = TArenaID_to_Arena(arena);
	
	objects->Move(&tgt, id, position.x, position.y, offset.x, offset.y);
}

void Client::lvzImagePersistent(const TArenaID arena, const TObjectID id, const int16_t image)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	if (!objects)
	{
		throwMissingModule("I_OBJECTS");
	}
	
	tgt.type = Target::T_ARENA;
	tgt.u.arena = TArenaID_to_Arena(arena);
	
	objects->Image(&tgt, id, image);
}

void Client::lvzLayerPersistent(const TArenaID arena, const TObjectID id, const TLVZLayer::type layer)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	if (!objects)
	{
		throwMissingModule("I_OBJECTS");
	}
	
	tgt.type = Target::T_ARENA;
	tgt.u.arena = TArenaID_to_Arena(arena);
	
	objects->Layer(&tgt, id, layer);
}

void Client::lvzTimerPersistent(const TArenaID arena, const TObjectID id, const int16_t time)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	if (!objects)
	{
		throwMissingModule("I_OBJECTS");
	}
	
	tgt.type = Target::T_ARENA;
	tgt.u.arena = TArenaID_to_Arena(arena);
	
	objects->Timer(&tgt, id, time);
}

void Client::lvzModePersistent(const TArenaID arena, const TObjectID id, const TLVZMode::type mode)
{
	Target tgt;
	
	throwIfNotAuthenticated();
	if (!objects)
	{
		throwMissingModule("I_OBJECTS");
	}
	
	tgt.type = Target::T_ARENA;
	tgt.u.arena = TArenaID_to_Arena(arena);
	
	objects->Timer(&tgt, id, mode);
}

class MyTASSSProcessor : virtual public TASSSProcessor 
{
	public:
	MyTASSSProcessor(boost::shared_ptr<TASSSIf> iface) :
		TASSSProcessor(iface)
	{
	}
	
	virtual bool process(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> piprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> poprot, void* callContext)
	{
		try
		{
			return TASSSProcessor::process(piprot, poprot, callContext);
		}
		catch (TTransportException &ttx)
		{
			lm->Log(L_ERROR, "<thrift_core> TProcessor::process(): TTransportException: %s", ttx.what());
			throw;
		}
		catch (TException &x)
		{
			lm->Log(L_ERROR, "<thrift_core> TProcessor::process(): TException: %s", x.what());
			throw;
		}
		catch (...)
		{
			lm->Log(L_ERROR, "<thrift_core> TProcessor::process(): Unknown exception");
			throw;
		}
	}
};

class ClientManager : public TProcessorFactory
{
	public:
	std::map<TConnection*, shared_ptr<Client> > clients;
	pthread_mutex_t clients_mtx;
	
	ClientManager()
	{
		pthread_mutex_init(&clients_mtx, NULL);
	}
	
	~ClientManager()
	{
		pthread_mutex_destroy(&clients_mtx);
	}
	
	void inline lockClients()
	{
		pthread_mutex_lock(&clients_mtx);
	}
	void inline unlockClients()
	{
		pthread_mutex_unlock(&clients_mtx);
	}
	
	
	shared_ptr<TProcessor> createProcessor(TConnection *connection)
	{
		shared_ptr<Client> client(new Client(connection));
		shared_ptr<TProcessor> processor(new MyTASSSProcessor(client));
		
		lockClients();
		clients[connection] = client;
		unlockClients();
		return processor;
	}
	
	void notifyClose(shared_ptr<TProcessor> processor, TConnection *connection, int debug_line)
	{
		shared_ptr<Client> client;
		
		lockClients();
		client = clients[connection];
		if (client) // shared_ptr has a bool operator
		{
			client->connectionClosed(false, debug_line);
		}
		
		clients.erase(connection);
		unlockClients();
	}
	
	void tick()
	{
		std::map<TConnection*, shared_ptr<Client> >::const_iterator iter;
		shared_ptr<Client> client;
		ticks_t now;
		
		now = current_ticks();
		lockClients();
		
		for (iter = clients.begin();
		     iter != clients.end();
		     iter++ )
		{
			client = iter->second; 
			
			if (TICK_DIFF(now, client->lastPoll) > 3000)
			{
				client->connectionClosed(true, -123);
				client->connection->close(false, -123);
				clients.erase(iter->first);
			}
		}
		
		unlockClients();
	}
	
	bool hasClients()
	{
		bool ret;
		lockClients();
		ret = !clients.empty();
		unlockClients();
		return ret;
	}
	
	// if isPosition is set, the client must receive position packets for that arena
	void fireEvent(shared_ptr<TPoll> event, TArenaID isPosition) 
	{
		std::map<TConnection*, shared_ptr<Client> >::const_iterator iter;
		shared_ptr<Client> client;
		
		lockClients();
		
		for (iter = clients.begin();
		     iter != clients.end();
		     iter++ )
		{
			client = iter->second;
			if (!client->authenticated)
			{
				continue;
			}
			
			client->lockQueuedEvents();
			if (!isPosition || client->sendPositionEvents[isPosition])
			{
				client->queuedEvents.push(event);
			}
			client->unlockQueuedEvents();
		}
	
		unlockClients();
	} 
};

void CoreRunner_tickTimer(int fd, short ev, void* arg);
class CoreRunner : public Runnable
{
	private:
		int port;
		struct event tickEvent;
		timeval tickEventTimeval;
	public:
		shared_ptr<TNonblockingServer> thriftServer;
		shared_ptr<ClientManager> clientManager;
		
		CoreRunner(int port_)
		{
			port = port_;
		}
		
		~CoreRunner()
		{
		}
	
	void run()
	{
		
		// using a thread pool with maximum 4 threads to handle incoming requests
		//shared_ptr<ThreadManager> threadManager = ThreadManager::newSimpleThreadManager(4);
		//threadManager->threadFactory(threadFactory);
		//threadManager->start();
		
		clientManager = shared_ptr<ClientManager>(new ClientManager());
		shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
		
		thriftServer = shared_ptr<TNonblockingServer>(new TNonblockingServer(clientManager, port));
		thriftServer->setInputProtocolFactory(protocolFactory);
    		thriftServer->setOutputProtocolFactory(protocolFactory);
    		//thriftServer.setThreadManager(threadManager);
    		
    		thriftServer->initServe();
    		
	        tickEventTimeval.tv_sec = 0;
	        tickEventTimeval.tv_usec = 10000; // 10ms
    		evtimer_set(&tickEvent, CoreRunner_tickTimer, this);
    		event_base_set(thriftServer->getEventBase(), &tickEvent);
    		if (evtimer_add(&tickEvent, &tickEventTimeval) == -1)
    		{
    			lm->Log(L_ERROR, "<thrift_core> evtimer_add failed!! Thrift will be unable to function properly");
    		}
    		
		lm->Log(L_INFO, "<thrift_core> Listening on %d", port);
		thriftServer->serve(); // blocks
	}
	
	void stop()
	{
		thriftServer->stop();
	}
	
	void tick()
	{
		clientManager->tick();
		if (evtimer_add(&tickEvent, &tickEventTimeval) == -1)
    		{
    			lm->Log(L_ERROR, "<thrift_core> evtimer_add failed!! Thrift will be unable to function properly");
    		}
	}
};

void CoreRunner_tickTimer(int fd, short ev, void* arg)
{
	static_cast<CoreRunner *>(arg)->tick();
}

void fireEvent(shared_ptr<TPoll> event, TArenaID isPosition)
{
	coreRunner->clientManager->fireEvent(event, isPosition);
}

void arenaActionCB(Arena *arena, int action)
{
	static TArenaID nextid = 0;
	int a;
	TArenaID id;
	ArenaD adata;
	shared_ptr<TPoll> event;
	std::string str;
	std::map<std::string /*command*/, std::map<const Client*, std::string /*helptext*/> >::const_iterator iterCommands;
	std::map<TConnection*, shared_ptr<Client> >::const_iterator iterClients;
	
	adata = GetArenaData(arena);
	id = adata->id;
	
	if (action == AA_PRECREATE || action == AA_POSTDESTROY || action == AA_CONFCHANGED) //other modules may fire callbacks that we listen on involving these arenas in AA_CREATE
	{
		if (action == AA_PRECREATE)
		{
			adata->id = ++nextid;
			if (!adata->id) // we had so many id's we wrapped over (INT_MAX -> INT_MIN) at some point and now we came back to 0
			{
				adata->id = ++nextid;
			}
			id = adata->id;
			pthread_mutex_lock(&known_mtx);
			knownArenas[id] = arena;
			pthread_mutex_unlock(&known_mtx);
		}
		else if (action == AA_POSTDESTROY)
		{
			pthread_mutex_lock(&known_mtx);
			knownArenas.erase(id);
			pthread_mutex_unlock(&known_mtx);
			adata->id = 0;
		}
		
		if (coreRunner->clientManager->hasClients())
		{
			event = shared_ptr<TPoll>(new TPoll());
			event->__isset.ArenaChanged = true;
			event->ArenaChanged.arena.__set_id(adata->id);
			
			if (action == AA_PRECREATE)
			{
				event->ArenaChanged.__set_reason(TArenaChangeReason::CREATED);
				str.assign(arena->name);
				event->ArenaChanged.arena.__set_name(str);
				str.assign(arena->basename);
				event->ArenaChanged.arena.__set_basename(str);
			}
			else if (action == AA_POSTDESTROY)
			{
				event->ArenaChanged.__set_reason(TArenaChangeReason::DESTROYED);
				event->ArenaChanged.arena.__set_destroyed(true);
			}
			else // AA_CONFCHANGED
			{
				event->ArenaChanged.__set_reason(TArenaChangeReason::CONFCHANGED);
			}
			fireEvent(event, 0);
		}	 
	}
	
	// more cleaning up
	if (action == AA_POSTDESTROY)
	{
		pthread_mutex_lock(&commands_mtx);
		
		for (iterCommands = commands[id].begin();
		     iterCommands != commands[id].end();
		     iterCommands++)
		{
			for (a = iterCommands->second.size()-1; a >= 0; a--)
			{
				cmd->RemoveCommand(iterCommands->first.c_str(), myCommand, arena);
			}
		}
		commands.erase(id);
		
		pthread_mutex_unlock(&commands_mtx);
		
		for (iterClients =  coreRunner->clientManager->clients.begin();
		     iterClients != coreRunner->clientManager->clients.end();
		     iterClients++ )
		{
			iterClients->second->lockQueuedEvents();
			iterClients->second->sendPositionEvents.erase(id);
			iterClients->second->unlockQueuedEvents();
		}
	}
}


void newPlayerCB(Player *p, int isnew)
{
	std::map<TConnection*, shared_ptr<Client> >::const_iterator iter;
	shared_ptr<Client> client;
	
	if (!isnew)
	{
		if (p->type == T_FAKE)
		{
			coreRunner->clientManager->lockClients();

			for (iter = coreRunner->clientManager->clients.begin();
			     iter != coreRunner->clientManager->clients.end();
			     iter++ )
			{
				client = iter->second;
				
				client->lockMyFakePlayers();
				client->myFakePlayers.remove(p);
				client->unlockMyFakePlayers();
			}
			
			coreRunner->clientManager->unlockClients();
		}
	}
}

void playerActionCB(Player *p, int action, Arena *arena)
{
	static TPlayerID nextid = 0;
	TPlayerID id;
	PlayerD pdata;
	ArenaD adata;
	shared_ptr<TPoll> event;
	std::string str;
	
	pdata = GetPlayerData(p);
	adata = arena ? GetArenaData(arena) : NULL;
	id = pdata->id;
	
	if (action == PA_CONNECT || action == PA_DISCONNECT)
	{
		if (action == PA_CONNECT)
		{
			pdata->id = ++nextid;
			if (!pdata->id) // we had so many id's we wrapped over (INT_MAX -> INT_MIN) at some point and now we came back to 0
			{
				pdata->id = ++nextid;
			}
			id = pdata->id;
			pthread_mutex_lock(&known_mtx);
			knownPlayers[id] = p;
			pthread_mutex_unlock(&known_mtx);
		}
		else
		{
			pthread_mutex_lock(&known_mtx);
			knownPlayers.erase(id);
			pthread_mutex_unlock(&known_mtx);
			pdata->id = 0;
		}
		
		if (coreRunner->clientManager->hasClients())
		{
			event = shared_ptr<TPoll>(new TPoll());
			event->__isset.PlayerChanged = true;
			event->PlayerChanged.player.__set_id(id);
			if (action == PA_CONNECT)
			{
				event->PlayerChanged.__set_reason(TPlayerChangeReason::CONNECT);
				
				str.assign(p->name);
				event->PlayerChanged.player.__set_name(str);
				str.assign(p->squad);
				event->PlayerChanged.player.__set_squad(str);
				event->PlayerChanged.player.__set_type((TClientType::type) p->type); // enum in thrift is defined so that it has the same values
				if (adata)
				{
					event->PlayerChanged.player.__set_arena(adata->id);
				}
				event->PlayerChanged.player.__set_freq(p->p_freq);
				event->PlayerChanged.player.__set_ship((TShip::type) p->p_ship);
				if (p->xres || p->yres)
				{
					event->PlayerChanged.player.__set_resolution(createPoint(p->xres, p->yres));
				}
			}
			else
			{
				event->PlayerChanged.player.__set_destroyed(true);
				event->PlayerChanged.__set_reason(TPlayerChangeReason::DISCONNECT);
			}
			
			fireEvent(event, 0);
		}
	}
	else if (action == PA_ENTERARENA || action == PA_ENTERGAME || action == PA_LEAVEARENA)
	{
		if (coreRunner->clientManager->hasClients())
		{
			event = shared_ptr<TPoll>(new TPoll());
			event->__isset.PlayerChanged = true;
			event->PlayerChanged.player.__set_id(id);
			
			if (action == PA_ENTERARENA)
			{
				event->PlayerChanged.__set_reason(TPlayerChangeReason::ENTERARENA);
				if (adata)
				{
					event->PlayerChanged.player.__set_arena(adata->id);
				}
				event->PlayerChanged.player.__set_freq(p->p_freq);
				event->PlayerChanged.player.__set_ship((TShip::type) p->p_ship);
				if (p->xres || p->yres)
				{
					event->PlayerChanged.player.__set_resolution(createPoint(p->xres, p->yres));
				}
			}
			else if (action == PA_ENTERGAME)
			{
				event->PlayerChanged.__set_reason(TPlayerChangeReason::ENTERGAME);
				event->PlayerChanged.player.__set_freq(p->p_freq);
				event->PlayerChanged.player.__set_ship((TShip::type) p->p_ship);
			}
			else
			{
				event->PlayerChanged.__set_reason(TPlayerChangeReason::LEAVEARENA);
				event->PlayerChanged.player.__set_arena(0);
			}
			
			fireEvent(event, 0);
		}
	}
}

Player * CreateFakePlayerOverride(const char *name, Arena *arena, int ship, int freq)
{
	Player *ret;
	ret = originalCreateFakePlayer(name, arena, ship, freq);
	
	// fake's have no callback to signify they changed had a name assigne, changes arena's, etc
	playerActionCB(ret, PA_CONNECT, ret->arena);
	
	return ret;
}

void shipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	PlayerD pdata;
	shared_ptr<TPoll> event;
	pdata = GetPlayerData(p);
	
	
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.PlayerChanged = true;
		event->PlayerChanged.player.__set_id(pdata->id);
		event->PlayerChanged.__set_reason(TPlayerChangeReason::SHIPFREQCHANGE);
		
		if (oldfreq != newfreq)
		{
			event->PlayerChanged.player.__set_freq(newfreq);
		}
		
		if (oldship != newship)
		{
			event->PlayerChanged.player.__set_ship((TShip::type) newship);
		}
		fireEvent(event, 0);
	}
}

void killCB(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
	PlayerD pdataKiller;
	PlayerD pdataKilled;
	shared_ptr<TPoll> event;
	
	pdataKiller = GetPlayerData(killer);
	pdataKilled = GetPlayerData(killed);
	
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.Kill = true;
		event->Kill.__set_killer(pdataKiller->id);
		event->Kill.__set_killed(pdataKilled->id);
		event->Kill.__set_bounty(bounty);
		fireEvent(event, 0);
	}
}

void spawnCB(Player *p, int reason)
{
	PlayerD pdata;
	shared_ptr<TPoll> event;
	
	pdata = GetPlayerData(p);
	
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.Spawn = true;
		event->Spawn.__set_player(pdata->id);
		event->Spawn.reason.afterDeath  = !!(reason & SPAWN_AFTERDEATH);
		event->Spawn.reason.shipReset   = !!(reason & SPAWN_SHIPRESET);
		event->Spawn.reason.flagVictory = !!(reason & SPAWN_FLAGVICTORY);
		event->Spawn.reason.shipChange  = !!(reason & SPAWN_SHIPCHANGE);
		event->Spawn.reason.initial     = !!(reason & SPAWN_INITIAL);
		fireEvent(event, 0);
	}
}

void goalCB(Arena *arena, Player *p, int bid, int x, int y)
{
	PlayerD pdata;
	shared_ptr<TPoll> event;
	
	pdata = GetPlayerData(p);
	
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.Goal = true;
		event->Goal.__set_player(pdata->id);
		event->Goal.__set_position(createPoint(x, y));
		fireEvent(event, 0);
	}
}

void chatMsgCB(Player *p, int type, int sound, Player *target, int freq, const char *text)
{
	shared_ptr<TPoll> event;
	PlayerD pdata;
	PlayerD pdataTarget;
	std::string str;
	
	if (target && target->type != T_FAKE)
	{
		if (type == MSG_CHAT || type == MSG_PRIV || type == MSG_REMOTEPRIV)
		{
			// Do not fire for private messages (unless the target is a fake player)
			return;
		}
	}
	
	pdata = p ? GetPlayerData(p) : NULL;
	pdataTarget  = target ? GetPlayerData(target) : NULL;
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.Chat = true;
		if (pdata)
		{
			event->Chat.__set_player(pdata->id);
		}
		event->Chat.__set_type((TChatType::type) type);
		
		if (sound)
		{
			event->Chat.__set_sound(sound);
		}
		
		if (pdataTarget)
		{
			event->Chat.__set_target(pdataTarget->id);
		}
		
		if (type == MSG_FREQ || type ==	MSG_NMEFREQ)
		{
			event->Chat.__set_freq(freq);
		}
		
		str.assign(text);
		event->Chat.__set_text(str);
		
		fireEvent(event, 0);
	}
}

void myCommand(const char *command, const char *params, Player *p, const Target *target)
{
	PlayerD pdata;
	ArenaD adata;
	shared_ptr<TPoll> event;
	std::string cmd;
	std::string str;
	std::map<TConnection*, shared_ptr<Client> >::const_iterator iter;
	
	pdata = GetPlayerData(p);
	adata = GetArenaData(p->arena);
	
	cmd.assign(command);
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.Command = true;
		cmd.assign(command);
		event->Command.__set_command(cmd);
		str.assign(params);
		event->Command.__set_params(str);
		event->Command.__set_player(pdata->id);
		
		Target_to_TTarget(target, &event->Command.target);
		
		coreRunner->clientManager->lockClients();
		pthread_mutex_lock(&commands_mtx);
		for (iter = coreRunner->clientManager->clients.begin();
		     iter != coreRunner->clientManager->clients.end();
		     iter++)
		{
			// iter->second is shared_ptr<Client>
			
			if (commands.count(0) && 
			    commands[0].count(cmd) && 
			    commands[0][cmd].count(iter->second.get()))
			{
				iter->second->lockQueuedEvents();
				iter->second->queuedEvents.push(event);
				iter->second->unlockQueuedEvents();
			}
			else
			{
				if (commands.count(adata->id) && 
				    commands[adata->id].count(cmd) && 
				    commands[adata->id][cmd].count(iter->second.get()))
				{
					iter->second->lockQueuedEvents();
					iter->second->queuedEvents.push(event);
					iter->second->unlockQueuedEvents();
				}
			}
			
		}
		pthread_mutex_unlock(&commands_mtx);
		coreRunner->clientManager->unlockClients();
	}
}

static void ppkCB(Player *p, const struct C2SPosition *pos)
{
	shared_ptr<TPoll> event;
	PlayerD pdata;
	ArenaD adata;
	
	if (!p->arena)
	{
		return;
	}
	
	pdata = GetPlayerData(p);
	adata = GetArenaData(p->arena);
	
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.Position = true;
		event->Position.arena = adata->id;
		
		event->Position.position.player = pdata->id;
		event->Position.position.time = ticks_to_TTime(pos->time);
		event->Position.position.position.x = pos->x;
		event->Position.position.position.y = pos->y;
		event->Position.position.speed.x = pos->xspeed;
		event->Position.position.speed.y = pos->yspeed;
		event->Position.position.rotation = pos->rotation;
		
		event->Position.position.status.stealth  = !!(pos->status & STATUS_STEALTH);
		event->Position.position.status.cloak    = !!(pos->status & STATUS_CLOAK);
		event->Position.position.status.xradar   = !!(pos->status & STATUS_XRADAR);
		event->Position.position.status.antiwarp = !!(pos->status & STATUS_ANTIWARP);
		event->Position.position.status.flash    = !!(pos->status & STATUS_FLASH);
		event->Position.position.status.safezone = !!(pos->status & STATUS_SAFEZONE);
		event->Position.position.status.ufo      = !!(pos->status & STATUS_UFO);
		event->Position.position.status.unknown  = !!(pos->status & 0x80U);
		
		event->Position.position.bounty = pos->bounty;
		event->Position.position.energy = pos->energy;
		
		if (pos->weapon.type != W_NULL)
		{
			event->Position.position.__isset.weapon = true;
			event->Position.position.weapon.type = (TWeapon::type) pos->weapon.type;
			event->Position.position.weapon.level = pos->weapon.level;
			event->Position.position.weapon.shrapbouncing = pos->weapon.shrapbouncing;
			event->Position.position.weapon.shraplevel = pos->weapon.shraplevel;
			event->Position.position.weapon.shrap = pos->weapon.shrap;
			event->Position.position.weapon.alternate = pos->weapon.alternate;
		}
		
		if (pos->extra.energy || pos->extra.s2cping) // https://bitbucket.org/grelminar/asss/issue/113/unable-to-detect-the-presence-of
		{
			event->Position.position.__isset.extra = true;
			
			event->Position.position.extra.s2cping = pos->extra.s2cping;
			event->Position.position.extra.timer = pos->extra.timer;
			event->Position.position.extra.shields = !!pos->extra.shields;
			event->Position.position.extra.tempSuper = !!pos->extra.super;
			event->Position.position.extra.bursts = pos->extra.bursts;
			event->Position.position.extra.repels = pos->extra.repels;
			event->Position.position.extra.thors = pos->extra.thors;
			event->Position.position.extra.bricks = pos->extra.bricks;
			event->Position.position.extra.decoys = pos->extra.decoys;
			event->Position.position.extra.rockets = pos->extra.rockets;
			event->Position.position.extra.portals = pos->extra.portals;
		}
		
		fireEvent(event, adata->id);
	}
}

static void attachCB(Player *p, Player *to)
{
	shared_ptr<TPoll> event;
	PlayerD pdata;
	PlayerD pdata_to;
	ArenaD adata;
	
	if (!p->arena)
	{
		return;
	}
	
	pdata = GetPlayerData(p);
	adata = GetArenaData(p->arena);
	pdata_to = to ? GetPlayerData(to) : NULL;
	
	if (coreRunner->clientManager->hasClients())
	{
		event = shared_ptr<TPoll>(new TPoll());
		event->__isset.Attach = true;
		event->Attach.player = pdata->id;
		if (pdata_to)
		{
			event->Attach.__set_to(pdata_to->id);
		}
		
		fireEvent(event, adata->id);
	}
}

}
extern "C" EXPORT const char info_thrift_core[] = "ASSS Thrift v1.0 by JoWie <jowie@welcome-to-the-machine.com>\n";

using namespace ThriftAsss;
static void releaseInterfaces()
{
        mm->ReleaseInterface(aman      );
        mm->ReleaseInterface(bricks    );
        mm->ReleaseInterface(cfg       );
        mm->ReleaseInterface(chat      );
        mm->ReleaseInterface(cmd       );
	mm->ReleaseInterface(fake      );        
        mm->ReleaseInterface(game      );
        mm->ReleaseInterface(lm        );
        mm->ReleaseInterface(mapdata   );
        mm->ReleaseInterface(ml        );
        mm->ReleaseInterface(objects   );
        mm->ReleaseInterface(net       );
        mm->ReleaseInterface(pd        );
        mm->ReleaseInterface(stats     );
}

extern "C" EXPORT int MM_thrift_core(int action, Imodman *mm_, Arena *arena)
{
	int a;
	int port;
	Link *link;
	void *cst;
	Player *p;
	std::map<std::string /*command*/, std::map<const Client*, std::string /*helptext*/> >::const_iterator iter;
	
        if (action == MM_LOAD)
        {
		mm      = mm_;
		aman    = (Iarenaman*)   mm->GetInterface(I_ARENAMAN     , ALLARENAS);
		bricks  = (Ibricks*)     mm->GetInterface(I_BRICKS       , ALLARENAS);
		cfg     = (Iconfig*)     mm->GetInterface(I_CONFIG       , ALLARENAS);
		chat    = (Ichat*)       mm->GetInterface(I_CHAT         , ALLARENAS);
		cmd     = (Icmdman*)     mm->GetInterface(I_CMDMAN       , ALLARENAS);
		fake    = (Ifake*)       mm->GetInterface(I_FAKE         , ALLARENAS);
		game    = (Igame*)       mm->GetInterface(I_GAME         , ALLARENAS);
		lm      = (Ilogman*)     mm->GetInterface(I_LOGMAN       , ALLARENAS);
		mapdata = (Imapdata*)    mm->GetInterface(I_MAPDATA      , ALLARENAS);
		ml      = (Imainloop*)   mm->GetInterface(I_MAINLOOP     , ALLARENAS);
		objects = (Iobjects*)    mm->GetInterface(I_OBJECTS      , ALLARENAS);
		net     = (Inet*)        mm->GetInterface(I_NET          , ALLARENAS);
		pd      = (Iplayerdata*) mm->GetInterface(I_PLAYERDATA   , ALLARENAS);
		stats   = (Istats*)      mm->GetInterface(I_STATS        , ALLARENAS);

 		if  (!aman || !cfg || !chat || !cmd || !game || !lm || !mapdata || !ml || !net || !pd)
		{
			releaseInterfaces();
			printf("<thrift_core> <thrift_core> Missing interface(s)");
			return MM_FAIL;
		}
		
		arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));
                if (arenaKey == -1)
                {
                        releaseInterfaces();
                        return MM_FAIL;
                }

                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));
                if (playerKey == -1)
                {
                        aman->FreeArenaData(arenaKey);
                        releaseInterfaces();
                        return MM_FAIL;
                }

		port = cfg->GetInt(GLOBAL, "Thrift", "Listen", 7487);
		if (port <= 0 || port > 65535)
		{
			lm->Log(L_ERROR, "<thrift_core> Invalid Thrift:Listen %d", port);
			return MM_FAIL;
		}
		
		pthread_mutex_init(&known_mtx, NULL);
		pthread_mutex_init(&commands_mtx, NULL);
		
		if (fake)
		{
			originalCreateFakePlayer = fake->CreateFakePlayer;
			fake->CreateFakePlayer = CreateFakePlayerOverride;
		}
		
		threadFactory = shared_ptr<PosixThreadFactory>(new PosixThreadFactory());
		coreRunner = shared_ptr<CoreRunner>(new CoreRunner(port));
		coreThread = threadFactory->newThread(coreRunner);
		lm->Log(L_INFO, "<thrift_core> Creating Thrift thread...");
		coreThread->start();
		
		aman->Lock();
		FOR_EACH_ARENA(cst)
		{
			arena = (Arena *) cst;
			arenaActionCB(arena, AA_PRECREATE);
			arenaActionCB(arena, AA_CREATE);
		}
		mm->RegCallback(CB_ARENAACTION, (void*) arenaActionCB, ALLARENAS);
		aman->Unlock();
		
		pd->Lock();
	        FOR_EACH_PLAYER(cst)
	        {
	        	p = (Player *) cst;
	                newPlayerCB(p, 1);
	        }
	        mm->RegCallback(CB_NEWPLAYER, (void*) newPlayerCB, ALLARENAS);
	        mm->RegCallback(CB_PLAYERACTION, (void*) playerActionCB, ALLARENAS);
	        pd->Unlock();
		
		mm->RegCallback(CB_SHIPFREQCHANGE, (void*) shipFreqChangeCB, ALLARENAS);
		mm->RegCallback(CB_KILL, (void*) killCB, ALLARENAS);
		mm->RegCallback(CB_SPAWN, (void*) spawnCB, ALLARENAS);
		mm->RegCallback(CB_GOAL, (void*) goalCB, ALLARENAS);
		mm->RegCallback(CB_CHATMSG, (void*) chatMsgCB, ALLARENAS);
		mm->RegCallback(CB_PPK, (void*) ppkCB, ALLARENAS);
		mm->RegCallback(CB_ATTACH, (void*) attachCB, ALLARENAS);
		
                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
        	if (fake)
        	{
			if (fake->CreateFakePlayer != CreateFakePlayerOverride)
			{
				return MM_FAIL;
			}
			
			fake->CreateFakePlayer = originalCreateFakePlayer;
        	}
        	mm->UnregCallback(CB_SHIPFREQCHANGE, (void*) shipFreqChangeCB, ALLARENAS);
        	mm->UnregCallback(CB_KILL, (void*) killCB, ALLARENAS);
		mm->UnregCallback(CB_SPAWN, (void*) spawnCB, ALLARENAS);
		mm->UnregCallback(CB_GOAL, (void*) goalCB, ALLARENAS);
		mm->UnregCallback(CB_CHATMSG, (void*) chatMsgCB, ALLARENAS);
		mm->UnregCallback(CB_PPK, (void*) ppkCB, ALLARENAS);
		mm->UnregCallback(CB_ATTACH, (void*) attachCB, ALLARENAS);
        
        	pthread_mutex_lock(&commands_mtx);
		for (iter =  commands[0].begin();
		     iter != commands[0].end();
		     iter++)
		{
			for (a = iter->second.size()-1; a >= 0; a--)
			{
				cmd->RemoveCommand(iter->first.c_str(), myCommand, NULL);
			}
		}
		commands.erase(0);
		pthread_mutex_unlock(&commands_mtx);
        	
        
        	pd->Lock();
	        FOR_EACH_PLAYER(cst)
	        {
	        	p = (Player *) cst;
	                newPlayerCB(p, 0);
	        }
	        mm->UnregCallback(CB_NEWPLAYER, (void*)newPlayerCB, ALLARENAS);
	        mm->UnregCallback(CB_PLAYERACTION, (void*) playerActionCB, ALLARENAS);
	        pd->Unlock();
	        
	        aman->Lock();
		FOR_EACH_ARENA(cst)
		{
			arena = (Arena *) cst;
			arenaActionCB(arena, AA_DESTROY);
			arenaActionCB(arena, AA_POSTDESTROY);
		}
		mm->UnregCallback(CB_ARENAACTION, (void*) arenaActionCB, ALLARENAS);
		aman->Unlock();
        
		coreRunner->stop();
		coreThread->join();
		coreThread.reset(); // decrease the reference count
		coreRunner.reset();
		threadFactory.reset();
        	
        	pthread_mutex_destroy(&known_mtx);
        	pthread_mutex_destroy(&commands_mtx);
        	
        	aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);
        	
                releaseInterfaces();

                return MM_OK;
        }

        return MM_FAIL;
}

