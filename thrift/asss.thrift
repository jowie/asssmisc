namespace java ThriftAsss
namespace cpp ThriftAsss
namespace csharp ThriftAsss
namespace py ThriftAsss
namespace php ThriftAsss
namespace perl ThriftAsss

include "core.thrift"
include "event.thrift"
service TASSS extends core.ASSSCore {
	# Must always be called after connecting.
	# Identify this session by a module name and password. 
	# The module name is only used to look up the password and for things like logging
	# A rpc client may be limited to a specific arena
	void authenticate(1: required string modulename, 
	                  2: required string password) throws (1: core.TGeneralException general, 
	                                                       2: core.TAuthenticateFailed auth),

	list<event.TPoll> poll(),



	# ... More functions go here
}