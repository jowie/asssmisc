/* 
ASSS Thrift 
Copyright (c) 2011  Joris v/d Wel
jowie@welcome-to-the-machine.com

This file is part of ASSS Thrift

   ASSS Thrift is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Thrift is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with ASSS Thrift.  If not, see <http://www.gnu.org/licenses/>.
  
   In addition, the following supplemental terms based on section 7 of
   the GNU General Public License (version 3):
   a) Preservation of all legal notices and author attributions
   b) Prohibition of misrepresentation of the origin of this material, and
      modified versions are required to be marked in reasonable ways as
      different from the original version
*/
#include "core_types.h"

namespace ThriftAsss {
	bool TArenaData::operator < (const TArenaData & rhs) const
	{
		return id < rhs.id;
	}
	
	bool TPoint::operator < (const TPoint & rhs) const
	{
		if (y == rhs.y)
		{
			return x < rhs.x;
		}
		
		return y < rhs.y;
	}
	
	bool TPlayerData::operator < (const TPlayerData & rhs) const
	{
		return id < rhs.id;
	}
	
	/*bool TTarget::operator < (const TTarget & rhs) const
	{
		
	}*/
}
