namespace java ThriftAsss
namespace cpp ThriftAsss
namespace csharp ThriftAsss
namespace py ThriftAsss
namespace php ThriftAsss
namespace perl ThriftAsss

include "core.thrift"
struct TPoll {
	### Core Events ###
	1 : optional core.TArenaChangedEvent ArenaChanged,
	2 : optional core.TPlayerChangedEvent PlayerChanged,
	3 : optional core.TKillEvent Kill,
	4 : optional core.TSpawnEvent Spawn,
	5 : optional core.TPositionEvent Position,
	6 : optional core.TAttachEvent Attach,
	7 : optional core.TGoalEvent Goal,
	8 : optional core.TChatEvent Chat,
	9 : optional core.TCommandEvent Command,
	10: optional core.TMapDataEvent MapData,
	
	# Add events here
}
