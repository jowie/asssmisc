/* $Id: motd.c 239 2009-10-01 22:24:03Z joris $ */
/**

 Staff Message of the day

 14 september 2007 - Started by JoWie
  1 oktober   2007 - JoWie: Fixed double cfg declarations, added release interfaces where it was missing
  30 juni     2009 - Added support for very long motd's, you won't be able to ?setmotd it however

*/

#include "asss.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#define CAP_SEEMOTD               "cmd_motd"


static void ShowMOTD (Player *p);
static void SetMOTD(const char *motd, const char *playerName);

static Imodman     *mm;
static Ichat       *chat;
static Icapman     *capman;
static Iconfig     *cfg;
static Icmdman     *cmd;
static Iplayerdata *pd;

static int playerKey = -1;

struct playerData
{
        /** Player has seen the MOTD before */
        bool hasSeenMOTD;
};

static void PlayerAction (Player *p, int action, Arena *arena)
{
        if (!p || !arena || p->arena != arena || playerKey == -1) return;

        if (action == PA_ENTERARENA)
        {
                if (capman->HasCapability(p, CAP_SEEMOTD))
                {
                        struct playerData *pdata = PPDATA(p, playerKey);
                        if (!pdata->hasSeenMOTD)
                        {
                                pdata->hasSeenMOTD = 1;
                                ShowMOTD(p);
                        }
                }
        }
}

static helptext_t motd_help =
        "Targets: none\n"
        "Args: none\n"
        "Displays the Message of the Day";

/** ?motd: Displays the message of the day */
static void Cmotd(const char *tc, const char *params, Player *p, const Target *target)
{
        ShowMOTD(p);
}

static helptext_t setmotd_help =
        "Targets: none\n"
        "Args: <message>\n"
        "Sets the Message of the Day without having to use ?setg";


/** ?setmotd: Sets the motd without having to use ?setg */
static void Csetmotd(const char *tc, const char *params, Player *p, const Target *target)
{
        SetMOTD(params, p->name);
        chat->SendCmdMessage(p, "MOTD set");
}

static helptext_t addmotd_help =
        "Targets: none\n"
        "Args: <message>\n"
        "Adds something to the Message of the Day";

static void Caddmotd(const char *tc, const char *params, Player *p, const Target *target)
{
	const char *motd;
	
	motd = cfg->GetStr(GLOBAL, "motd", "motd");
	if (motd)
	{
		size_t lineLength = strlen(motd) + 3 + strlen(params) + 1;
		char *line = amalloc(lineLength);
		snprintf(line, lineLength, "%s | %s", motd, params);
		SetMOTD(line, p->name);
		chat->SendCmdMessage(p, "MOTD set");
	}
	else
	{
		SetMOTD(params, p->name);
        	chat->SendCmdMessage(p, "MOTD set");
	}
}

static void SetMOTD(const char *motd, const char *playerName)
{
        time_t tm = time(NULL);
        char info[128];

        snprintf(info, 100, "set by %s with ?setmotd on ", playerName);
        ctime_r(&tm, info + strlen(info));
        RemoveCRLF(info);
        info[127] = 0;
        
        size_t lineLength = strlen(motd) + 2 + strlen(playerName) + 1;
        char *line = amalloc(lineLength);
	snprintf(line, lineLength, "%s -%s", motd, playerName);

        cfg->SetStr(GLOBAL, "motd", "motd", line, info, 1);
        afree(line);
}

/** Send the MOTD to a player */
static void ShowMOTD (Player *p)
{
        if (!p) return;
        LinkedList lst = LL_INITIALIZER;
        LLAdd(&lst, p);

        const char *motd;
        char buf[256];
	size_t motd_len;
	int a;

        motd = cfg->GetStr(GLOBAL, "motd", "motd");
        
        if (motd)
        {
        	motd_len = strlen(motd);
        	for (a = 0; a < motd_len; a += 200)
        	{
        		if (!a)
        		{
		                snprintf(buf, 207, "MOTD: %s", motd+a);
		                buf[206] = 0;
			}
			else
			{
			       snprintf(buf, 201, "%s", motd+a);
		               buf[200] = 0;
			}
	                chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, 0, NULL, "%s", buf);
                }
        }
        LLEmpty(&lst);
}

EXPORT const char info_motd[] = "MotD $Revision: 239 $ by JoWie\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(cfg   );
        mm->ReleaseInterface(cmd   );
        mm->ReleaseInterface(capman);
        mm->ReleaseInterface(chat  );
        mm->ReleaseInterface(pd    );
}

EXPORT int MM_motd(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;

                chat   = mm->GetInterface(I_CHAT      , ALLARENAS);
                capman = mm->GetInterface(I_CAPMAN    , ALLARENAS);
                cfg    = mm->GetInterface(I_CONFIG    , ALLARENAS);
                cmd    = mm->GetInterface(I_CMDMAN    , ALLARENAS);
                pd     = mm->GetInterface(I_PLAYERDATA, ALLARENAS);

                if (!chat || !capman || !cfg || !cmd || !pd)
                {
                        printf("<motd> Missing Interface\n");

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));

                if (playerKey == -1)
                {
                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                mm->RegCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);
                cmd->AddCommand("motd", Cmotd, ALLARENAS, motd_help);
                cmd->AddCommand("setmotd", Csetmotd, ALLARENAS, setmotd_help);
                cmd->AddCommand("addmotd", Caddmotd, ALLARENAS, addmotd_help);

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                cmd->RemoveCommand("motd", Cmotd, ALLARENAS);
                cmd->RemoveCommand("setmotd", Csetmotd, ALLARENAS);
                cmd->RemoveCommand("addmotd", Caddmotd, ALLARENAS);

                mm->UnregCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);

                pd->FreePlayerData(playerKey);

                ReleaseInterfaces();
                return MM_OK;
        }

        return MM_FAIL;
}
