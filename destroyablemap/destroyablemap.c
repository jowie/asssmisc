/* $Id: destroyablemap.c 25 2010-09-12 19:14:16Z jowie $ */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "asss.h"
#include "clientset.h"
#include "mapdata.h"
#include "packets/flags.h"
#include "damage.h"

#define MAXFLAGS 255

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif


static Imodman		*mm;
static Imainloop	*ml;
static Iarenaman 	*aman;
static Iplayerdata	*pd;
static Inet		*net;
static Icmdman		*cmd;
static Ilogman		*lm;
static Iconfig		*cfg;
static Imapdata		*mapdata;
static Iclientset	*clientset;
static Idamage		*damage;
static Iflagcore	*flagcore;

static int arenaKey  = -1;
static int playerKey = -1;
static override_key_t override_flag_carryflags;
static void (*flagCore_ReserveFlags)(Arena *arena, int flagcount);

static void DamageRemoveWeaponCB(Arena *arena, DamageWeapon *wpn, u8 removereason);
static int UpdateFlagsTimer(Arena *arena);
static void PlayerActionCB(Player *p, int action, Arena *arena);
static void ReadConfig(Arena *arena);


struct ArenaData
{
	bool enabled;
	int flagStart; //which flags can we use
	LinkedList destroyQueue; // struct Tile
	LinkedList destroyed; // struct Tile
	int destroyedCount;

	int destroyableTileStart;
	int destroyableTileEnd;
};

#define PlayerData PlayerData_
struct PlayerData
{
	int needToSendFlagCount;
	int sentFlagCount;
	Link *nextFlagToSend;
	int usedFlags; // how many flags does the player have on his screen right now?
};

struct Tile
{
	short x;
	short y;
};

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<destroyablemap> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct ArenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        struct ArenaData *adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);
        return adata;
}

static struct PlayerData* GetPlayerData(Player *p)
{
        assertlm(p);
        struct PlayerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);
        return pdata;
}


static void ArenaActionCB(Arena *arena, int action)
{
	struct ArenaData *adata;
	struct PlayerData* pdata;
	int val;
	Link *link;
	Player *p;
	if (action == AA_CREATE)
	{

		adata = GetArenaData(arena);
		adata->enabled = true;
		LLInit(&adata->destroyQueue);
		LLInit(&adata->destroyed);
		adata->destroyedCount = 0;
		adata->flagStart = 0;

		if (clientset->GetArenaOverride(arena, override_flag_carryflags, &val))
		{
			if (val <= CARRY_NONE)
			{
				lm->LogA(L_ERROR, "destroyablemap", arena, "CarryMode was set to %d, turf game is not supported", val);
				clientset->ArenaOverride(arena, override_flag_carryflags, CARRY_ONE);
			}
		}
		else
		{
			clientset->ArenaOverride(arena, override_flag_carryflags, CARRY_ONE);
		}


		damage->AddWeaponCallback(arena, NULL, DamageRemoveWeaponCB, NULL);
		ml->SetTimer((TimerFunc) UpdateFlagsTimer, 10, 10, arena, arena);
		ReadConfig(arena);
	}
	else if (action == AA_CONFCHANGED)
	{
		ReadConfig(arena);
	}
	else if (action == AA_DESTROY) // may be called multiple times due to some stuff we do in MM_destroyablemap
	{
		adata = GetArenaData(arena);
		if (!adata->enabled) return;
		adata->enabled = false;
		ml->ClearTimer((TimerFunc) UpdateFlagsTimer, arena);
		damage->RemoveWeaponCallback(arena, NULL, DamageRemoveWeaponCB, NULL);
		LLEnum(&adata->destroyQueue, afree);
		LLEnum(&adata->destroyed, afree);
		adata->destroyedCount = 0;
		LLEmpty(&adata->destroyQueue);
		LLEmpty(&adata->destroyed);

		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena)
			{
				pdata = GetPlayerData(p);
				PlayerActionCB(p, PA_LEAVEARENA, arena);
			}
		}
		pd->Unlock();
	}
}

static void ReadConfig(Arena *arena)
{
	ConfigHandle ch;
	struct ArenaData *adata;
	ch = arena->cfg;

	adata = GetArenaData(arena);

	adata->destroyableTileStart = cfg->GetInt(ch, "destroyablemap", "TileStart", 0);
	adata->destroyableTileEnd = cfg->GetInt(ch, "destroyablemap", "TileEnd", TILE_END);
}


static void ReserveFlagsOverride(Arena *arena, int flagcount)
{
	struct ArenaData *adata;
	adata = GetArenaData(arena);
	if (adata->enabled)
	{
		adata->flagStart = flagcount+1;
		if (flagcount >= MAXFLAGS)
		{
			lm->LogA(L_ERROR, "destroyablemap", arena, "There are not enough available flags; destroyablemap will not function");
		}
		else if (flagcount > MAXFLAGS - 25)
		{
			lm->LogA(L_WARN, "destroyablemap", arena, "There are very few available flags; destroyablemap may have a laggy performance");
		}
	}

	flagCore_ReserveFlags(arena, flagcount);
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	struct PlayerData* pdata;
	struct ArenaData *adata;
	if (action == PA_ENTERGAME) // download is complete
	{
		adata = GetArenaData(arena);
		pdata = GetPlayerData(p);
		if (!adata->enabled)
			return;

		pdata->needToSendFlagCount = adata->destroyedCount;
		pdata->nextFlagToSend = LLGetHead(&adata->destroyed);
		pdata->sentFlagCount = 0;
		pdata->usedFlags = 0;
	}
	else if (action == PA_LEAVEARENA)
	{
		pdata = GetPlayerData(p);
		pdata->needToSendFlagCount = 0;
		pdata->nextFlagToSend = NULL;
		pdata->sentFlagCount = 0;
		pdata->usedFlags = 0;
	}
}

static bool IsDestroyed(Arena *arena, short x, short y)
{
	struct ArenaData *adata;
	struct Tile *tile;
	Link *l;

	adata = GetArenaData(arena);
	if (!adata->enabled)
		return false;

	for (l = LLGetHead(&adata->destroyed); l; l = l->next)
	{
		tile = (struct Tile*) l->data;
		if (tile->x == x && tile->y == y)
			return true;
	}
	return false;
}

// Might want to check IsDestroyed first
static void DestroyTile(Arena *arena, short x, short y)
{
	struct ArenaData *adata;
	adata = GetArenaData(arena);
	struct Tile *tile;

	if (!adata->enabled)
		return;

	tile = amalloc(sizeof(struct Tile));
	tile->x = x;
	tile->y = y;

	if (!IsDestroyed(arena, x, y))
		LLAdd(&adata->destroyQueue, tile);
}

static int UpdateFlagsTimer(Arena *arena)
{
	struct ArenaData *adata;
	Link *link;
	struct Tile *tile;
	int fid;
	int usedFlags;
	int oldUsedFlags;
	struct S2CFlagLocation flag;
	Player *p;
	struct PlayerData* pdata;

	flag.type = S2C_FLAGLOC;

	if (arena->status != ARENA_RUNNING)
		return 1;

	adata = GetArenaData(arena);
	if (!adata->enabled)
		return 0;


	usedFlags = 0;
	for (fid = adata->flagStart; fid < MAXFLAGS; fid++)
	{
		tile = (struct Tile*) LLRemoveFirst(&adata->destroyQueue);
		if (!tile)
			break;

		usedFlags++;
		LLAdd(&adata->destroyed, tile);
		adata->destroyedCount++;

		// send the flags to each player with the team set to their own team. this way they can not pick it up (the flags will be removed in a short while)
		flag.fid = fid;
		flag.x = tile->x;
		flag.y = tile->y;
		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena && p->status == S_PLAYING && IS_STANDARD(p))
			{
				flag.freq = p->p_freq;
				net->SendToOne(p, (byte*)&flag, sizeof(flag), NET_RELIABLE);
			}
		}
		pd->Unlock();

		//make other modules believe the tile is gone (like the damage module)
		mapdata->DoBrick(arena, 0, tile->x, tile->y, tile->x, tile->y); // this module is one big hack.
	}


	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->arena == arena && p->status == S_PLAYING && IS_STANDARD(p))
		{
			pdata = GetPlayerData(p);
			oldUsedFlags = pdata->usedFlags;
			pdata->usedFlags = usedFlags;

			if (pdata->sentFlagCount < pdata->needToSendFlagCount)
			{
				for (fid = adata->flagStart + pdata->usedFlags; fid < MAXFLAGS; fid++)
				{
					if (!pdata->nextFlagToSend)
					{
						// done!
						pdata->needToSendFlagCount = 0;
						pdata->nextFlagToSend = NULL;
						pdata->sentFlagCount = 0;
						break;
					}
					tile = pdata->nextFlagToSend->data;
					pdata->sentFlagCount++;
					pdata->usedFlags++;

					flag.fid = fid;
					flag.x = tile->x;
					flag.y = tile->y;
					flag.freq = p->p_freq;
					net->SendToOne(p, (byte*)&flag, sizeof(flag), NET_RELIABLE);

					pdata->nextFlagToSend = pdata->nextFlagToSend->next;
				}
			}

			// reset unused flags
			for (fid = adata->flagStart + pdata->usedFlags; fid < adata->flagStart + oldUsedFlags; fid++)
			{
				flag.fid = fid;
				flag.x = -1;
				flag.y = -1;
				flag.freq = -1;
				net->SendToOne(p, (byte*)&flag, sizeof(flag), NET_RELIABLE);
			}
		}
	}
	pd->Unlock();

	return 1;
}

// ?placeflag 500 500
static void Cplaceflag(const char *tc, const char *params, Player *p, const Target *target)
{
	long x, y;
	char *pEnd;

	x = strtol(params, &pEnd, 10);
	while (*pEnd == ' ' || *pEnd == ',')
		pEnd++;

	y = strtol(pEnd, NULL, 10);

	if (!x)	x = p->position.x / 16;
	if (!y) y = p->position.y / 16;

	if (!IsDestroyed(p->arena, x, y))
		DestroyTile(p->arena, x, y);
}

static void DamageRemoveWeaponCB(Arena *arena, DamageWeapon *wpn, u8 removereason)
{
	enum map_tile_t tile;
	struct ArenaData *adata;
	if (removereason != DAMAGE_REMOVEWPN_HIT)
		return;

	if (wpn->wpn.type == W_BOMB || wpn->wpn.type == W_PROXBOMB)
	{
		if (wpn->hitx < 0 || wpn->hity < 0 || wpn->hitx > 1023 || wpn->hity > 1023)
			return;

		adata = GetArenaData(arena);
		tile = mapdata->GetTile(arena, wpn->hitx, wpn->hity);

		if (tile < adata->destroyableTileStart || tile > adata->destroyableTileEnd || tile > TILE_END) // exclude special tiles; safe zone; asteroids; wormholes; bricks; etc
			return;


		// note: the flag trick does not work on doors or bricks
		DestroyTile(arena, wpn->hitx, wpn->hity);
	}

}

EXPORT const char info_destroyablemap[] = "DestroyableMap $Revision: 25 $ by JoWie\n";
static void ReleaseInterfaces()
{
	mm->ReleaseInterface(ml       );
        mm->ReleaseInterface(aman     );
        mm->ReleaseInterface(pd       );
        mm->ReleaseInterface(net      );
        mm->ReleaseInterface(cmd      );
        mm->ReleaseInterface(lm       );
        mm->ReleaseInterface(clientset);
        mm->ReleaseInterface(mapdata  );
        mm->ReleaseInterface(damage   );
        mm->ReleaseInterface(cfg      );
        mm->ReleaseInterface(flagcore );
}

EXPORT int MM_destroyablemap(int action, Imodman *mm_, Arena *arena)
{
	Arena *a;
	Link *link;
        if (action == MM_LOAD)
        {
                mm        = mm_;
                ml        = mm->GetInterface(I_MAINLOOP        , ALLARENAS);
                aman      = mm->GetInterface(I_ARENAMAN        , ALLARENAS);
                pd        = mm->GetInterface(I_PLAYERDATA      , ALLARENAS);
                net       = mm->GetInterface(I_NET             , ALLARENAS);
                cmd       = mm->GetInterface(I_CMDMAN          , ALLARENAS);
                lm        = mm->GetInterface(I_LOGMAN          , ALLARENAS);
                mapdata   = mm->GetInterface(I_MAPDATA         , ALLARENAS);
                clientset = mm->GetInterface(I_CLIENTSET       , ALLARENAS);
                damage    = mm->GetInterface(I_DAMAGE          , ALLARENAS);
                cfg       = mm->GetInterface(I_CONFIG	       , ALLARENAS);
                flagcore  = mm->GetInterface(I_FLAGCORE	       , ALLARENAS);



                if (!ml || !aman || !pd || !net || !cmd || !lm || !clientset || !mapdata || !damage || !cfg || !flagcore)
                {
                        ReleaseInterfaces();
                        printf("<destroyablemap> Missing interfaces\n");
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct ArenaData));
                playerKey = pd->AllocatePlayerData(sizeof(struct PlayerData));

                if (arenaKey == -1 || playerKey == -1) // check if we ran out of memory
                {
                        if (arenaKey  != -1) // free data if it was allocated
                                aman->FreeArenaData(arenaKey);

                        if (playerKey != -1) // free data if it was allocated
                                pd->FreePlayerData (playerKey);

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

		override_flag_carryflags = clientset->GetOverrideKey("Flag", "CarryFlags");

		flagCore_ReserveFlags = flagcore->ReserveFlags;
		flagcore->ReserveFlags = ReserveFlagsOverride;

                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
		if (flagcore->ReserveFlags != ReserveFlagsOverride)
			return MM_FAIL;

		flagcore->ReserveFlags = flagCore_ReserveFlags;

		// unloaded but there are still arena's running
		aman->Lock();
		FOR_EACH_ARENA(a)
		{
			ArenaActionCB(a, AA_DESTROY);
		}
		aman->Unlock();

                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);

                ReleaseInterfaces();

                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
		cmd->AddCommand("placeflag", Cplaceflag, arena, NULL);
		mm->RegCallback(CB_ARENAACTION, ArenaActionCB, arena); // AA_CREATE is fired after MM_ATACH (AA_PRECREATE is not)
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
		cmd->RemoveCommand("placeflag", Cplaceflag, arena);
		mm->UnregCallback(CB_ARENAACTION, ArenaActionCB, arena);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);

		ArenaActionCB(arena, AA_DESTROY);

                return MM_OK;
        }

        return MM_FAIL;
}

